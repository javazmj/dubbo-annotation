package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ShopAuditLog;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface ShopAuditLogMapper {
    @Delete({
        "delete from shop_audit_log",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into shop_audit_log (id, shop_id, ",
        "shop_phone, shop_address, ",
        "\"business _license\", industry_license, ",
        "other_license, is_audit, ",
        "fail_detail, auditer_name, ",
        "create_time, audit_time)",
        "values (#{id,jdbcType=INTEGER}, #{shopId,jdbcType=INTEGER}, ",
        "#{shopPhone,jdbcType=VARCHAR}, #{shopAddress,jdbcType=VARCHAR}, ",
        "#{businessLicense,jdbcType=VARCHAR}, #{industryLicense,jdbcType=VARCHAR}, ",
        "#{otherLicense,jdbcType=VARCHAR}, #{isAudit,jdbcType=TINYINT}, ",
        "#{failDetail,jdbcType=VARCHAR}, #{auditerName,jdbcType=VARCHAR}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{auditTime,jdbcType=TIMESTAMP})"
    })
    int insert(ShopAuditLog record);

    @Select({
        "select",
        "id, shop_id, shop_phone, shop_address, \"business _license\", industry_license, ",
        "other_license, is_audit, fail_detail, auditer_name, create_time, audit_time",
        "from shop_audit_log",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="shop_phone", property="shopPhone", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_address", property="shopAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="business _license", property="businessLicense", jdbcType=JdbcType.VARCHAR),
        @Result(column="industry_license", property="industryLicense", jdbcType=JdbcType.VARCHAR),
        @Result(column="other_license", property="otherLicense", jdbcType=JdbcType.VARCHAR),
        @Result(column="is_audit", property="isAudit", jdbcType=JdbcType.TINYINT),
        @Result(column="fail_detail", property="failDetail", jdbcType=JdbcType.VARCHAR),
        @Result(column="auditer_name", property="auditerName", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="audit_time", property="auditTime", jdbcType=JdbcType.TIMESTAMP)
    })
    ShopAuditLog selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, shop_id, shop_phone, shop_address, \"business _license\", industry_license, ",
        "other_license, is_audit, fail_detail, auditer_name, create_time, audit_time",
        "from shop_audit_log"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="shop_phone", property="shopPhone", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_address", property="shopAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="business _license", property="businessLicense", jdbcType=JdbcType.VARCHAR),
        @Result(column="industry_license", property="industryLicense", jdbcType=JdbcType.VARCHAR),
        @Result(column="other_license", property="otherLicense", jdbcType=JdbcType.VARCHAR),
        @Result(column="is_audit", property="isAudit", jdbcType=JdbcType.TINYINT),
        @Result(column="fail_detail", property="failDetail", jdbcType=JdbcType.VARCHAR),
        @Result(column="auditer_name", property="auditerName", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="audit_time", property="auditTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<ShopAuditLog> selectAll();

    @Update({
        "update shop_audit_log",
        "set shop_id = #{shopId,jdbcType=INTEGER},",
          "shop_phone = #{shopPhone,jdbcType=VARCHAR},",
          "shop_address = #{shopAddress,jdbcType=VARCHAR},",
          "\"business _license\" = #{businessLicense,jdbcType=VARCHAR},",
          "industry_license = #{industryLicense,jdbcType=VARCHAR},",
          "other_license = #{otherLicense,jdbcType=VARCHAR},",
          "is_audit = #{isAudit,jdbcType=TINYINT},",
          "fail_detail = #{failDetail,jdbcType=VARCHAR},",
          "auditer_name = #{auditerName,jdbcType=VARCHAR},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "audit_time = #{auditTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ShopAuditLog record);
}