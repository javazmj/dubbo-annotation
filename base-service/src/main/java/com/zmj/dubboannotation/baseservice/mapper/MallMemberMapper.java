package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.baseservice.provider.MemberProvider;
import com.zmj.dubboannotation.serviceapi.model.MallMember;
import com.zmj.dubboannotation.serviceapi.vo.MemberVO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MallMemberMapper {
    @Delete({
            "delete from mall_member",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
            "insert into mall_member (id, mobile, ",
            "email, password, ",
            "nick_name, head, ",
            "member_integral, max_integral, ",
            "rank, salt, last_ip, ",
            "last_time, create_time, ",
            "update_time, del_flag)",
            "values (#{id,jdbcType=VARCHAR}, #{mobile,jdbcType=VARCHAR}, ",
            "#{email,jdbcType=VARCHAR}, #{password,jdbcType=VARCHAR}, ",
            "#{nickName,jdbcType=VARCHAR}, #{head,jdbcType=VARCHAR}, ",
            "#{memberIntegral,jdbcType=DECIMAL}, #{maxIntegral,jdbcType=DECIMAL}, ",
            "#{rank,jdbcType=TINYINT}, #{salt,jdbcType=VARCHAR}, #{lastIp,jdbcType=VARCHAR}, ",
            "#{lastTime,jdbcType=TIMESTAMP}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{updateTime,jdbcType=TIMESTAMP}, #{delFlag,jdbcType=TINYINT})"
    })
    int insert(MallMember record);

    @Select({
            "select",
            "id, mobile, email, password, nick_name, head, member_integral, max_integral, ",
            "rank, salt, last_ip, last_time, create_time, update_time, del_flag",
            "from mall_member",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="mobile", property="mobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
            @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
            @Result(column="nick_name", property="nickName", jdbcType=JdbcType.VARCHAR),
            @Result(column="head", property="head", jdbcType=JdbcType.VARCHAR),
            @Result(column="member_integral", property="memberIntegral", jdbcType=JdbcType.DECIMAL),
            @Result(column="max_integral", property="maxIntegral", jdbcType=JdbcType.DECIMAL),
            @Result(column="rank", property="rank", jdbcType=JdbcType.TINYINT),
            @Result(column="salt", property="salt", jdbcType=JdbcType.VARCHAR),
            @Result(column="last_ip", property="lastIp", jdbcType=JdbcType.VARCHAR),
            @Result(column="last_time", property="lastTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    MallMember selectByPrimaryKey(String id);



    @SelectProvider(type = MemberProvider.class,method = "selectByParam")
    List<MemberVO> selectByParam(MemberVO memberVO);

    @Select({
            "select",
            "id, mobile, email, password, nick_name, head, member_integral, max_integral, ",
            "rank, salt, last_ip, last_time, create_time, update_time, del_flag",
            "from mall_member"
    })
    @Results({
            @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="mobile", property="mobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
            @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
            @Result(column="nick_name", property="nickName", jdbcType=JdbcType.VARCHAR),
            @Result(column="head", property="head", jdbcType=JdbcType.VARCHAR),
            @Result(column="member_integral", property="memberIntegral", jdbcType=JdbcType.DECIMAL),
            @Result(column="max_integral", property="maxIntegral", jdbcType=JdbcType.DECIMAL),
            @Result(column="rank", property="rank", jdbcType=JdbcType.TINYINT),
            @Result(column="salt", property="salt", jdbcType=JdbcType.VARCHAR),
            @Result(column="last_ip", property="lastIp", jdbcType=JdbcType.VARCHAR),
            @Result(column="last_time", property="lastTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<MallMember> selectAll();


    @UpdateProvider(type = MemberProvider.class,method = "updateByParam")
    int updateByParam(MallMember record);


    @Select({
            "select",
            "mm.id, mm.mobile, mm.email, mm.password, mm.nick_name as nickName, mm.head, mm.member_integral as memberIntegral, mm.max_integral as maxIntegral, ",
            "mm.rank, mm.salt, mm.last_ip as lastIp, mm.last_time as lastTime, mm.create_time as createTime, mm.update_time as updateTime, mm.del_flag, ",
            "md.name, md.age, md.birthday, md.id_card as idCard, md.vocation, md.card_face as cardFace, ",
            "md.create_time as detailCreateTime, md.update_time as detailUpdateTime",
            "from mall_member mm left join member_detail md on mm.id = md.member_id",
            "where mm.email = #{email,jdbcType=VARCHAR}"
    })
    @ResultType(MemberVO.class)
    MemberVO findByEmail(@Param("email") String email);


    @Select({
            "select",
            "mm.id, mm.mobile, mm.email, mm.password, mm.nick_name as nickName, mm.head, mm.member_integral as memberIntegral, mm.max_integral as maxIntegral, ",
            "mm.rank, mm.salt, mm.last_ip as lastIp, mm.last_time as lastTime, mm.create_time as createTime, mm.update_time as updateTime, mm.del_flag, ",
            "md.name, md.age, md.birthday, md.id_card as idCard, md.vocation, md.card_face as cardFace, ",
            "md.create_time as detailCreateTime, md.update_time as detailUpdateTime",
            "from mall_member mm left join member_detail md on mm.id = md.member_id",
            "where mm.mobile = #{mobile,jdbcType=VARCHAR}"
    })
    @ResultType(MemberVO.class)
    MemberVO findByMobile(@Param("mobile") String mobile);
}