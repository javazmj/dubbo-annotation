package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmj.dubboannotation.baseservice.mapper.MemberImageMapper;
import com.zmj.dubboannotation.serviceapi.api.MemberImageService;
import com.zmj.dubboannotation.serviceapi.model.MemberImage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Created by: meijun
 * Date: 2018/09/14 22:54
 */
@Service(interfaceClass = MemberImageService.class)
@Component
@Slf4j
public class MemberImageServiceImpl extends ServiceImpl<MemberImageMapper, MemberImage> implements MemberImageService {


}
