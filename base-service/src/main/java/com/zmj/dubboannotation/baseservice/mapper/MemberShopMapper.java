package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MemberShop;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface MemberShopMapper {
    @Delete({
        "delete from member_shop",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into member_shop (id, member_id, ",
        "shop_name, shop_head, ",
        "shop_phone, shop_address, ",
        "shop_qr_code, is_audit, ",
        "is_self_support, create_time, ",
        "audit_time, del_flag)",
        "values (#{id,jdbcType=INTEGER}, #{memberId,jdbcType=VARCHAR}, ",
        "#{shopName,jdbcType=VARCHAR}, #{shopHead,jdbcType=VARCHAR}, ",
        "#{shopPhone,jdbcType=VARCHAR}, #{shopAddress,jdbcType=VARCHAR}, ",
        "#{shopQrCode,jdbcType=VARCHAR}, #{isAudit,jdbcType=TINYINT}, ",
        "#{isSelfSupport,jdbcType=TINYINT}, #{createTime,jdbcType=TIMESTAMP}, ",
        "#{auditTime,jdbcType=TIMESTAMP}, #{delFlag,jdbcType=TINYINT})"
    })
    int insert(MemberShop record);

    @Select({
        "select",
        "id, member_id, shop_name, shop_head, shop_phone, shop_address, shop_qr_code, ",
        "is_audit, is_self_support, create_time, audit_time, del_flag",
        "from member_shop",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_name", property="shopName", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_head", property="shopHead", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_phone", property="shopPhone", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_address", property="shopAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_qr_code", property="shopQrCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="is_audit", property="isAudit", jdbcType=JdbcType.TINYINT),
        @Result(column="is_self_support", property="isSelfSupport", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="audit_time", property="auditTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    MemberShop selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, member_id, shop_name, shop_head, shop_phone, shop_address, shop_qr_code, ",
        "is_audit, is_self_support, create_time, audit_time, del_flag",
        "from member_shop"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_name", property="shopName", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_head", property="shopHead", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_phone", property="shopPhone", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_address", property="shopAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="shop_qr_code", property="shopQrCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="is_audit", property="isAudit", jdbcType=JdbcType.TINYINT),
        @Result(column="is_self_support", property="isSelfSupport", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="audit_time", property="auditTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<MemberShop> selectAll();

    @Update({
        "update member_shop",
        "set member_id = #{memberId,jdbcType=VARCHAR},",
          "shop_name = #{shopName,jdbcType=VARCHAR},",
          "shop_head = #{shopHead,jdbcType=VARCHAR},",
          "shop_phone = #{shopPhone,jdbcType=VARCHAR},",
          "shop_address = #{shopAddress,jdbcType=VARCHAR},",
          "shop_qr_code = #{shopQrCode,jdbcType=VARCHAR},",
          "is_audit = #{isAudit,jdbcType=TINYINT},",
          "is_self_support = #{isSelfSupport,jdbcType=TINYINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "audit_time = #{auditTime,jdbcType=TIMESTAMP},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MemberShop record);
}