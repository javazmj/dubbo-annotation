package com.zmj.dubboannotation.baseservice.provider;

import com.zmj.dubboannotation.serviceapi.model.MallMember;
import com.zmj.dubboannotation.serviceapi.vo.MemberVO;
import org.apache.ibatis.jdbc.SQL;

/**
 * Created by: meijun
 * Date: 2018/10/29 20:29
 */
public class MemberProvider {

    private final String TABLE = "mall_member";

    public String updateByParam(MallMember mallMember) {
        //优化
        return new SQL(){
            {
                UPDATE(TABLE);
                if (null != mallMember.getMobile() ) {
                    SET("mobile = #{mobile,jdbcType=VARCHAR}");
                }
                if (null != mallMember.getEmail() ) {
                    SET("email = #{email,jdbcType=VARCHAR}");
                }
                if (null != mallMember.getPassword()) {
                    SET("password = #{password,jdbcType=VARCHAR}");
                }
                if (null != mallMember.getNickName()) {
                    SET("nick_name = #{nickName,jdbcType=VARCHAR}");
                }
                if (null != mallMember.getHead()) {
                    SET("head = #{head,jdbcType=VARCHAR}");
                }
                if (null != mallMember.getMaxIntegral()) {
                    SET("member_integral = #{memberIntegral,jdbcType=DECIMAL}");
                }
                if (null != mallMember.getMaxIntegral()) {
                    SET("max_integral = #{maxIntegral,jdbcType=DECIMAL}");
                }
                if (null != mallMember.getRank()) {
                    SET("rank = #{rank,jdbcType=TINYINT}");
                }
                if (null != mallMember.getLastIp()) {
                    SET("last_ip = #{lastIp,jdbcType=VARCHAR}");
                }
                if (null != mallMember.getLastTime()) {
                    SET("last_time = #{lastTime,jdbcType=TIMESTAMP}");
                }
                SET("update_time = NOW()");
                if (null != mallMember.getDelFlag()) {
                    SET("del_flag = #{delFlag,jdbcType=TINYINT}");
                }
                WHERE("id = #{id,jdbcType=BIGINT}");

            }
        }.toString();

    }

    public String selectByParam(MemberVO memberVO) {
        //优化
        return new SQL(){
            {
                SELECT("mm.id, mm.mobile, mm.email, mm.password, mm.nick_name as nickName, mm.head, mm.member_integral as memberIntegral, mm.max_integral as maxIntegral, " +
                        "mm.rank, mm.salt, mm.last_ip as lastIp, mm.last_time as lastTime, mm.create_time as createTime, mm.update_time as updateTime, mm.del_flag, " +
                        "md.name, md.age, md.birthday, md.id_card as idCard, md.vocation, md.card_face as cardFace, " +
                        "md.create_time as detailCreateTime, md.update_time as detailUpdateTime")
                .FROM("mall_member mm").LEFT_OUTER_JOIN("member_detail md on mm.id = md.member_id");

                if (null != memberVO.getId() ) {
                    WHERE("mm.id = #{id,jdbcType=BIGINT}");
                }
                if (null != memberVO.getMobile() ) {
                    WHERE("mm.mobile = #{mobile,jdbcType=VARCHAR}");
                }
                if (null != memberVO.getEmail() ) {
                    WHERE("mm.email = #{email,jdbcType=VARCHAR}");
                }
                if (null != memberVO.getNickName()) {
                    WHERE("mm.nick_name = #{nickName,jdbcType=VARCHAR}");
                }
                if (null != memberVO.getRank()) {
                    WHERE("mm.rank = #{rank,jdbcType=TINYINT}");
                }
                if (null != memberVO.getIdCard()) {
                    WHERE("md.id_card = #{idCard,jdbcType=TINYINT}");
                }
                WHERE("mm.del_flag = 0");
            }
        }.toString();

    }
}
