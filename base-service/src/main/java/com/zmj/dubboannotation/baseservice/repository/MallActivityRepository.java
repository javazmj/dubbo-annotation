package com.zmj.dubboannotation.baseservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MallActivity;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/09/15 12:17
 */
public interface MallActivityRepository extends JpaRepository<MallActivity,Integer> {

    @Override
    <S extends MallActivity> S save(S s);

    @Query(nativeQuery = true,
            value = "select m from MallActivity where m.del_flag = 0 and m.type = type?1 and m.start_time &gt= startTime?2 ")
    List<MallActivity> findValidAllActity(@Param("type")Integer type,@Param("startTime") Date startTime);
}
