package com.zmj.dubboannotation.baseservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MallMember;
import com.zmj.dubboannotation.serviceapi.vo.MemberVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by: meijun
 * Date: 2018/09/16 22:36
 */
public interface MallMemberRepository extends JpaRepository<MallMember,String> {

   /* @Transactional
    @Modifying(clearAutomatically =  true)
    @Query(value = "update MallMember u set u.member_integral = u.member_integral + :MemberIntegral  where u.id = :id ")
    int updateIntegralById(@Param("MemberIntegral") Integer integer, @Param("id") Integer id);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update MallMember u set u.member_integral= integral?1  +  u.member_integral  where  u.id = ?2",nativeQuery = true)
    int updateIntegralByIdSecond(Integer MemberIntegral, Integer id);
*/
    MallMember findByMobileAndPassword(@Param("mobile") String mobile,@Param("password") String password);


//    @Query(value = "select" +
//            "    mm.id, mm.mobile, mm.email, mm.password, mm.nick_name, mm.head, mm.member_integral, mm.max_integral," +
//            "    mm.rank, mm.salt, mm.last_ip, mm.last_time, mm.create_time, mm.update_time," +
//            "    md.name, md.age, md.birthday, md.id_card, md.vocation, md.card_face," +
//            "    md.create_time as detail_create_time, md.update_time as detail_update_time from mall_member mm left join member_detail md " +
//            "on mm.id = md.member_id where mm.email = :email and mm.password = :password",nativeQuery = true)
//    Object findByEmailAndPassword(@Param("email") String email, @Param("password") String password);

    MallMember findByMobile(@Param("mobile")String mobile);

    MallMember findByEmail(@Param("email") String email);

}
