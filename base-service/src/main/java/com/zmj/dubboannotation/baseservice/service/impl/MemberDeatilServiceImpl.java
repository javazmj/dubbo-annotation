package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.commoncore.dto.LoginDTO;
import com.zmj.commoncore.utils.MD5Util;
import com.zmj.dubboannotation.baseservice.mapper.MallMemberMapper;
import com.zmj.dubboannotation.baseservice.repository.MallMemberRepository;
import com.zmj.dubboannotation.baseservice.repository.MemberDetailRepository;
import com.zmj.dubboannotation.serviceapi.api.MemberDetailService;
import com.zmj.dubboannotation.serviceapi.api.MemberService;
import com.zmj.dubboannotation.serviceapi.model.MallMember;
import com.zmj.dubboannotation.serviceapi.model.MemberDetail;
import com.zmj.dubboannotation.serviceapi.vo.MemberVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/09/14 22:54
 */
@Service(interfaceClass = MemberDetailService.class)
@Component
@Slf4j
public class MemberDeatilServiceImpl implements MemberDetailService {


    @Autowired
    private MemberDetailRepository memberDetailRepository;


    @Override
    public MemberDetail findByMemberId(String memberId) {
        return memberDetailRepository.findByMemberId(memberId);
    }
}
