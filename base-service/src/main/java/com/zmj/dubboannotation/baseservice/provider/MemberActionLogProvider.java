package com.zmj.dubboannotation.baseservice.provider;

import com.zmj.dubboannotation.serviceapi.model.MemberActionLog;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by: meijun
 * Date: 2018/10/29 20:29
 */
public class MemberActionLogProvider {

    public String insertMore(Map map) {
        List<MemberActionLog> memberActionLogs = (List<MemberActionLog>) map.get("list");

        StringBuilder sb = new StringBuilder();
        sb.append("insert into member_action_log ( member_id, product_id, sku_product_id, action, create_time) values ");
        MessageFormat mf = new MessageFormat("( #'{'list[{0}].memberId,jdbcType=BIGINT}," +
                "#'{'list[{0}].productId,jdbcType=BIGINT}, #'{'list[{0}].skuProductId,jdbcType=BIGINT}, #'{'list[{0}].action,jdbcType=TINYINT}, " +
                "#'{'list[{0}].createTime,jdbcType=TIMESTAMP})" );
        for (int i = 0; i < memberActionLogs.size(); i++) {
            sb.append(mf.format(new Object[]{i}));
            if (i < memberActionLogs.size() - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

}
