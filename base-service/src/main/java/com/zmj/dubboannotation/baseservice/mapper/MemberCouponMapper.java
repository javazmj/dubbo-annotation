package com.zmj.dubboannotation.baseservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmj.dubboannotation.serviceapi.model.MemberCoupon;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

public interface MemberCouponMapper extends BaseMapper<MemberCoupon> {
    @Delete({
        "delete from member_coupon",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into member_coupon (id, shop_activity_id, ",
        "mall_activity_id, activity_name, ",
        "operation_price, derate_price, ",
        "is_superpose, is_use, ",
        "start_time, end_time, ",
        "create_time, update_time, ",
        "del_flag)",
        "values (#{id,jdbcType=INTEGER}, #{shopActivityId,jdbcType=INTEGER}, ",
        "#{mallActivityId,jdbcType=INTEGER}, #{activityName,jdbcType=VARCHAR}, ",
        "#{operationPrice,jdbcType=DECIMAL}, #{deratePrice,jdbcType=DECIMAL}, ",
        "#{isSuperpose,jdbcType=TINYINT}, #{isUse,jdbcType=TINYINT}, ",
        "#{startTime,jdbcType=TIMESTAMP}, #{endTime,jdbcType=TIMESTAMP}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{delFlag,jdbcType=TINYINT})"
    })
    int insert(MemberCoupon record);

    @Select({
        "select",
        "id, shop_activity_id, mall_activity_id, activity_name, operation_price, derate_price, ",
        "is_superpose, is_use, start_time, end_time, create_time, update_time, del_flag",
        "from member_coupon",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="shop_activity_id", property="shopActivityId", jdbcType=JdbcType.INTEGER),
        @Result(column="mall_activity_id", property="mallActivityId", jdbcType=JdbcType.INTEGER),
        @Result(column="activity_name", property="activityName", jdbcType=JdbcType.VARCHAR),
        @Result(column="operation_price", property="operationPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="derate_price", property="deratePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="is_superpose", property="isSuperpose", jdbcType=JdbcType.TINYINT),
        @Result(column="is_use", property="isUse", jdbcType=JdbcType.TINYINT),
        @Result(column="start_time", property="startTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    MemberCoupon selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, shop_activity_id, mall_activity_id, activity_name, operation_price, derate_price, ",
        "is_superpose, is_use, start_time, end_time, create_time, update_time, del_flag",
        "from member_coupon"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="shop_activity_id", property="shopActivityId", jdbcType=JdbcType.INTEGER),
        @Result(column="mall_activity_id", property="mallActivityId", jdbcType=JdbcType.INTEGER),
        @Result(column="activity_name", property="activityName", jdbcType=JdbcType.VARCHAR),
        @Result(column="operation_price", property="operationPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="derate_price", property="deratePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="is_superpose", property="isSuperpose", jdbcType=JdbcType.TINYINT),
        @Result(column="is_use", property="isUse", jdbcType=JdbcType.TINYINT),
        @Result(column="start_time", property="startTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<MemberCoupon> selectAll();

    @Update({
        "update member_coupon",
        "set shop_activity_id = #{shopActivityId,jdbcType=INTEGER},",
          "mall_activity_id = #{mallActivityId,jdbcType=INTEGER},",
          "activity_name = #{activityName,jdbcType=VARCHAR},",
          "operation_price = #{operationPrice,jdbcType=DECIMAL},",
          "derate_price = #{deratePrice,jdbcType=DECIMAL},",
          "is_superpose = #{isSuperpose,jdbcType=TINYINT},",
          "is_use = #{isUse,jdbcType=TINYINT},",
          "start_time = #{startTime,jdbcType=TIMESTAMP},",
          "end_time = #{endTime,jdbcType=TIMESTAMP},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MemberCoupon record);

    /**
     * 查询在有效时间段内 并且未使用过的优惠券
     * @param memberId
     * @param shopActivityId
     * @return
     */
    @Select({"select * from member_coupon where shop_activity_id = #{shopActivityId} and member_id = #{memberId} " +
            "and is_use = 2 and DATE_FORMAT(start_time, '%Y-%m-%d %H:%i:%s') <= DATE_FORMAT(now(), '%Y-%m-%d %H:%i:%s') " +
            "and DATE_FORMAT(end_time, '%Y-%m-%d %H:%i:%s') > DATE_FORMAT(now(), '%Y-%m-%d %H:%i:%s')" })
    MemberCoupon findByShopIdAndMemberId(@Param("memberId") String memberId,@Param("shopActivityId")Integer shopActivityId);

    @Select({"select * from member_coupon where mall_activity_id = #{mallActivityId} and member_id = #{memberId} " +
            "and is_use = 2 and DATE_FORMAT(start_time, '%Y-%m-%d %H:%i:%s') <= DATE_FORMAT(now(), '%Y-%m-%d %H:%i:%s') " +
            "and DATE_FORMAT(end_time, '%Y-%m-%d %H:%i:%s') > DATE_FORMAT(now(), '%Y-%m-%d %H:%i:%s')" })
    MemberCoupon findByMallIdAndMemberId(@Param("memberId") String memberId, @Param("mallActivityId")Integer mallActivityId);
}