package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.baseservice.mapper.MemberAddressMapper;
import com.zmj.dubboannotation.serviceapi.api.MemberAddressService;
import com.zmj.dubboannotation.serviceapi.model.MemberAddress;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by: meijun
 * Date: 2018/11/17 9:53
 */
@Service(interfaceClass = MemberAddressService.class )
@Component
@Slf4j
public class MemberAddressServiceImpl implements MemberAddressService {

    @Autowired
    private MemberAddressMapper memberAddressMapper;

    @Override
    public MemberAddress findByIdAndMemberId(Integer id,String memberId) {
        return memberAddressMapper.findByIdAndMemberId(id,memberId);
    }
}
