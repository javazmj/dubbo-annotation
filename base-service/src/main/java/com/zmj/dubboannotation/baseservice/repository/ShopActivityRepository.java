package com.zmj.dubboannotation.baseservice.repository;

import com.zmj.dubboannotation.serviceapi.model.ShopActivity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by: meijun
 * Date: 2018/09/15 12:17
 */
public interface ShopActivityRepository extends JpaRepository<ShopActivity,Integer> {

    @Override
    <S extends ShopActivity> S save(S s);

}
