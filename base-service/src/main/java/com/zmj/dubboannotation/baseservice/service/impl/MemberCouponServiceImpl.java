package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmj.dubboannotation.baseservice.mapper.MemberCouponMapper;
import com.zmj.dubboannotation.serviceapi.api.MemberCouponService;
import com.zmj.dubboannotation.serviceapi.model.MemberCoupon;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/11/16 15:12
 * @Email: 536304123@QQ.COM
 */
@Service(interfaceClass = MemberCouponService.class )
@Component
@Slf4j
public class MemberCouponServiceImpl extends ServiceImpl<MemberCouponMapper,MemberCoupon> implements MemberCouponService {

    @Autowired
    private MemberCouponMapper memberCouponMapper;

    @Override
    public MemberCoupon findByShopIdAndMemberId(String memberId,Integer shopActivityId) {
        return memberCouponMapper.findByShopIdAndMemberId(memberId, shopActivityId);
    }

    @Override
    public MemberCoupon findByMallIdAndMemberId(String memberId, Integer mallActivityId) {
        return memberCouponMapper.findByMallIdAndMemberId(memberId,mallActivityId);
    }

}
