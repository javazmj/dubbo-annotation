package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ShopProductFreightConfig;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

public interface ShopProductFreightConfigMapper {
    @Delete({
        "delete from shop_product_freight_config",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into shop_product_freight_config (id, shop_id, ",
        "freight, no_area_codes, ",
        "add_area_codes, add_freight, ",
        "create_time, update_time, ",
        "del_flag, derate_freight)",
        "values (#{id,jdbcType=INTEGER}, #{shopId,jdbcType=INTEGER}, ",
        "#{freight,jdbcType=DECIMAL}, #{noAreaCodes,jdbcType=VARCHAR}, ",
        "#{addAreaCodes,jdbcType=VARCHAR}, #{addFreight,jdbcType=DECIMAL}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{delFlag,jdbcType=TINYINT}, #{derateFreight,jdbcType=DECIMAL})"
    })
    int insert(ShopProductFreightConfig record);

    @Select({
        "select",
        "id, shop_id, freight, no_area_codes, add_area_codes, add_freight, create_time, ",
        "update_time, del_flag, derate_freight",
        "from shop_product_freight_config",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="freight", property="freight", jdbcType=JdbcType.DECIMAL),
        @Result(column="no_area_codes", property="noAreaCodes", jdbcType=JdbcType.VARCHAR),
        @Result(column="add_area_codes", property="addAreaCodes", jdbcType=JdbcType.VARCHAR),
        @Result(column="add_freight", property="addFreight", jdbcType=JdbcType.DECIMAL),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT),
        @Result(column="derate_freight", property="derateFreight", jdbcType=JdbcType.DECIMAL)
    })
    ShopProductFreightConfig selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, shop_id, freight, no_area_codes, add_area_codes, add_freight, create_time, ",
        "update_time, del_flag, derate_freight",
        "from shop_product_freight_config"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="freight", property="freight", jdbcType=JdbcType.DECIMAL),
        @Result(column="no_area_codes", property="noAreaCodes", jdbcType=JdbcType.VARCHAR),
        @Result(column="add_area_codes", property="addAreaCodes", jdbcType=JdbcType.VARCHAR),
        @Result(column="add_freight", property="addFreight", jdbcType=JdbcType.DECIMAL),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT),
        @Result(column="derate_freight", property="derateFreight", jdbcType=JdbcType.DECIMAL)
    })
    List<ShopProductFreightConfig> selectAll();

    @Update({
        "update shop_product_freight_config",
        "set shop_id = #{shopId,jdbcType=INTEGER},",
          "freight = #{freight,jdbcType=DECIMAL},",
          "no_area_codes = #{noAreaCodes,jdbcType=VARCHAR},",
          "add_area_codes = #{addAreaCodes,jdbcType=VARCHAR},",
          "add_freight = #{addFreight,jdbcType=DECIMAL},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "del_flag = #{delFlag,jdbcType=TINYINT},",
          "derate_freight = #{derateFreight,jdbcType=DECIMAL}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ShopProductFreightConfig record);

    @Select("select * from shop_product_freight_config where del_flag = 0 and shop_id = #{shopId}")
    ShopProductFreightConfig findByShopId(@Param("shopId") Integer shopId);

}