package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.commoncore.dto.LoginDTO;
import com.zmj.commoncore.utils.MD5Util;
import com.zmj.dubboannotation.baseservice.mapper.MallMemberMapper;
import com.zmj.dubboannotation.baseservice.repository.MallMemberRepository;
import com.zmj.dubboannotation.serviceapi.api.MemberService;
import com.zmj.dubboannotation.serviceapi.model.MallMember;
import com.zmj.dubboannotation.serviceapi.vo.MemberVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/09/14 22:54
 */
@Service(interfaceClass = MemberService.class)
@Component
@Slf4j
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MallMemberMapper memberMapper;

    @Autowired
    private MallMemberRepository memberRepository;

    @Override
    public List<MallMember> findAll() {
        return memberMapper.selectAll();
    }

    @Override
    public MemberVO findByMobileAndPassord(LoginDTO loginDTO) {

        MemberVO member = memberMapper.findByMobile(loginDTO.getMobile());

        if(null != member) {
            String md5Str = MD5Util.getMD5Str(loginDTO.getPassword(), member.getSalt());
            if(StringUtils.equals(md5Str,member.getPassword())) {
                return member;
            }
        }
        return null;
    }

    @Override
    public MemberVO findByEmailAndPassord(LoginDTO loginDTO) {

        MemberVO member = memberMapper.findByEmail(loginDTO.getEmail());

        if(null != member) {
            String md5Str = MD5Util.getMD5Str(loginDTO.getPassword(), member.getSalt());
            if(StringUtils.equals(md5Str,member.getPassword())) {
                return member;
            }
        }
        return null;
    }

    @Override
    public MallMember findByMobile(String mobile) {
        return memberRepository.findByMobile(mobile);
    }

    @Override
    public MallMember findByEmail(String email) {
        return memberRepository.findByEmail(email);
    }

    @Override
    public int updateByParam(MemberVO memberVO) {
        MallMember member = new MallMember();
        BeanUtils.copyProperties(memberVO,member);
        return memberMapper.updateByParam(member);
    }

    @Override
    public List<MemberVO> selectByParam(MemberVO memberVO) {
        return memberMapper.selectByParam(memberVO);
    }

    @Override
    public MallMember selectByPrimaryKey(String id) {
        return memberMapper.selectByPrimaryKey(id);
    }

}
