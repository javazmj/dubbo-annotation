package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MemberAddress;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

public interface MemberAddressMapper {
    @Delete({
        "delete from member_address",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into member_address (id, member_id, ",
        "receiver_name, receiver_mobile, ",
        "receiver_address_code, receiver_address, ",
        "receiver_address_info, checked, ",
        "create_time, update_time, ",
        "del_flag)",
        "values (#{id,jdbcType=INTEGER}, #{memberId,jdbcType=VARCHAR}, ",
        "#{receiverName,jdbcType=VARCHAR}, #{receiverMobile,jdbcType=VARCHAR}, ",
        "#{receiverAddressCode,jdbcType=VARCHAR}, #{receiverAddress,jdbcType=VARCHAR}, ",
        "#{receiverAddressInfo,jdbcType=VARCHAR}, #{checked,jdbcType=TINYINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{delFlag,jdbcType=TINYINT})"
    })
    int insert(MemberAddress record);

    @Select({
        "select",
        "id, member_id, receiver_name, receiver_mobile, receiver_address_code, receiver_address, ",
        "receiver_address_info, checked, create_time, update_time, del_flag",
        "from member_address",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_name", property="receiverName", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_mobile", property="receiverMobile", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_address_code", property="receiverAddressCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_address", property="receiverAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_address_info", property="receiverAddressInfo", jdbcType=JdbcType.VARCHAR),
        @Result(column="checked", property="checked", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    MemberAddress selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, member_id, receiver_name, receiver_mobile, receiver_address_code, receiver_address, ",
        "receiver_address_info, checked, create_time, update_time, del_flag",
        "from member_address"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_name", property="receiverName", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_mobile", property="receiverMobile", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_address_code", property="receiverAddressCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_address", property="receiverAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_address_info", property="receiverAddressInfo", jdbcType=JdbcType.VARCHAR),
        @Result(column="checked", property="checked", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<MemberAddress> selectAll();

    @Update({
        "update member_address",
        "set member_id = #{memberId,jdbcType=VARCHAR},",
          "receiver_name = #{receiverName,jdbcType=VARCHAR},",
          "receiver_mobile = #{receiverMobile,jdbcType=VARCHAR},",
          "receiver_address_code = #{receiverAddressCode,jdbcType=VARCHAR},",
          "receiver_address = #{receiverAddress,jdbcType=VARCHAR},",
          "receiver_address_info = #{receiverAddressInfo,jdbcType=VARCHAR},",
          "checked = #{checked,jdbcType=TINYINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MemberAddress record);

    @Select("select * from member_address where del_flag = 0 and id = #{id} and member_id = #{memberId}")
    MemberAddress findByIdAndMemberId(@Param("id") Integer id,@Param("memberId") String memberId);

}