package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.baseservice.mapper.MemberLoginLogMapper;
import com.zmj.dubboannotation.serviceapi.api.MemberLoginLogService;
import com.zmj.dubboannotation.serviceapi.model.MemberLoginLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by: meijun
 * Date: 2018/10/29 22:11
 */
@Service(interfaceClass = MemberLoginLogService.class)
@Component
@Slf4j
public class MemberLoginLogServiceImpl implements MemberLoginLogService {

    @Autowired
    private MemberLoginLogMapper memberLoginLogMapper;

    @Override
    @Transactional
    public int insert(MemberLoginLog record) {
        return memberLoginLogMapper.insert(record);
    }
}
