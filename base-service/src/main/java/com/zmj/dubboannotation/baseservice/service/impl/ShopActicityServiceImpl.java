package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.baseservice.mapper.ShopActivityMapper;
import com.zmj.dubboannotation.baseservice.repository.ShopActivityRepository;
import com.zmj.dubboannotation.serviceapi.api.ShopActivityService;
import com.zmj.dubboannotation.serviceapi.model.ShopActivity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Service(interfaceClass = ShopActivityService.class)
@Component
@Slf4j
public class ShopActicityServiceImpl implements ShopActivityService {

    @Autowired
    private ShopActivityRepository shopActivityRepository;

    @Autowired
    private ShopActivityMapper shopActivityMapper;


    @Override
    @Transactional
    public ShopActivity save(ShopActivity shopActivity) {
        return shopActivityRepository.save(shopActivity);
    }

    @Override
    public List<ShopActivity> findAll() {
        return shopActivityRepository.findAll();
    }

    @Override
    public List<ShopActivity> findValidAllActity(Integer shopId,Integer type, Date startTime) {
        return shopActivityMapper.findValidAllActity(shopId,type,startTime);
    }

    @Override
    public ShopActivity findMaxActivity(Integer shopId,Integer type, Date startTime, BigDecimal shopTotalPrice) {
        return shopActivityMapper.findMaxActivity(shopId,type,startTime,shopTotalPrice);
    }
}
