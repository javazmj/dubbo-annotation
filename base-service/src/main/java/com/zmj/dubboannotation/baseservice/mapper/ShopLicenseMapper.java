package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ShopLicense;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface ShopLicenseMapper {
    @Delete({
        "delete from shop_license",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into shop_license (id, shop_id, ",
        "license_url, license_desc, ",
        "sort, create_time, ",
        "update_time, del_flag)",
        "values (#{id,jdbcType=INTEGER}, #{shopId,jdbcType=INTEGER}, ",
        "#{licenseUrl,jdbcType=VARCHAR}, #{licenseDesc,jdbcType=TINYINT}, ",
        "#{sort,jdbcType=TINYINT}, #{createTime,jdbcType=TIMESTAMP}, ",
        "#{updateTime,jdbcType=TIMESTAMP}, #{delFlag,jdbcType=TINYINT})"
    })
    int insert(ShopLicense record);

    @Select({
        "select",
        "id, shop_id, license_url, license_desc, sort, create_time, update_time, del_flag",
        "from shop_license",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="license_url", property="licenseUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="license_desc", property="licenseDesc", jdbcType=JdbcType.TINYINT),
        @Result(column="sort", property="sort", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    ShopLicense selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, shop_id, license_url, license_desc, sort, create_time, update_time, del_flag",
        "from shop_license"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="license_url", property="licenseUrl", jdbcType=JdbcType.VARCHAR),
        @Result(column="license_desc", property="licenseDesc", jdbcType=JdbcType.TINYINT),
        @Result(column="sort", property="sort", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<ShopLicense> selectAll();

    @Update({
        "update shop_license",
        "set shop_id = #{shopId,jdbcType=INTEGER},",
          "license_url = #{licenseUrl,jdbcType=VARCHAR},",
          "license_desc = #{licenseDesc,jdbcType=TINYINT},",
          "sort = #{sort,jdbcType=TINYINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ShopLicense record);
}