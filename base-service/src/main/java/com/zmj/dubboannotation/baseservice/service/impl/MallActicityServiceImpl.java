package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.baseservice.mapper.MallActivityMapper;
import com.zmj.dubboannotation.baseservice.repository.MallActivityRepository;
import com.zmj.dubboannotation.serviceapi.api.MallActivityService;
import com.zmj.dubboannotation.serviceapi.model.MallActivity;
import com.zmj.dubboannotation.serviceapi.model.ShopActivity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Service(interfaceClass = MallActivityService.class)
@Component
@Slf4j
public class MallActicityServiceImpl implements MallActivityService {

    @Autowired
    private MallActivityRepository mallActivityRepository;

    @Autowired
    private MallActivityMapper mallActivityMapper;


    @Override
    public MallActivity save(MallActivity mallActivity) {
        return mallActivityRepository.save(mallActivity);
    }

    @Override
    public List<MallActivity> findAll() {
        return mallActivityRepository.findAll();
    }

    @Override
    public List<MallActivity> findValidAllActity(Integer type, Date startTime) {
        return mallActivityMapper.findValidAllActity(type,startTime);
    }

    @Override
    public ShopActivity findMaxActivity(Integer type, BigDecimal mallTotalPrice) {
        return mallActivityMapper.findMaxActivity(type,mallTotalPrice);
    }
}
