package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zmj.dubboannotation.baseservice.mapper.ShopProductFreightConfigMapper;
import com.zmj.dubboannotation.serviceapi.api.FreightConfigService;
import com.zmj.dubboannotation.serviceapi.model.ShopProductFreightConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by: meijun
 * Date: 2018/11/17 10:24
 */
@Service(interfaceClass = FreightConfigService.class)
@Component
@Slf4j
public class FreightConfigServiceImpl implements FreightConfigService {

    @Autowired
    private ShopProductFreightConfigMapper freightConfigMapper;

    @Override
    public ShopProductFreightConfig findByShopId(Integer shopId) {
        /*QueryWrapper<ShopProductFreightConfigMapper> wrapper = new QueryWrapper<>();
        wrapper.eq()
        freightConfigMapper.selectOne();*/
        return freightConfigMapper.findByShopId(shopId);
    }
}
