package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.baseservice.mapper.MemberActionLogMapper;
import com.zmj.dubboannotation.serviceapi.api.MemberActionLogService;
import com.zmj.dubboannotation.serviceapi.model.MemberActionLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/11/2 14:20
 * @Email: 536304123@QQ.COM
 */
@Service(interfaceClass = MemberActionLogService.class )
@Component
@Slf4j
public class MemberActionLogServiceImpl implements MemberActionLogService {

    @Autowired
    private MemberActionLogMapper memberActionLogMapper;

    @Override
    @Transactional
    public int insertMore(List<MemberActionLog> list) {
        return memberActionLogMapper.insertMore(list);
    }

    @Override
    public List<MemberActionLog> findByMemberIdAndAction(String memberId, int action) {
        return memberActionLogMapper.findByMemberIdAndAction(memberId,action);
    }
}
