package com.zmj.dubboannotation.baseservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmj.dubboannotation.serviceapi.model.MemberImage;
import java.util.List;

import com.zmj.dubboannotation.serviceapi.model.Test;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

public interface MemberImageMapper extends BaseMapper<MemberImage> {

}