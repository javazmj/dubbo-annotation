package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MallActivity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.zmj.dubboannotation.serviceapi.model.ShopActivity;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

public interface MallActivityMapper {
    @Delete({
        "delete from mall_activity",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into mall_activity (id, name, ",
        "type, nums, use_category_id, ",
        "not_use_category_id, operation_price, ",
        "derate_price, is_superpose, ",
        "start_time, end_time, ",
        "create_time, update_time, ",
        "create_by, update_by, ",
        "del_flag)",
        "values (#{id,jdbcType=INTEGER}, #{name,jdbcType=VARCHAR}, ",
        "#{type,jdbcType=TINYINT}, #{nums,jdbcType=INTEGER}, #{useCategoryId,jdbcType=VARCHAR}, ",
        "#{notUseCategoryId,jdbcType=VARCHAR}, #{operationPrice,jdbcType=DECIMAL}, ",
        "#{deratePrice,jdbcType=DECIMAL}, #{isSuperpose,jdbcType=TINYINT}, ",
        "#{startTime,jdbcType=TIMESTAMP}, #{endTime,jdbcType=TIMESTAMP}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{createBy,jdbcType=VARCHAR}, #{updateBy,jdbcType=TIMESTAMP}, ",
        "#{delFlag,jdbcType=TINYINT})"
    })
    int insert(MallActivity record);

    @Select({
        "select",
        "id, name, type, nums, use_category_id, not_use_category_id, operation_price, ",
        "derate_price, is_superpose, start_time, end_time, create_time, update_time, ",
        "create_by, update_by, del_flag",
        "from mall_activity",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="type", property="type", jdbcType=JdbcType.TINYINT),
        @Result(column="nums", property="nums", jdbcType=JdbcType.INTEGER),
        @Result(column="use_category_id", property="useCategoryId", jdbcType=JdbcType.VARCHAR),
        @Result(column="not_use_category_id", property="notUseCategoryId", jdbcType=JdbcType.VARCHAR),
        @Result(column="operation_price", property="operationPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="derate_price", property="deratePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="is_superpose", property="isSuperpose", jdbcType=JdbcType.TINYINT),
        @Result(column="start_time", property="startTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    MallActivity selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, name, type, nums, use_category_id, not_use_category_id, operation_price, ",
        "derate_price, is_superpose, start_time, end_time, create_time, update_time, ",
        "create_by, update_by, del_flag",
        "from mall_activity"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="type", property="type", jdbcType=JdbcType.TINYINT),
        @Result(column="nums", property="nums", jdbcType=JdbcType.INTEGER),
        @Result(column="use_category_id", property="useCategoryId", jdbcType=JdbcType.VARCHAR),
        @Result(column="not_use_category_id", property="notUseCategoryId", jdbcType=JdbcType.VARCHAR),
        @Result(column="operation_price", property="operationPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="derate_price", property="deratePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="is_superpose", property="isSuperpose", jdbcType=JdbcType.TINYINT),
        @Result(column="start_time", property="startTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<MallActivity> selectAll();

    @Update({
        "update mall_activity",
        "set name = #{name,jdbcType=VARCHAR},",
          "type = #{type,jdbcType=TINYINT},",
          "nums = #{nums,jdbcType=INTEGER},",
          "use_category_id = #{useCategoryId,jdbcType=VARCHAR},",
          "not_use_category_id = #{notUseCategoryId,jdbcType=VARCHAR},",
          "operation_price = #{operationPrice,jdbcType=DECIMAL},",
          "derate_price = #{deratePrice,jdbcType=DECIMAL},",
          "is_superpose = #{isSuperpose,jdbcType=TINYINT},",
          "start_time = #{startTime,jdbcType=TIMESTAMP},",
          "end_time = #{endTime,jdbcType=TIMESTAMP},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "create_by = #{createBy,jdbcType=VARCHAR},",
          "update_by = #{updateBy,jdbcType=TIMESTAMP},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MallActivity record);

    @Select({"select * from mall_activity where del_flag = 0 and type = #{type} and DATE_FORMAT(start_time, '%Y-%m-%d %H:%i:%s') <= DATE_FORMAT(#{startTime}, '%Y-%m-%d %H:%i:%s') " +
            "and DATE_FORMAT(end_time, '%Y-%m-%d %H:%i:%s') > DATE_FORMAT(#{startTime}, '%Y-%m-%d %H:%i:%s')"})
    List<MallActivity> findValidAllActity(@Param("type") Integer type, @Param("startTime") Date startTime);

    @Select({"select * from shop_activity where operation_price = ( select MAX(operation_price) from shop_activity" +
            "            where shop_id = 1 and del_flag = 0 and is_audit = 1 and type = 1 and DATE_FORMAT(start_time, '%Y-%m-%d %H:%i:%s') <= DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s') " +
            "             and DATE_FORMAT(end_time, '%Y-%m-%d %H:%i:%s') > DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s') and operation_price <= #{mallTotalPrice})"})
    ShopActivity findMaxActivity(@Param("type")Integer type, @Param("mallTotalPrice")BigDecimal mallTotalPrice);
}