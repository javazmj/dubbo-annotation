package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MemberLoginLog;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface MemberLoginLogMapper {
    @Delete({
        "delete from member_login_log",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into member_login_log (id, member_id, ",
        "login_time, login_ip, ",
        "login_type)",
        "values (#{id,jdbcType=INTEGER}, #{memberId,jdbcType=VARCHAR}, ",
        "#{loginTime,jdbcType=TIMESTAMP}, #{loginIp,jdbcType=VARCHAR}, ",
        "#{loginType,jdbcType=VARCHAR})"
    })
    int insert(MemberLoginLog record);

    @Select({
        "select",
        "id, member_id, login_time, login_ip, login_type",
        "from member_login_log",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="login_time", property="loginTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="login_ip", property="loginIp", jdbcType=JdbcType.VARCHAR),
        @Result(column="login_type", property="loginType", jdbcType=JdbcType.VARCHAR)
    })
    MemberLoginLog selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, member_id, login_time, login_ip, login_type",
        "from member_login_log"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="login_time", property="loginTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="login_ip", property="loginIp", jdbcType=JdbcType.VARCHAR),
        @Result(column="login_type", property="loginType", jdbcType=JdbcType.VARCHAR)
    })
    List<MemberLoginLog> selectAll();

    @Update({
        "update member_login_log",
        "set member_id = #{memberId,jdbcType=VARCHAR},",
          "login_time = #{loginTime,jdbcType=TIMESTAMP},",
          "login_ip = #{loginIp,jdbcType=VARCHAR},",
          "login_type = #{loginType,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MemberLoginLog record);
}