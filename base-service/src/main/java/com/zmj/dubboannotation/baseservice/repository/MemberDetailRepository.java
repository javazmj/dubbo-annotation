package com.zmj.dubboannotation.baseservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MemberDetail;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by: meijun
 * Date: 2018/09/16 22:36
 */
public interface MemberDetailRepository extends JpaRepository<MemberDetail,Integer> {

    MemberDetail findByMemberId(@Param("memberId") String memberId);

}
