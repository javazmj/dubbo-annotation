package com.zmj.dubboannotation.baseservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MemberShop;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by: meijun
 * Date: 2018/09/16 22:36
 */
public interface MemberShopRepository extends JpaRepository<MemberShop,Integer> {

    MemberShop findByMemberId(@Param("memberId") String memberId);

}
