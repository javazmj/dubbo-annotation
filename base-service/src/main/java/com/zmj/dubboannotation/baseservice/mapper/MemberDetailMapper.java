package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MemberDetail;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface MemberDetailMapper {
    @Delete({
        "delete from member_detail",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into member_detail (id, member_id, ",
        "name, age, birthday, ",
        "id_card, vocation, ",
        "card_face, card_back, ",
        "create_time, update_time)",
        "values (#{id,jdbcType=VARCHAR}, #{memberId,jdbcType=INTEGER}, ",
        "#{name,jdbcType=VARCHAR}, #{age,jdbcType=INTEGER}, #{birthday,jdbcType=DATE}, ",
        "#{idCard,jdbcType=VARCHAR}, #{vocation,jdbcType=VARCHAR}, ",
        "#{cardFace,jdbcType=VARCHAR}, #{cardBack,jdbcType=VARCHAR}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP})"
    })
    int insert(MemberDetail record);

    @Select({
        "select",
        "id, member_id, name, age, birthday, id_card, vocation, card_face, card_back, ",
        "create_time, update_time",
        "from member_detail",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.INTEGER),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="age", property="age", jdbcType=JdbcType.INTEGER),
        @Result(column="birthday", property="birthday", jdbcType=JdbcType.DATE),
        @Result(column="id_card", property="idCard", jdbcType=JdbcType.VARCHAR),
        @Result(column="vocation", property="vocation", jdbcType=JdbcType.VARCHAR),
        @Result(column="card_face", property="cardFace", jdbcType=JdbcType.VARCHAR),
        @Result(column="card_back", property="cardBack", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP)
    })
    MemberDetail selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, member_id, name, age, birthday, id_card, vocation, card_face, card_back, ",
        "create_time, update_time",
        "from member_detail"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.INTEGER),
        @Result(column="name", property="name", jdbcType=JdbcType.VARCHAR),
        @Result(column="age", property="age", jdbcType=JdbcType.INTEGER),
        @Result(column="birthday", property="birthday", jdbcType=JdbcType.DATE),
        @Result(column="id_card", property="idCard", jdbcType=JdbcType.VARCHAR),
        @Result(column="vocation", property="vocation", jdbcType=JdbcType.VARCHAR),
        @Result(column="card_face", property="cardFace", jdbcType=JdbcType.VARCHAR),
        @Result(column="card_back", property="cardBack", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<MemberDetail> selectAll();

    @Update({
        "update member_detail",
        "set member_id = #{memberId,jdbcType=VARCHAR},",
          "name = #{name,jdbcType=VARCHAR},",
          "age = #{age,jdbcType=INTEGER},",
          "birthday = #{birthday,jdbcType=DATE},",
          "id_card = #{idCard,jdbcType=VARCHAR},",
          "vocation = #{vocation,jdbcType=VARCHAR},",
          "card_face = #{cardFace,jdbcType=VARCHAR},",
          "card_back = #{cardBack,jdbcType=VARCHAR},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MemberDetail record);
}