package com.zmj.dubboannotation.baseservice.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.baseservice.mapper.MemberShopMapper;
import com.zmj.dubboannotation.baseservice.repository.MemberShopRepository;
import com.zmj.dubboannotation.serviceapi.api.ShopService;
import com.zmj.dubboannotation.serviceapi.model.MemberShop;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by: meijun
 * Date: 2018/10/31 22:29
 */
@Service(interfaceClass = ShopService.class)
@Component
@Slf4j
public class ShopServiceImpl implements ShopService {

    @Autowired
    private MemberShopMapper shopMapper;

    @Autowired
    private MemberShopRepository memberShopRepository;

    @Override
    public MemberShop findById(Integer id) {
        return shopMapper.selectByPrimaryKey(id);
    }

    @Override
    public MemberShop findByMemberId(String memberId) {
        return memberShopRepository.findByMemberId(memberId);
    }
}
