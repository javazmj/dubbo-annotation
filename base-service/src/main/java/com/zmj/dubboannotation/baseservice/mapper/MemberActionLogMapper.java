package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.baseservice.provider.MemberActionLogProvider;
import com.zmj.dubboannotation.serviceapi.model.MemberActionLog;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

public interface MemberActionLogMapper {
    @Delete({
        "delete from member_action_log",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into member_action_log (id, member_id, ",
        "product_id, sku_product_id, ",
        "action, create_time)",
        "values (#{id,jdbcType=INTEGER}, #{memberId,jdbcType=VARCHAR}, ",
        "#{productId,jdbcType=VARCHAR}, #{skuProductId,jdbcType=VARCHAR}, ",
        "#{action,jdbcType=TINYINT}, #{createTime,jdbcType=TIMESTAMP})"
    })
    int insert(MemberActionLog record);

    @Select({
        "select",
        "id, member_id, product_id, sku_product_id, action, create_time",
        "from member_action_log",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="sku_product_id", property="skuProductId", jdbcType=JdbcType.VARCHAR),
        @Result(column="action", property="action", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    MemberActionLog selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, member_id, product_id, sku_product_id, action, create_time",
        "from member_action_log"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="sku_product_id", property="skuProductId", jdbcType=JdbcType.VARCHAR),
        @Result(column="action", property="action", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<MemberActionLog> selectAll();

    @Update({
        "update member_action_log",
        "set member_id = #{memberId,jdbcType=VARCHAR},",
          "product_id = #{productId,jdbcType=VARCHAR},",
          "sku_product_id = #{skuProductId,jdbcType=VARCHAR},",
          "action = #{action,jdbcType=TINYINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MemberActionLog record);

    @InsertProvider(type = MemberActionLogProvider.class,method = "insertMore")
    int insertMore(@Param("list") List<MemberActionLog> list);

    @Select({
            "select",
            "id, member_id, product_id, sku_product_id, action, create_time",
            "from member_action_log where member_id=#{memberId,jdbcType=VARCHAR} and action = #{action,jdbcType=TINYINT]"
    })
    @Results({
            @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
            @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
            @Result(column="sku_product_id", property="skuProductId", jdbcType=JdbcType.VARCHAR),
            @Result(column="action", property="action", jdbcType=JdbcType.TINYINT),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<MemberActionLog> findByMemberIdAndAction(String memberId, int action);
}