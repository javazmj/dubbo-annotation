package com.zmj.dubboannotation.baseservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmj.dubboannotation.serviceapi.model.Test;

/**
 * <p>
 * 测试CRUD表 Mapper 接口
 * </p>
 *
 * @author zmj
 * @since 2018-11-12
 */
public interface TestMapper extends BaseMapper<Test> {

}
