package com.zmj.dubboannotation.baseservice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmj.dubboannotation.baseservice.mapper.TestMapper;
import com.zmj.dubboannotation.serviceapi.api.ITestService;
import com.zmj.dubboannotation.serviceapi.model.Test;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测试CRUD表 服务实现类
 * </p>
 *
 * @author zmj
 * @since 2018-11-12
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements ITestService {

}
