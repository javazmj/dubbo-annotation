package com.zmj.dubboannotation.baseservice.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Created by: meijun
 * Date: 2018/11/12 20:45
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TestMapperTest {

    @Autowired
    private  TestMapper testMapper;

    @Test
    public void create () {
        com.zmj.dubboannotation.serviceapi.model.Test test = new com.zmj.dubboannotation.serviceapi.model.Test();
        test.setId(UUID.randomUUID().version());
        test.setName("zmj");
        test.setAge(11);
        test.setCreateTime(LocalDate.now().atStartOfDay());

        int insert = testMapper.insert(test);
        Assert.assertEquals(insert,1);
    }

    @Test
    public void update () {
        com.zmj.dubboannotation.serviceapi.model.Test test = new com.zmj.dubboannotation.serviceapi.model.Test();
        test.setId(4);
        test.setName("zmj344");
        test.setAge(43);
        test.setCreateTime(LocalDate.now().atStartOfDay());

        int insert = testMapper.updateById(test);
        Assert.assertEquals(insert,1);
    }

    @Test
    public void get () {

        com.zmj.dubboannotation.serviceapi.model.Test test = testMapper.selectById(4);
        Assert.assertNotNull(test);
    }

    @Test
    public void find () {

        com.zmj.dubboannotation.serviceapi.model.Test test = testMapper.
                selectOne(new QueryWrapper<com.zmj.dubboannotation.serviceapi.model.Test>().lambda().
                        ge(com.zmj.dubboannotation.serviceapi.model.Test :: getAge,4));
        Assert.assertNotNull(test);
    }

}