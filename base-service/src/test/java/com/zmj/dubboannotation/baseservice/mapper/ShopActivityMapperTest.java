package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ShopActivity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

/**
 *
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/11/16 17:19
 * @Email: 536304123@QQ.COM
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ShopActivityMapperTest {

    @Autowired
    private  ShopActivityMapper shopActivityMapper;

    @Test
    public void findMaxActivity() {

        ShopActivity maxActivity = shopActivityMapper.findMaxActivity(1, 1, new Date(), BigDecimal.valueOf(500));
        Assert.assertNotNull(maxActivity);
    }
}