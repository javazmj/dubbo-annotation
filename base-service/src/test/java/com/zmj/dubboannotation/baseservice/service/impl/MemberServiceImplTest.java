package com.zmj.dubboannotation.baseservice.service.impl;

import com.zmj.dubboannotation.serviceapi.api.MemberService;
import com.zmj.dubboannotation.serviceapi.model.MallMember;
import com.zmj.dubboannotation.serviceapi.vo.MemberVO;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/10/29 20:42
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class MemberServiceImplTest {

    @Autowired
    private MemberService memberService;


    @Test
    public void updateByPrimaryKey() {
        MemberVO memberVO = new MemberVO();
        memberVO.setId("1055276244765159425");
        memberVO.setEmail("2131231");
        int i = memberService.updateByParam(memberVO);

        Assert.assertEquals(1,i);
    }
}