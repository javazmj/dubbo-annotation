package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MemberImage;
import org.assertj.core.util.DateUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by: meijun
 * Date: 2019/01/15 13:45
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberImageMapperTest {

    @Autowired
    private MemberImageMapper mapper;

    @Test
    public void insert () {
        MemberImage memberImage = new MemberImage();
        memberImage.setMemberId("1056431008299274241");
        memberImage.setUrl("group1/M00/00/00/rBFNjlw9UyCAQxYXAAE39QwiZWI436.jpg");
        memberImage.setCreateTime(DateUtil.now());

        int insert = mapper.insert(memberImage);
        Assert.assertEquals(insert,1);
    }

}