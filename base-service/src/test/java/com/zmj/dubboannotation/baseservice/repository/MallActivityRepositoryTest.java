package com.zmj.dubboannotation.baseservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MallActivity;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/11/08 21:11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MallActivityRepositoryTest {


    @Autowired
    private MallActivityRepository repository;

    @Test
    public void save() {
        MallActivity mallActivity = new MallActivity();
        mallActivity.setName("服装类满");
        mallActivity.setOperationPrice(BigDecimal.valueOf(299));
        mallActivity.setDeratePrice(BigDecimal.valueOf(50));
        mallActivity.setIsSuperpose(Integer.valueOf(2).byteValue());
        mallActivity.setStartTime(DateUtils.addDays(new Date(),0));
        mallActivity.setEndTime(DateUtils.addDays(new Date(),3));
        MallActivity save = repository.save(mallActivity);

        Assert.assertNotNull(save.getId());
    }

}