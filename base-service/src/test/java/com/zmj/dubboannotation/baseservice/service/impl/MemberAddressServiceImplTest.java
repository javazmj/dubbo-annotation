package com.zmj.dubboannotation.baseservice.service.impl;

import com.zmj.dubboannotation.baseservice.mapper.MemberAddressMapper;
import com.zmj.dubboannotation.serviceapi.model.MemberAddress;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by: meijun
 * Date: 2018/11/17 20:04
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MemberAddressServiceImplTest {

    @Autowired
    private MemberAddressMapper addressMapper;

    @Test
    public void findById() {

        MemberAddress address = addressMapper.findByIdAndMemberId(1,"1055276244765159425");
        Assert.assertNotNull(address);
    }
}