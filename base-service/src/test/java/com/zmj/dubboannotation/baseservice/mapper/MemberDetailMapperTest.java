package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MemberDetail;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/10/25 9:54
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberDetailMapperTest {

    @Autowired
    private MemberDetailMapper memberDetailMapper;

    @Test
    public void insert() {
        MemberDetail memberDetail = new MemberDetail();
        memberDetail.setAge(20);
        memberDetail.setBirthday(new Date());
        memberDetail.setName("周美军");
        memberDetail.setMemberId("1055276244765159425");

        int insert = memberDetailMapper.insert(memberDetail);

        Assert.assertEquals(1,insert);
    }
}