package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MallActivity;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/11/13 21:30
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MallActivityMapperTest {


    @Autowired
    private  MallActivityMapper mallActivityMapper;

    @Test
    public void findValidAllActity() throws ParseException {
        int type = 1;
        Date date = DateUtils.parseDate("2018-11-07","yyyy-MM-dd");
        List<MallActivity> validAllActity = mallActivityMapper.findValidAllActity(type, date);

        Assert.assertNotNull(validAllActity);
    }
}