package com.zmj.dubboannotation.baseservice.mapper;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.zmj.commoncore.utils.MD5Util;
import com.zmj.dubboannotation.serviceapi.model.MallMember;
import com.zmj.dubboannotation.serviceapi.vo.MemberVO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/10/25 9:48
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MallMemberMapperTest {

    @Autowired
    private MallMemberMapper mallMemberMapper;

    @Test
    public void insert() {
        MallMember member = new MallMember();
        member.setId(IdWorker.getIdStr());
        member.setMobile("13803490306");
        member.setEmail("536304123@qq.com");
        member.setSalt(Integer.toString(987898));
        member.setPassword(MD5Util.getMD5Str("admin",member.getSalt()));
        int insert = mallMemberMapper.insert(member);

        Assert.assertEquals(1,insert);
    }


    @Test
    public void selectByParam() {
        MemberVO member = new MemberVO();
        member.setId("1055276244765159425");
        member.setEmail("meijun_over@163.com");
        List<MemberVO> list = mallMemberMapper.selectByParam(member);

        Assert.assertEquals(1,list.size());
    }
}