package com.zmj.dubboannotation.baseservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MemberShop;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/10/25 16:59
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberShopMapperTest {

    @Autowired
    private MemberShopMapper memberShopMapper;

    @Test
    public void insert() {

        MemberShop memberShop = new MemberShop();
        memberShop.setMemberId("1055276244765159425");
        memberShop.setShopName("测试店铺");
        memberShop.setShopPhone("010-9999999");
        memberShop.setShopAddress("北京市通州区");

        int insert = memberShopMapper.insert(memberShop);
        Assert.assertEquals(1,insert);
    }
}