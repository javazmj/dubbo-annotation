package com.zmj.dubboannotation.orderservice.sercive.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.orderservice.mapper.OrderItemMapper;
import com.zmj.dubboannotation.serviceapi.api.OrderItemService;
import com.zmj.dubboannotation.serviceapi.model.OrderItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by: meijun
 * Date: 2018/10/22 23:19
 */
@Service(interfaceClass = OrderItemService.class)
@Component
@Slf4j
public class OrderItemServiceImpl implements OrderItemService {

    @Autowired
    private OrderItemMapper orderItemMapper;

    //    @Compensable(confirmMethod = "confirmOrderItem",cancelMethod = "cancelOrderItem")
    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
    @Override
    public int insert(OrderItem OrderItem) {
        return orderItemMapper.insert(OrderItem);
    }


   /* @Override
    public void insertMore(ArrayList<OrderItem> OrderItems) {
        orderItemMapper.insertMore(OrderItems);
    }

    @Override
    public void insertCollectList(ArrayList<OrderItem> OrderItems) {
        orderItemMapper.insertCollectList(OrderItems);
    }*/
}
