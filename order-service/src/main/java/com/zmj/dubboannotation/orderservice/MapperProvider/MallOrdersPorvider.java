package com.zmj.dubboannotation.orderservice.MapperProvider;

import com.zmj.dubboannotation.serviceapi.model.OrderItem;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by: meijun
 * Date: 2018/09/16 23:35
 */
public class MallOrdersPorvider {

    public String insertMore(Map map) {
        ArrayList<OrderItem> ordersItem = (ArrayList<OrderItem>) map.get("list");

        StringBuilder sb = new StringBuilder();
        sb.append("insert into order_item ( id, member_id, order_child_id, sku_product_id , product_name, current_unit_price,quantity, total_price) values ");
        MessageFormat mf = new MessageFormat("( #'{'list[{0}].id,jdbcType=BIGINT}," +
                "#'{'list[{0}].memberId,jdbcType=BIGINT}, #'{'list[{0}].orderChildId,jdbcType=BIGINT},#'{'list[{0}].skuProductId,jdbcType=BIGINT}, " +
                "#'{'list[{0}].productName,jdbcType=VARCHAR}, #'{'list[{0}].currentUnitPrice,jdbcType=DECIMAL}, #'{'list[{0}].quantity,jdbcType=INTEGER}, " +
                "#'{'list[{0}].totalPrice,jdbcType=DECIMAL} )" );
        for (int i = 0; i < ordersItem.size(); i++) {
            sb.append(mf.format(new Object[]{i}));
            if (i < ordersItem.size() - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }
}
