package com.zmj.dubboannotation.orderservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MallOrder;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by: meijun
 * Date: 2018/09/15 11:21
 */
public interface MallOrdersRepository extends JpaRepository<MallOrder,String> {

}
