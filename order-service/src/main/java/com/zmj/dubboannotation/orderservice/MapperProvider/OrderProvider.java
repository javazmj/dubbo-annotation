package com.zmj.dubboannotation.orderservice.MapperProvider;

import com.zmj.dubboannotation.serviceapi.model.MallOrder;
import org.apache.ibatis.jdbc.SQL;

/**
 * Created by: meijun
 * Date: 2018/10/28 20:30
 */
public class OrderProvider {

    private final String TABLE = "mall_order";

    public String findByQuery(MallOrder mallOrder) {
        SQL sql = new SQL().SELECT("id, order_sn, pay_sn, member_id, receiver_name, receiver_mobile," +
                        " receiver_address, receiver_address_info, product_price,total_price, pay_price, postage_price, mall_reduce_price, " +
                "shop_reduce_price, order_integral, integral_price, payment_type, pay_status, " +
                "order_status, is_invoice, invoice_id, shop_activity_id, mall_activity_id, payment_time, " +
                "create_time, update_time, del_flag").FROM(TABLE);

        String memberId = mallOrder.getMemberId();
        if(null != memberId) {
            sql.WHERE("member_id = #{memberId}");
        }

        String id = mallOrder.getId();
        if(null != id) {
            sql.WHERE("id = #{id}");
        }

        return sql.toString();

    }
}
