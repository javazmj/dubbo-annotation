package com.zmj.dubboannotation.orderservice;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.CrossOrigin;


@CrossOrigin
@EnableDubboConfiguration
@MapperScan(basePackages = "com.zmj.dubboannotation.orderservice.mapper")
@EntityScan(basePackages = "com.zmj.dubboannotation.serviceapi.model")
@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.zmj.dubboannotation.orderservice.repository")
@EnableJpaAuditing
@EnableAspectJAutoProxy
@EnableTransactionManagement
//@ImportResource(locations = {"classpath:tcc-transaction.xml","classpath:tcc-repository.xml"})
public class OrderServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderServiceApplication.class, args);
    }
}
