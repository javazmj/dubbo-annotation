package com.zmj.dubboannotation.orderservice.mapper;

import com.zmj.dubboannotation.orderservice.MapperProvider.OrderProvider;
import com.zmj.dubboannotation.serviceapi.model.MallOrder;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

public interface MallOrderMapper {
    @Delete({
        "delete from mall_order",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into mall_order (id, order_sn, ",
        "pay_sn, member_id, ",
        "receiver_name, receiver_mobile, ",
        "receiver_address, receiver_address_info, ",
        "product_price, total_price, ",
        "pay_price, postage_price, ",
        "mall_reduce_price, shop_reduce_price, ",
        "order_integral, integral_price, ",
        "payment_type, pay_status, ",
        "order_status, is_invoice, ",
        "invoice_id, shop_activity_id, ",
        "mall_activity_id, payment_time, ",
        "create_time, update_time) ",
        "values (#{id,jdbcType=VARCHAR}, #{orderSn,jdbcType=VARCHAR}, ",
        "#{paySn,jdbcType=VARCHAR}, #{memberId,jdbcType=VARCHAR}, ",
        "#{receiverName,jdbcType=VARCHAR}, #{receiverMobile,jdbcType=VARCHAR}, ",
        "#{receiverAddress,jdbcType=VARCHAR}, #{receiverAddressInfo,jdbcType=VARCHAR}, ",
        "#{productPrice,jdbcType=DECIMAL}, #{totalPrice,jdbcType=DECIMAL}, ",
        "#{payPrice,jdbcType=DECIMAL}, #{postagePrice,jdbcType=DECIMAL}, ",
        "#{mallReducePrice,jdbcType=DECIMAL}, #{shopReducePrice,jdbcType=DECIMAL}, ",
        "#{orderIntegral,jdbcType=DECIMAL}, #{integralPrice,jdbcType=DECIMAL}, ",
        "#{paymentType,jdbcType=TINYINT}, #{payStatus,jdbcType=TINYINT}, ",
        "#{orderStatus,jdbcType=TINYINT}, #{isInvoice,jdbcType=TINYINT}, ",
        "#{invoiceId,jdbcType=INTEGER}, #{shopActivityId,jdbcType=VARCHAR}, ",
        "#{mallActivityId,jdbcType=VARCHAR}, #{paymentTime,jdbcType=TIMESTAMP}, ",
        "NOW(), NOW())"
    })
    int insert(MallOrder record);

    @Select({
        "select",
        "id, order_sn, pay_sn, member_id, receiver_name, receiver_mobile, receiver_address, ",
        "receiver_address_info, product_price, total_price, pay_price, postage_price, ",
        "mall_reduce_price, shop_reduce_price, order_integral, integral_price, payment_type, ",
        "pay_status, order_status, is_invoice, invoice_id, shop_activity_id, mall_activity_id, ",
        "payment_time, create_time, update_time, del_flag",
        "from mall_order",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="order_sn", property="orderSn", jdbcType=JdbcType.VARCHAR),
        @Result(column="pay_sn", property="paySn", jdbcType=JdbcType.VARCHAR),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_name", property="receiverName", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_mobile", property="receiverMobile", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_address", property="receiverAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_address_info", property="receiverAddressInfo", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_price", property="productPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="total_price", property="totalPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="pay_price", property="payPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="postage_price", property="postagePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="mall_reduce_price", property="mallReducePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="shop_reduce_price", property="shopReducePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="order_integral", property="orderIntegral", jdbcType=JdbcType.DECIMAL),
        @Result(column="integral_price", property="integralPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="payment_type", property="paymentType", jdbcType=JdbcType.TINYINT),
        @Result(column="pay_status", property="payStatus", jdbcType=JdbcType.TINYINT),
        @Result(column="order_status", property="orderStatus", jdbcType=JdbcType.TINYINT),
        @Result(column="is_invoice", property="isInvoice", jdbcType=JdbcType.TINYINT),
        @Result(column="invoice_id", property="invoiceId", jdbcType=JdbcType.INTEGER),
        @Result(column="shop_activity_id", property="shopActivityId", jdbcType=JdbcType.VARCHAR),
        @Result(column="mall_activity_id", property="mallActivityId", jdbcType=JdbcType.VARCHAR),
        @Result(column="payment_time", property="paymentTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    MallOrder selectByPrimaryKey(Long id);

    @Select({
        "select",
        "id, order_sn, pay_sn, member_id, receiver_name, receiver_mobile, receiver_address, ",
        "receiver_address_info, product_price, total_price, pay_price, postage_price, ",
        "mall_reduce_price, shop_reduce_price, order_integral, integral_price, payment_type, ",
        "pay_status, order_status, is_invoice, invoice_id, shop_activity_id, mall_activity_id, ",
        "payment_time, create_time, update_time, del_flag",
        "from mall_order"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="order_sn", property="orderSn", jdbcType=JdbcType.VARCHAR),
        @Result(column="pay_sn", property="paySn", jdbcType=JdbcType.VARCHAR),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_name", property="receiverName", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_mobile", property="receiverMobile", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_address", property="receiverAddress", jdbcType=JdbcType.VARCHAR),
        @Result(column="receiver_address_info", property="receiverAddressInfo", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_price", property="productPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="total_price", property="totalPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="pay_price", property="payPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="postage_price", property="postagePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="mall_reduce_price", property="mallReducePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="shop_reduce_price", property="shopReducePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="order_integral", property="orderIntegral", jdbcType=JdbcType.DECIMAL),
        @Result(column="integral_price", property="integralPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="payment_type", property="paymentType", jdbcType=JdbcType.TINYINT),
        @Result(column="pay_status", property="payStatus", jdbcType=JdbcType.TINYINT),
        @Result(column="order_status", property="orderStatus", jdbcType=JdbcType.TINYINT),
        @Result(column="is_invoice", property="isInvoice", jdbcType=JdbcType.TINYINT),
        @Result(column="invoice_id", property="invoiceId", jdbcType=JdbcType.INTEGER),
        @Result(column="shop_activity_id", property="shopActivityId", jdbcType=JdbcType.VARCHAR),
        @Result(column="mall_activity_id", property="mallActivityId", jdbcType=JdbcType.VARCHAR),
        @Result(column="payment_time", property="paymentTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<MallOrder> selectAll();

    @Update({
        "update mall_order",
        "set order_sn = #{orderSn,jdbcType=VARCHAR},",
          "pay_sn = #{paySn,jdbcType=VARCHAR},",
          "member_id = #{memberId,jdbcType=VARCHAR},",
          "receiver_name = #{receiverName,jdbcType=VARCHAR},",
          "receiver_mobile = #{receiverMobile,jdbcType=VARCHAR},",
          "receiver_address = #{receiverAddress,jdbcType=VARCHAR},",
          "receiver_address_info = #{receiverAddressInfo,jdbcType=VARCHAR},",
          "product_price = #{productPrice,jdbcType=DECIMAL},",
          "total_price = #{totalPrice,jdbcType=DECIMAL},",
          "pay_price = #{payPrice,jdbcType=DECIMAL},",
          "postage_price = #{postagePrice,jdbcType=DECIMAL},",
          "mall_reduce_price = #{mallReducePrice,jdbcType=DECIMAL},",
          "shop_reduce_price = #{shopReducePrice,jdbcType=DECIMAL},",
          "order_integral = #{orderIntegral,jdbcType=DECIMAL},",
          "integral_price = #{integralPrice,jdbcType=DECIMAL},",
          "payment_type = #{paymentType,jdbcType=TINYINT},",
          "pay_status = #{payStatus,jdbcType=TINYINT},",
          "order_status = #{orderStatus,jdbcType=TINYINT},",
          "is_invoice = #{isInvoice,jdbcType=TINYINT},",
          "invoice_id = #{invoiceId,jdbcType=INTEGER},",
          "shop_activity_id = #{shopActivityId,jdbcType=VARCHAR},",
          "mall_activity_id = #{mallActivityId,jdbcType=VARCHAR},",
          "payment_time = #{paymentTime,jdbcType=TIMESTAMP},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(MallOrder record);

    @SelectProvider(type = OrderProvider.class, method = "findByQuery")
    List<MallOrder> findByQuery(MallOrder mallOrder);


}