package com.zmj.dubboannotation.orderservice.mapper;

import com.zmj.dubboannotation.orderservice.MapperProvider.MallOrdersPorvider;
import com.zmj.dubboannotation.serviceapi.model.OrderItem;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

public interface OrderItemMapper {
    @Delete({
        "delete from order_item",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
        "insert into order_item (id, member_id, ",
        "order_child_id, sku_product_id, ",
        "product_name, current_unit_price, ",
        "quantity, total_price)",
        "values (#{id,jdbcType=VARCHAR}, #{memberId,jdbcType=VARCHAR}, ",
        "#{orderChildId,jdbcType=VARCHAR}, #{skuProductId,jdbcType=VARCHAR}, ",
        "#{productName,jdbcType=VARCHAR}, #{currentUnitPrice,jdbcType=DECIMAL}, ",
        "#{quantity,jdbcType=INTEGER}, #{totalPrice,jdbcType=DECIMAL})"
    })
    int insert(OrderItem record);

    @Select({
        "select",
        "id, member_id, order_child_id, sku_product_id, product_name, current_unit_price, ",
        "quantity, total_price",
        "from order_item",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="order_child_id", property="orderChildId", jdbcType=JdbcType.VARCHAR),
        @Result(column="sku_product_id", property="skuProductId", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
        @Result(column="current_unit_price", property="currentUnitPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="quantity", property="quantity", jdbcType=JdbcType.INTEGER),
        @Result(column="total_price", property="totalPrice", jdbcType=JdbcType.DECIMAL)
    })
    OrderItem selectByPrimaryKey(Long id);

    @Select({
        "select",
        "id, member_id, order_child_id, sku_product_id, product_name, current_unit_price, ",
        "quantity, total_price",
        "from order_item"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="order_child_id", property="orderChildId", jdbcType=JdbcType.VARCHAR),
        @Result(column="sku_product_id", property="skuProductId", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
        @Result(column="current_unit_price", property="currentUnitPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="quantity", property="quantity", jdbcType=JdbcType.INTEGER),
        @Result(column="total_price", property="totalPrice", jdbcType=JdbcType.DECIMAL)
    })
    List<OrderItem> selectAll();

    @Update({
        "update order_item",
        "set member_id = #{memberId,jdbcType=VARCHAR},",
          "order_child_id = #{orderChildId,jdbcType=VARCHAR},",
          "sku_product_id = #{skuProductId,jdbcType=VARCHAR},",
          "product_name = #{productName,jdbcType=VARCHAR},",
          "current_unit_price = #{currentUnitPrice,jdbcType=DECIMAL},",
          "quantity = #{quantity,jdbcType=INTEGER},",
          "total_price = #{totalPrice,jdbcType=DECIMAL}",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(OrderItem record);

    /**
     * 批量插入
     * @param orderItem
     */
    @InsertProvider(type = MallOrdersPorvider.class,method = "insertMore")
    int insertMore(@Param("list") ArrayList<OrderItem> orderItem);

    @Insert({
            "<script>",
            "insert into order_item (id, member_id, order_child_id, ",
            "sku_product_id , product_name, current_unit_price, ",
            "quantity, total_price)",
            "values ",
            "<foreach collection='orderItem' item='item' index='index' separator=','>",
            "(#{item.orderChildId,jdbcType=VARCHAR}, #{item.skuProductId,jdbcType=VARCHAR}, ",
            " #{item.productName,jdbcType=VARCHAR}, #{item.currentUnitPrice,jdbcType=DECIMAL}, ",
            "#{item.quantity,jdbcType=INTEGER}, #{item.totalPrice,jdbcType=DECIMAL})",
            "</foreach>",
            "</script>"
    })
    int insertCollectList(@Param(value="orderItem") ArrayList<OrderItem> orderItem);

    @Select("SELECT *  FROM " +
            "order_item " +
            "WHERE" +
            " order_child_id IN (" +
            "SELECT " +
            " o.id " +
            "FROM " +
            "order_child o " +
            "LEFT JOIN mall_order o1 ON o.mall_order_sn = o1.order_sn " +
            " WHERE " +
            "o1.order_sn = #{orderSn} " +
            "AND o1.member_id = #{memberId} " +
            "AND o1.pay_status = 1 " +
            "AND o1.order_status = 10 " +
            ")")
    List<OrderItem> findByOrderChildId(@Param("orderSn")String orderSn,@Param("memberId")String memberId);
}