package com.zmj.dubboannotation.orderservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.OrderChild;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface OrderChildMapper {
    @Delete({
            "delete from order_child",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(Long id);

    @Insert({
            "insert into order_child (id, mall_order_sn, ",
            "pay_sn, member_id, ",
            "product_price, total_price, ",
            "pay_price, postage_price, ",
            "mall_reduce_price, shop_reduce_price, ",
            "order_integral, integral_price, ",
            "order_status, shop_activity_id, ",
            "express_id, express_name, ",
            "send_time, end_time, ",
            "close_time, create_time, ",
            "update_time)",
            "values (#{id,jdbcType=VARCHAR}, #{mallOrderSn,jdbcType=VARCHAR}, ",
            "#{paySn,jdbcType=VARCHAR}, #{memberId,jdbcType=VARCHAR}, ",
            "#{productPrice,jdbcType=DECIMAL}, #{totalPrice,jdbcType=DECIMAL}, ",
            "#{payPrice,jdbcType=DECIMAL}, #{postagePrice,jdbcType=DECIMAL}, ",
            "#{mallReducePrice,jdbcType=DECIMAL}, #{shopReducePrice,jdbcType=DECIMAL}, ",
            "#{orderIntegral,jdbcType=DECIMAL}, #{integralPrice,jdbcType=DECIMAL}, ",
            "#{orderStatus,jdbcType=TINYINT}, #{shopActivityId,jdbcType=VARCHAR}, ",
            "#{expressId,jdbcType=VARCHAR}, #{expressName,jdbcType=VARCHAR}, ",
            "#{sendTime,jdbcType=TIMESTAMP}, #{endTime,jdbcType=TIMESTAMP}, ",
            "#{closeTime,jdbcType=TIMESTAMP},now(), ",
            "now())"
    })
    int insert(OrderChild record);

    @Select({
            "select",
            "id, mall_order_sn, pay_sn, member_id, product_price, total_price, pay_price, ",
            "postage_price, mall_reduce_price, shop_reduce_price, order_integral, integral_price, ",
            "order_status, shop_activity_id, express_id, express_name, send_time, end_time, ",
            "close_time, create_time, update_time, del_flag",
            "from order_child",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="mall_order_sn", property="mallOrderSn", jdbcType=JdbcType.VARCHAR),
            @Result(column="pay_sn", property="paySn", jdbcType=JdbcType.VARCHAR),
            @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_price", property="productPrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="total_price", property="totalPrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="pay_price", property="payPrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="postage_price", property="postagePrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="mall_reduce_price", property="mallReducePrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="shop_reduce_price", property="shopReducePrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="order_integral", property="orderIntegral", jdbcType=JdbcType.DECIMAL),
            @Result(column="integral_price", property="integralPrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="order_status", property="orderStatus", jdbcType=JdbcType.TINYINT),
            @Result(column="shop_activity_id", property="shopActivityId", jdbcType=JdbcType.VARCHAR),
            @Result(column="express_id", property="expressId", jdbcType=JdbcType.VARCHAR),
            @Result(column="express_name", property="expressName", jdbcType=JdbcType.VARCHAR),
            @Result(column="send_time", property="sendTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="close_time", property="closeTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    OrderChild selectByPrimaryKey(Long id);

    @Select({
            "select",
            "id, mall_order_sn, pay_sn, member_id, product_price, total_price, pay_price, ",
            "postage_price, mall_reduce_price, shop_reduce_price, order_integral, integral_price, ",
            "order_status, shop_activity_id, express_id, express_name, send_time, end_time, ",
            "close_time, create_time, update_time, del_flag",
            "from order_child"
    })
    @Results({
            @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="mall_order_sn", property="mallOrderSn", jdbcType=JdbcType.VARCHAR),
            @Result(column="pay_sn", property="paySn", jdbcType=JdbcType.VARCHAR),
            @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_price", property="productPrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="total_price", property="totalPrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="pay_price", property="payPrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="postage_price", property="postagePrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="mall_reduce_price", property="mallReducePrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="shop_reduce_price", property="shopReducePrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="order_integral", property="orderIntegral", jdbcType=JdbcType.DECIMAL),
            @Result(column="integral_price", property="integralPrice", jdbcType=JdbcType.DECIMAL),
            @Result(column="order_status", property="orderStatus", jdbcType=JdbcType.TINYINT),
            @Result(column="shop_activity_id", property="shopActivityId", jdbcType=JdbcType.VARCHAR),
            @Result(column="express_id", property="expressId", jdbcType=JdbcType.VARCHAR),
            @Result(column="express_name", property="expressName", jdbcType=JdbcType.VARCHAR),
            @Result(column="send_time", property="sendTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="end_time", property="endTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="close_time", property="closeTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<OrderChild> selectAll();

    @Update({
            "update order_child",
            "set mall_order_sn = #{mallOrderSn,jdbcType=VARCHAR},",
            "pay_sn = #{paySn,jdbcType=VARCHAR},",
            "member_id = #{memberId,jdbcType=VARCHAR},",
            "product_price = #{productPrice,jdbcType=DECIMAL},",
            "total_price = #{totalPrice,jdbcType=DECIMAL},",
            "pay_price = #{payPrice,jdbcType=DECIMAL},",
            "postage_price = #{postagePrice,jdbcType=DECIMAL},",
            "mall_reduce_price = #{mallReducePrice,jdbcType=DECIMAL},",
            "shop_reduce_price = #{shopReducePrice,jdbcType=DECIMAL},",
            "order_integral = #{orderIntegral,jdbcType=DECIMAL},",
            "integral_price = #{integralPrice,jdbcType=DECIMAL},",
            "order_status = #{orderStatus,jdbcType=TINYINT},",
            "shop_activity_id = #{shopActivityId,jdbcType=VARCHAR},",
            "express_id = #{expressId,jdbcType=VARCHAR},",
            "express_name = #{expressName,jdbcType=VARCHAR},",
            "send_time = #{sendTime,jdbcType=TIMESTAMP},",
            "end_time = #{endTime,jdbcType=TIMESTAMP},",
            "close_time = #{closeTime,jdbcType=TIMESTAMP},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP},",
            "del_flag = #{delFlag,jdbcType=TINYINT}",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(OrderChild record);
}