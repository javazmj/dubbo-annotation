package com.zmj.commoncore.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class ProductCommentDTO implements Serializable {

    private static final long serialVersionUID = -4578297990233921427L;

    private String productId;

    private String memberId;

    @NotBlank
    private String comment;

    private Byte commentGrade;

    private String skuProductId;

    private String orderId;

}