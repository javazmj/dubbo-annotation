package com.zmj.commoncore.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by: meijun
 * Date: 2018/11/10 17:29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel
public class OrderDTO implements Serializable {

    private static final long serialVersionUID = -4893860288485701614L;

    //收货地址ID
    @ApiModelProperty(value = "收货地址ID", required = true)
    private Integer addressId;

    //发票ID
    @ApiModelProperty(value = "发票ID", required = false)
    private Integer invoiceId;

    //使用的商城优惠券ID
    @ApiModelProperty(value = "商城优惠券ID", required = false)
    private Integer mallActivityId;

    //使用的店铺优惠券ID
    @ApiModelProperty(value = "店铺优惠券ID", required = false)
    private Integer shopActivityId;

    //选择购买的商品skuId
    @ApiModelProperty(value = "商品skuId", required = false)
    private String skuId;

    //选择购买的商品数量
    @ApiModelProperty(value = "商品数量", required = false)
    private Integer nums;

    @ApiModelProperty(value = "用户ID", required = false)
    private String memberId;
}



