package com.zmj.commoncore.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *  商品信息传输实体类
 * @author zmj
 * @version 2018/7/3
 */
@Data
@NoArgsConstructor
@ToString
public class ProductDTO implements Serializable {

    private static final long serialVersionUID = -4856177066037025950L;

    private String productName;

    private Integer code;

    private Integer shopId;

    private Integer categoryId;

    private Integer brandId;

    /**
     * 是否参与店铺活动0不参与 1参与
     */
    private Byte isSetMeal;
    /**
     * 赠品id(不是必须,有可能该赠品未加入商品库)
     */
    private Integer productGiveawayId;

    private String detail;

    /**
     * 商品统一库存
     */
    private Integer productStock;

    /**
     * 包装售后描述
     */
    private String guarantee;

    /**
     * 商品类型 1单品 2赠品
     */
    private Byte produteType;

    private Byte isShow;

    /**
     * 是否上架
     */
    private Byte isGrund;

    /**
     * 是否通过审核(针对所有这个商品下的sku)
     */
    private Byte isAudit;

    /**
     * 所选属性和属性值
     *  attributeValues : “属性id1:属性值id11/属性值id12,属性id2:属性值id21/属性值id22,”
     *  例  attributeValues = "11(尺码id):3/4,12(颜色ID):1/2"
     */
    private String attributes;
}
