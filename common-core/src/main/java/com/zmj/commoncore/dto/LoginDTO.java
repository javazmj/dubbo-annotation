package com.zmj.commoncore.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Created by: meijun
 * Date: 2018/10/16 21:17
 */
@Data
public class LoginDTO implements Serializable {

    private static final long serialVersionUID = -265089534959873333L;

    private String mobile;

    private String email;

    @NotBlank
    private String password;

    private String verifyCode;
}
