package com.zmj.commoncore.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 购物车传输类
 * Created by: meijun
 * Date: 2018/10/30 14:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel
public class CartDTO implements Serializable {

    private static final long serialVersionUID = 8012062256442918363L;
    /**
     * 店铺id
     */
    @NotNull(message = "店铺ID不能为空")
    @ApiModelProperty(value = "店铺ID", required = true)
    private Integer shopId;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "店铺ID", required = false)
    private String memberId;
    /**
     * 商品id 这里是skuid
     */
    @NotNull(message = "商品ID不能为空")
    @ApiModelProperty(value = "商品ID", required = true)
    private String productId;

    /**
     * 选择规格值的ID  :  5-7
     */
    @NotEmpty(message = "规格值不能为空")
    @ApiModelProperty(value = "规格值", required = true)
    private String attrValueId;
    /**
     * 数量
     */
    @Min(value = 1)
    @ApiModelProperty(value = "数量", required = false)
    private Integer quantity;

    /**
     * 是否选中 1未选中 2选中
     */
    @ApiModelProperty(value = "是否选中,1未选中 2选中", required = false)
    private Byte checked = 1;

    /**
     * 加入购物车时间
     */
    @ApiModelProperty(value = "加入购物车时间", required = false)
    private Date date = new Date();

    /**
     * 商品skuid
     */
    @ApiModelProperty(value = "商品skuid", required = false)
    private String skuProductId;


}
