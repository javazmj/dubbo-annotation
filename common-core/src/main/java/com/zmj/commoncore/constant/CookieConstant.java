package com.zmj.commoncore.constant;

/**
 * Created by: meijun
 * Date: 2018/10/22 21:39
 */
public interface CookieConstant {

    String COOKIE_TOKEN = "token";

    Integer COOKIE_EXPIRE = 60 * 60 * 24 * 7;
}
