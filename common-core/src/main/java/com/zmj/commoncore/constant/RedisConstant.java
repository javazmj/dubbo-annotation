package com.zmj.commoncore.constant;

/**
 * Created by: meijun
 * Date: 2018/10/22 21:37
 */
public interface RedisConstant {

    String TOKEN_PREFIX = "token_%s";

    Integer TOKEN_EXPIRE = 60 * 60 * 5;


    String LOGIN_CODE_PREFIX = "loginVerifyCode"; //登录验证码

    String REGISTER_CODE_PREFIX = "registerVerifyCode"; //注册验证码

    String MEMBER_ACTION_PREFIX = "memberActionLog"; //拦截用户操作记录

    String MEMBER_CART = "cart"; //用户购物车

    Integer MEMBER_CART_EXPIRE = 60 * 60 * 24 * 30;//购物车失效时间一个月

    Integer CODE_EXPIRE = 60 * 2; //失效时间120秒
}
