package com.zmj.commoncore.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *  商品SKU生成策略
 * @author zmj
 * @version 2018/7/10
 */
public class SKUGenerator {

    private static final String SPLIT = "-";


    /**
     * sku生成
     * @param skuId
     * @param attributes
     * @return String
     */
    public static String createSKU(String skuId,String attributes){
        String attributesASC = attributesASC(attributes);
        System.out.println(attributesASC);
        StringBuffer sb = new StringBuffer();
        sb.append(skuId);
        sb.append(SPLIT);
        sb.append(attributesASC);
        return sb.toString();
    }

    /**
     * 产品标题截取 -
     * @param name
     * @return
     */
    public static String nameSpilt(String name) {
        return name.substring(0,name.lastIndexOf("-"));
    }

    /**
     * 将属性asc排序
     * @param attributes
     * @return
     */
    private static String attributesASC (String attributes) {
        String[] split = attributes.split("/");
        List<String> stringList = Arrays.asList(split);
        Collections.sort(stringList);
        StringBuilder sb = new StringBuilder();
        for (String s:stringList) {
            sb.append(s + "-") ;
        }
        return sb.toString().substring(0,sb.toString().length() -1 );
    }

}
