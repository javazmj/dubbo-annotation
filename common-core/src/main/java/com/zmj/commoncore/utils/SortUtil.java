package com.zmj.commoncore.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @author zmj
 * @version 2018/7/12
 */
public class SortUtil {

    public static String sort(String sort,String sortName) {
        return  sortName + "  " + sort;
    }

    /**
     * 用来追加商城/店铺活动id 以逗号分隔
     * @param str
     * @param addStr
     * @return
     */
    public static String strAppend(String str,String addStr) {
        StringBuffer sb = new StringBuffer(str);
        if(StringUtils.isNotBlank(str)){
            sb.append(",");
        }
        sb.append(addStr);
        return  sb.toString();
    }
}
