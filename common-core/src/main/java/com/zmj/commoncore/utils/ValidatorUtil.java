package com.zmj.commoncore.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author zmj
 * @version 2018/6/20
 */
public class ValidatorUtil {

//    private static final Pattern mobile_pattern = Pattern.compile("1\\d{10}");
    /**
     * 正则表达式：验证手机号
     */
    private static final Pattern MOBILE_PATTERN = Pattern.compile("/^1(3|4|5|7|8)\\d{9}$/");

    /**
     * 正则表达式：验证邮箱
     */
    public static final Pattern  EMAIL_PATTERN = Pattern.compile("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$");

    public static boolean isMobile(String src) {
        if(StringUtils.isEmpty(src)) {
            return false;
        }
        Matcher m = MOBILE_PATTERN.matcher(src);
        return m.matches();
    }

    public static boolean isEmail(String src) {
        if(StringUtils.isEmpty(src)) {
            return false;
        }
        Matcher m = EMAIL_PATTERN.matcher(src);
        return m.matches();
    }
}
