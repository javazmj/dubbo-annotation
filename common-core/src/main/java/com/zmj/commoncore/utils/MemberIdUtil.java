package com.zmj.commoncore.utils;

import com.zmj.commoncore.constant.CookieConstant;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *   根据cookie中的值取用户的id
 *   防止横向越权
 * </p>
 *
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/11/1 13:57
 * @Email: 536304123@QQ.COM
 */
public class MemberIdUtil {

    public static String getCookieMemberId(HttpServletRequest request){
        Cookie cookie = CookieUtil.get(request, CookieConstant.COOKIE_TOKEN);
        if(null != cookie) {
            String cookieValue = cookie.getValue();
            return JWTUtil.getAppUID(cookieValue);
        }
        return null;

    }
}
