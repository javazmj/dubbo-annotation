package com.zmj.commoncore.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * <p>
 *
 * </p>
 *
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/10/29 10:05
 * @Email: 536304123@QQ.COM
 */
public class AttrValueUtil {

    private static final String ATTRNAME = "attributeValueName";

    private static final String SPILT = "-";

    public static String getAttr(String attrValue) {
        JSONArray array = JSONArray.parseArray(attrValue);
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < array.size(); i++) {
            JSONObject json = array.getJSONObject(i);
            String value = json.getString(ATTRNAME);
            buffer.append(value);
            buffer.append(SPILT);
        }
        String s = buffer.substring(0, buffer.length() - 1);
        return s;
    }
}
