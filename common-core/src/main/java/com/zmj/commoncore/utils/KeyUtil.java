package com.zmj.commoncore.utils;

import java.util.Random;

/**
 * Created by: meijun
 * Date: 2017/12/20 16:48
 */
public class KeyUtil {

    /**
     * 生成唯一的主键
     * 格式 : 时间 + 随机数
     * @return
     */
    public static synchronized  String genUniqueKey () {
        Random random  = new Random();
        Integer number = random.nextInt(900000) + 100000;
        return  System.currentTimeMillis() + String.valueOf(number) ;

    }
}
