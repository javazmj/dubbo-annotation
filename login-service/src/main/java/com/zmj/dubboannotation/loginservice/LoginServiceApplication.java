package com.zmj.dubboannotation.loginservice;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@EnableDubboConfiguration
@MapperScan(basePackages = "com.zmj.dubboannotation.loginservice.mapper")
@EntityScan(basePackages = "com.zmj.dubboannotation.serviceapi.model")
@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.zmj.dubboannotation.loginservice.repository")
@EnableAspectJAutoProxy
@EnableTransactionManagement
public class LoginServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoginServiceApplication.class, args);
    }
}
