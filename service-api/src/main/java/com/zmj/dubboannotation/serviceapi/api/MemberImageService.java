package com.zmj.dubboannotation.serviceapi.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmj.dubboannotation.serviceapi.model.MemberImage;

/**
 * Created by: meijun
 * Date: 2018/11/17 9:53
 */
public interface MemberImageService extends IService<MemberImage> {

}
