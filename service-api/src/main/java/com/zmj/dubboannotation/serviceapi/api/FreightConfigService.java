package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.ShopProductFreightConfig;

/**
 * Created by: meijun
 * Date: 2018/11/17 10:22
 */
public interface FreightConfigService {

    ShopProductFreightConfig findByShopId(Integer shopId);
}
