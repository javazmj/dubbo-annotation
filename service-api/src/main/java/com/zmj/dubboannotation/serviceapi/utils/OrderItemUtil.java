package com.zmj.dubboannotation.serviceapi.utils;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.zmj.commoncore.dto.CartDTO;
import com.zmj.commoncore.utils.DecimalUtil;
import com.zmj.dubboannotation.serviceapi.model.OrderChild;
import com.zmj.dubboannotation.serviceapi.model.OrderItem;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;

/**
 * <p>
 *      创建订单详情工具类
 * </p>
 *
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/11/16 14:48
 * @Email: 536304123@QQ.COM
 */
public class OrderItemUtil {

    public static void create(OrderItem orderItem, CartDTO cartDTO, OrderChild orderChild, ProductVO productVO) {

        orderItem.setId(IdWorker.getIdStr());
        orderItem.setMemberId(cartDTO.getMemberId());
        orderItem.setOrderChildId(orderChild.getId());
        orderItem.setSkuProductId(productVO.getSkuId());
        orderItem.setProductName(productVO.getSkuName());
        orderItem.setCurrentUnitPrice(productVO.getSellMobilePrice());
        orderItem.setQuantity(cartDTO.getQuantity());
        orderItem.setTotalPrice(DecimalUtil.multiply(orderItem.getCurrentUnitPrice(),orderItem.getQuantity()));

    }
}
