package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.enums.ResultEnum;

/**
 * Created by: meijun
 * Date: 2018/11/19 22:08
 */
public class BaseException extends RuntimeException{

    private Integer code;

    public BaseException() {
    }

    public BaseException(ResultEnum resultEnum) {
        super(resultEnum.getMsg() );
        this.code = resultEnum.getCode();
    }

    public BaseException(Integer code, String message) {
        super(message);
        this.code = code;
    }


}
