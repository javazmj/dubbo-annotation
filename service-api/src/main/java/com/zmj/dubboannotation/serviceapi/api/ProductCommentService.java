package com.zmj.dubboannotation.serviceapi.api;

import com.github.pagehelper.PageInfo;
import com.zmj.commoncore.dto.ProductCommentDTO;
import com.zmj.dubboannotation.serviceapi.model.ProductComment;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/10/28 13:38
 */
public interface ProductCommentService {

    PageInfo<ProductComment> findAllByProductId(String productId,Integer pageNum,Integer pageSize);

    List<ProductComment> findByQuery(ProductCommentDTO productCommentDTO);

    int save(ProductCommentDTO productCommentDTO);
}
