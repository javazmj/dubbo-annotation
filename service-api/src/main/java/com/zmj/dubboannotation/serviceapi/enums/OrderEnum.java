package com.zmj.dubboannotation.serviceapi.enums;

import lombok.Getter;

/**
 * Created by: meijun
 * Date: 2017/12/20 15:37
 */
@Getter
public enum OrderEnum implements CodeEnum {


    //支付方式
    PAYMENT_TYPE_WECHAT(1,"微信"),
    PAYMENT_TYPE_ALIPAY(2,"支付宝"),
    PAYMENT_TYPE_UNION(3,"银联在线"),


    //支付状态
    PAY_STATUS_WAIT(1,"待支付"),
    PAY_STATUS_SUCCESS(2,"已支付"),

    //订单状态
    ORDER_STATUS_CANCEL(0,"已取消"),
    ORDER_STATUS_NOT_PAY(10,"未付款"),
    ORDER_STATUS_PAY(20,"已付款"),
    ORDER_STATUS_WAIT_SEND(30,"待发货"),
    ORDER_STATUS_SEND(40,"已发货"),
    ORDER_STATUS_FINISHED(50,"交易成功"),
    ORDER_STATUS_CLOSE(60,"交易关闭"),

    IS_INVOICE_NO(0,"不开发票"),
    IS_INVOICE_DEFAULT(1,"普通发票"),
    IS_INVOICE_PLUS(2,"增值税发票"),
    ;

    private Integer code;

    private String  msg;

    OrderEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
