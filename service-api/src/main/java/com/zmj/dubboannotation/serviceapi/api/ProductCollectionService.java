package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.MemberProductCollection;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/10/28 13:38
 */
public interface ProductCollectionService {

  List<MemberProductCollection> findAllById(String memberId);

  int save(MemberProductCollection memberProductCollection);

  int delete(int id);

  MemberProductCollection selectByProductId(String memberId,String productId);

  int deleteByProductId(String valueOf, String skuId);
}
