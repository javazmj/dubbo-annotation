package com.zmj.dubboannotation.serviceapi.api;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * Created by: meijun
 * Date: 2018/10/23 21:06
 */
public interface CartService {

    JSONObject cartList(Map<Object, Object> cart, boolean checked);

    /**
     * 删除购物车选中商品
     * @param cart
     * @return
     */
    int deleteChecked(Map<Object, Object> cart,String memberId);

}
