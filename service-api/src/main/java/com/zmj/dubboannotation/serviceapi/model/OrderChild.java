package com.zmj.dubboannotation.serviceapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderChild extends BaseModel implements Serializable {

    private static final long serialVersionUID = 7166073882306897588L;

    @Id
    @GeneratedValue
    private String id;

    private String mallOrderSn;

    private String paySn;

    private String memberId;

    private BigDecimal productPrice;

    private BigDecimal totalPrice;

    private BigDecimal payPrice;

    private BigDecimal postagePrice;

    private BigDecimal mallReducePrice;

    private BigDecimal shopReducePrice;

    private BigDecimal orderIntegral;

    private BigDecimal integralPrice;

    private Byte orderStatus;

    private String shopActivityId;

    private String expressId;

    private String expressName;

    private Date sendTime;

    private Date endTime;

    private Date closeTime;

}