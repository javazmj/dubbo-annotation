package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.MemberActionLog;

import java.util.List;

/**
 *
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/11/2 14:18
 * @Email: 536304123@QQ.COM
 */
public interface MemberActionLogService {

    int insertMore(List<MemberActionLog> list);

    List<MemberActionLog> findByMemberIdAndAction(String memberId,int action);
}
