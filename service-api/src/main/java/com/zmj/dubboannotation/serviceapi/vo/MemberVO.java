package com.zmj.dubboannotation.serviceapi.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by: meijun
 * Date: 2018/10/21 9:41
 */
@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MemberVO implements Serializable {

    private static final long serialVersionUID = 4839149311507816169L;

    @Id
    @GeneratedValue
    private String id;

    private String mobile;

    private String email;

    @JSONField(serialize = false)
    @JsonIgnore
    private String password;

    private String nickName;

    private String head;

    private BigDecimal memberIntegral;

    private BigDecimal maxIntegral;

    private Byte rank;

    @JSONField(serialize = false)
    @JsonIgnore
    private String salt;

    private String lastIp;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    private String name;

    private Integer age;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date birthday;

    private String idCard;

    private String vocation;

    private String cardFace;

    private String cardBack;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date detailCreateTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date detailUpdateTime;
    //验证码
    private String verifyCode;

}
