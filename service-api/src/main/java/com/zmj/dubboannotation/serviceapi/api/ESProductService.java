package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.ESProduct;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/11/26 20:15
 */
public interface ESProductService {

    boolean insert(ESProduct ESProduct);

    boolean delete(String id);

    List<ESProduct> search(String searchContent);

    List<ESProduct> searchUser(Integer pageNumber, Integer pageSize, String searchContent);

    List<ESProduct> searchUserByWeight(String searchContent);

    ESProduct queryESProductById(String id);

    void saveEntity(ESProduct entity);

    void saveEntity(List<ESProduct> entityList);

    List<ESProduct> searchEntity(String searchContent);

    ESProduct getById(String id);
}
