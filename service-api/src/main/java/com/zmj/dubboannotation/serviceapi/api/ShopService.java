package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.MemberShop;

/**
 * Created by: meijun
 * Date: 2018/10/31 22:28
 */
public interface ShopService {

    MemberShop findById(Integer id);

    MemberShop findByMemberId(String memberId);

}
