package com.zmj.dubboannotation.serviceapi.api;

import com.github.pagehelper.PageInfo;
import com.zmj.commoncore.dto.ProductDTO;
import com.zmj.dubboannotation.serviceapi.model.ESProduct;
import com.zmj.dubboannotation.serviceapi.model.MallProduct;
import com.zmj.dubboannotation.serviceapi.model.MemberActionLog;
import com.zmj.dubboannotation.serviceapi.model.MemberProductCollection;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/09/14 22:52
 */
public interface ProductService {

    List<MallProduct> findAll();

    int save(ProductDTO productDTO);

    ProductVO findProductBySkuId(String skuId);

    PageInfo<ProductVO> findProductByParam(Integer pageNum, Integer pageSize, MallProduct product);

    PageInfo<ProductVO> findProductByList(Integer pageNum, Integer pageSize, List<MemberProductCollection> list);

    List<ProductVO> findAllESProduct();

    PageInfo<ProductVO> findRecommend(Integer pageNum, Integer pageSize, List<MemberActionLog> actionLogs);
}
