package com.zmj.dubboannotation.serviceapi.enums;

/**
 * Created by: meijun
 * Date: 2018/3/12 20:04
 */
public interface CodeEnum {
    Integer getCode();
}
