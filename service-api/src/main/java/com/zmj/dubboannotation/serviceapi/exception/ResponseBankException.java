package com.zmj.dubboannotation.serviceapi.exception;


import com.zmj.dubboannotation.serviceapi.api.BaseException;
import com.zmj.dubboannotation.serviceapi.enums.ResultEnum;

/**
 * Created by: meijun
 * Date: 2018/3/27 19:41
 */
public class ResponseBankException extends BaseException {

    private Integer code;

    public ResponseBankException (){
    }

    public ResponseBankException(ResultEnum resultEnum) {
        super(resultEnum );
    }

    public ResponseBankException(Integer code, String message) {
        super(code,message);
    }
}
