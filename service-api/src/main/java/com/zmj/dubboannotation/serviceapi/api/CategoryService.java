package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.MallCategory;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/10/23 21:06
 */
public interface CategoryService {

    MallCategory save(MallCategory category);

    List<MallCategory> findAll();

}
