package com.zmj.dubboannotation.serviceapi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by: meijun
 * Date: 2018/10/31 21:00
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CartProductVO implements Serializable {

    private static final long serialVersionUID = -4258460571920728113L;

    /**
     * 商品ID
     */
    private String id;

    /**
     * 商品skuid
     */
    private String skuProductId;
    /**
     * 商品名称
     */
    private String skuName;
    /**
     * 商品副标题
     */
    private String title;
    /**
     * 商品原价
     */
    private BigDecimal originPrice;
    /**
     * 商品售价
     */
    private BigDecimal sellPrice;

    /**
     * 手机专享价
     */
    private BigDecimal sellMobilePrice;

    /**
     * 商品id 这里是skuid
     */
    private Long productId;

    /**
     * 数量
     */
    private Integer quantity;

    /**
     * 是否选中
     */
    private Byte checked;

    /**
     * 加入购物车时间
     */
    private Date date;



}
