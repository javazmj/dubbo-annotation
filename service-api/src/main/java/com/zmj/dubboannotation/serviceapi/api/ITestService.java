package com.zmj.dubboannotation.serviceapi.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmj.dubboannotation.serviceapi.model.Test;

/**
 * <p>
 * 测试CRUD表 服务类
 * </p>
 *
 * @author zmj
 * @since 2018-11-12
 */
public interface ITestService extends IService<Test> {

}
