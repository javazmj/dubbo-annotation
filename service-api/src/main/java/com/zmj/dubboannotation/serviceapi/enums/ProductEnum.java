package com.zmj.dubboannotation.serviceapi.enums;

import lombok.Getter;

/**
 * Created by: meijun
 * Date: 2017/12/19 10:32
 */
@Getter
public enum ProductEnum implements CodeEnum{

    //是否参与活动
    IS_SET_MEAL_NO(1,"不参与"),
    IS_SET_MEAL_YES(2,"参与"),

    //商品类型
    PRODUTE_TYPE_SKU(1,"单品"),
    PRODUTE_TYPE_GIVE(2,"赠品"),

    //是否上架
    IS_GRUND_UP(1,"在架"),
    IS_GRUND_DOWN(2,"下架"),

    //是否通过审核
    IS_AUDIT_YES(1,"通过"),
    IS_AUDIT_NO(2,"未通过"),

    //购物车是否勾选商品
    CHECKED(1,"已勾选"),
    CHECKED_NO(2,"未勾选"),


    //是否可以和其他优惠券叠加
    IS_SUPERPOSE(1,"可以叠加"),
    IS_SUPERPOSE_NO(2,"不可叠加"),


    //是否使用过该优惠券
    IS_USE(1,"已使用"),
    IS_USE_NO(2,"未使用"),
    ;

    private Integer code;

    private String message;

    ProductEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
