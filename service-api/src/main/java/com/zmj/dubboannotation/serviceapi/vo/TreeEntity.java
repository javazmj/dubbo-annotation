package com.zmj.dubboannotation.serviceapi.vo;

import java.util.List;

/**
 * 树形数据实体接口
 * @param <E>
 * @author jianda
 * @date 2017年5月26日
 */
public interface TreeEntity<E> {

    public Integer getId();

    public Integer getParentId();

    public void setChildren(List<E> children);

}
