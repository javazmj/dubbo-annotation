package com.zmj.dubboannotation.serviceapi.vo;

import com.zmj.dubboannotation.serviceapi.enums.ResultEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * Http请求返回的对象
 * Created by: meijun
 * Date: 2017/12/19 13:16
 */
@Data
public class ResultVO<T> implements Serializable {

    private static final long serialVersionUID = -1896396588821960462L;
    //错误码
    private Integer code;
    //提示消息
    private String  msg;
    //返回内容
    private T data;

    public ResultVO() {
    }

    public ResultVO(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public ResultVO(ResultEnum resultEnum) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
    }
}
