package com.zmj.dubboannotation.serviceapi.enums;

import lombok.Getter;

/**
 * Created by: meijun
 * Date: 2017/12/20 15:37
 */
@Getter
public enum MallEnum implements CodeEnum {
    //del_flag标记
    DEL_FLAG_NORMAL(0,"未删除"),
    DEL_FLAG_DELETE(1,"已删除"),

    //优惠活动类型
    ACTIVITY_REDUCE(1,"满减"),
    ACTIVITY_COUPON(2,"优惠券"),

    //是否主图
    IS_MAIN_YES(1,"主图"),
    IS_MAIN_NO(0,"非主图"),

    //是否显示
    IS_SHOW(1,"不显示"),
    IS_SHOW_NO(0,"显示"),

    //用户行为
    ACTION_LOOK(1,"浏览"),
    ACTION_COLLECT(2,"收藏"),
    ACTION_ADD_CART(3,"加入购物车"),


    LOGIN_TYPE_WEB_PASSWORD(11,"web账号密码登录"),
    LOGIN_TYPE_WEB_VERIFYCODE(12,"web手机号验证码登录"),
    LOGIN_TYPE_MOBILE_PASSWORD(21,"mobile账号密码登录"),
    LOGIN_TYPE_MOBILE_VERIFYCODE(22,".mobile手机号验证码登录"),

    ;

    private Integer code;

    private String  msg;

    MallEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
