package com.zmj.dubboannotation.serviceapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EntityListeners(AuditingEntityListener.class)
public class MallProduct extends BaseModel implements Serializable {

    private static final long serialVersionUID = -5636274042034620439L;

    @Id
    @GeneratedValue
    private String id;

    private String productName;

    private String title;

    private Integer code;

    private Integer shopId;

    private Integer categoryId;

    private Integer brandId;

    private Byte isSetMeal;

    private Integer productGiveawayId;

    private String detail;

    private Integer productStock;

    private String guarantee;

    private Byte produteType;

    private Byte isGrund;

    private Byte isAudit;

    private String skuProductId;

    private String skuId;


}