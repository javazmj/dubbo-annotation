package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.OrderItem;

import java.util.ArrayList;

/**
 * Created by: meijun
 * Date: 2018/09/14 22:52
 */
public interface OrderItemService {

//   @Compensable
   int insert(OrderItem ordersItem);

   /*void insertMore(ArrayList<OrderItem> orderItems);

   void insertCollectList(ArrayList<OrderItem> orderItems);*/
}
