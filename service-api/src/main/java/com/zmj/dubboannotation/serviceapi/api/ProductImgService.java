package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.ProductImage;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/09/14 22:52
 */
public interface ProductImgService {

    List<ProductImage> findAllByProductId(String productId);

}
