package com.zmj.dubboannotation.serviceapi.exception;

import com.zmj.dubboannotation.serviceapi.api.BaseException;
import com.zmj.dubboannotation.serviceapi.enums.ResultEnum;
import lombok.Getter;

/**
 * Created by: meijun
 * Date: 2018/09/15 19:06
 */
@Getter
public class ProductException extends BaseException {

    private Integer code;

    public ProductException() {
    }

    public ProductException(ResultEnum resultEnum) {
        super(resultEnum);
    }

    public ProductException(Integer code, String message) {
        super(code,message);
    }
}
