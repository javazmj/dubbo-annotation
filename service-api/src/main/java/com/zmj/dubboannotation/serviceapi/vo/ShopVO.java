package com.zmj.dubboannotation.serviceapi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by: meijun
 * Date: 2018/10/31 21:00
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ShopVO implements Serializable {

    private static final long serialVersionUID = 4582709941695376071L;

    private Integer id;

    private String memberId;

    private String shopName;

    private String shopHead;

    private String shopPhone;

    private String shopAddress;

    private String shopQrCode;

    private Byte isAudit;

    private Byte isSelfSupport;

    private Date auditTime;



}
