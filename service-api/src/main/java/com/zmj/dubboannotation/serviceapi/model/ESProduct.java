package com.zmj.dubboannotation.serviceapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

/**
 * Created by: meijun
 * Date: 2018/11/26 20:16
 */
@Document(indexName = "mal-dev",type = "product",shards = 1,replicas = 0,refreshInterval = "-1")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" })
@Data
public class ESProduct implements Serializable {

    private static final long serialVersionUID = 1959729452732824999L;

    private String id;

    private String shopName;

    private Integer shopId;

    private String productName;

    private String title;

    private Integer code;

    private Integer categoryId;

    private String  categoryName;

    private Integer brandId;

    private String brandNameEnglish;

    private String brandNameChinese;

    private Byte isSetMeal;

    private Integer productGiveawayId;

    private String detail;

    private Integer productStock;

    private String guarantee;

    private Byte produteType;

    private Byte isGrund;

    private Byte isAudit;

    private String skuProductId;

    private String skuId;

    private String attrValueName;
}
