package com.zmj.dubboannotation.serviceapi.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import java.util.Date;

/**
 * Created by: meijun
 * Date: 2018/10/22 23:23
 */
@Data
@EntityListeners(AuditingEntityListener.class)
@DynamicInsert
@DynamicUpdate
public class BaseModel {

    @CreatedBy
    @JsonIgnore
    protected String createBy;

    @LastModifiedBy
    @JsonIgnore
    protected String updateBy;

//    @JsonIgnore
    @JSONField(serialize = false)
    //@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_time")
    @CreatedDate
    protected Date createTime;

    @JsonIgnore
    @JSONField(serialize = false)
    //@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "update_time")
    @LastModifiedDate
    protected Date updateTime;

    @JsonIgnore
    @Value("0")
    protected Byte delFlag;
}
