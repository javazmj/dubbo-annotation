package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.ShopActivity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/10/23 21:06
 */
public interface ShopActivityService {

    ShopActivity save(ShopActivity shopActivity);

    List<ShopActivity> findAll();

    List<ShopActivity> findValidAllActity(Integer shopId ,Integer type, Date startTime);

    ShopActivity findMaxActivity(Integer shopId , Integer type, Date startTime, BigDecimal shopTotalPrice);

}
