package com.zmj.dubboannotation.serviceapi.exception;

import com.zmj.dubboannotation.serviceapi.enums.ResultEnum;
import lombok.Getter;

/**
 * Created by: meijun
 * Date: 2018/09/15 19:05
 */
@Getter
public class MallAuthorException extends RuntimeException {

    private Integer code;

    public MallAuthorException(ResultEnum resultEnum) {
        super(resultEnum.getMsg() );
        this.code = resultEnum.getCode();
    }

    public MallAuthorException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
