package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.MemberLoginLog;

/**
 * Created by: meijun
 * Date: 2018/10/29 22:09
 */
public interface MemberLoginLogService {

    int insert(MemberLoginLog record);
}
