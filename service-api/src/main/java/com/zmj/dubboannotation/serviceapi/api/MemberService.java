package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.commoncore.dto.LoginDTO;
import com.zmj.dubboannotation.serviceapi.model.MallMember;
import com.zmj.dubboannotation.serviceapi.vo.MemberVO;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/09/14 22:52
 */
public interface MemberService {

    List<MallMember> findAll();

    MemberVO findByMobileAndPassord(LoginDTO loginDTO);

    MemberVO findByEmailAndPassord(LoginDTO loginDTO);

    MallMember findByMobile(String mobile);

    MallMember findByEmail(String email);

    int updateByParam(MemberVO memberVO);

    List<MemberVO> selectByParam(MemberVO memberVO);

    MallMember selectByPrimaryKey(String id);
}
