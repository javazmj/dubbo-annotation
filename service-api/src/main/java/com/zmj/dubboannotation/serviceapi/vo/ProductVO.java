package com.zmj.dubboannotation.serviceapi.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by: meijun
 * Date: 2018/10/26 16:46
 */
@Data
@ToString
public class ProductVO implements Serializable {

    private static final long serialVersionUID = -6014548096125749393L;

    private String id;

    private Integer shopId;

    private Integer categoryId;

    private Integer brandId;

    private Integer productGiveawayId;

    private String detail;

    private Integer productStock;

    private String guarantee;

    private Byte produteType;

    private Byte isGrund;

    private Byte isAudit;

    @JSONField(serialize = false)
    private String skuProductId;

    private String skuId;

    private String skuName;

    private String title;

    private String attrValue;

    private BigDecimal originPrice;

    private BigDecimal sellPrice;

    private BigDecimal sellMobilePrice;

    private Integer dummySalesVolume;

    private String mainImg;
}
