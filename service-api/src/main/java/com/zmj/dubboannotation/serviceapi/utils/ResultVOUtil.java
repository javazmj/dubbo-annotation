package com.zmj.dubboannotation.serviceapi.utils;

import com.zmj.dubboannotation.serviceapi.enums.ResultEnum;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;

/**
 * Created by: meijun
 * Date: 2017/12/19 14:29
 */
public class ResultVOUtil {
    /**
     * 成功返回
     * @param o
     * @return
     */
    public  static ResultVO success (Object o) {
        ResultVO  resultVO = new ResultVO(ResultEnum.SUCCESS);
        resultVO.setData(o);
        return  resultVO;
    }

    /**
     * 成功返回
     * @param msg
     * @param o
     * @return
     */
    public  static ResultVO success (String msg,Object o) {
        ResultVO  resultVO = new ResultVO();
        resultVO.setMsg(msg);
        resultVO.setData(o);
        return  resultVO;
    }

    /**
     * 成功返回
     * @return
     */
    public  static ResultVO success () {
        ResultVO  resultVO = new ResultVO(ResultEnum.SUCCESS);
        return  resultVO;
    }

    /**
     * 失败返回
     * @param resultEnum
     * @return
     */
    public  static ResultVO error (ResultEnum resultEnum) {
        ResultVO  resultVO = new ResultVO(resultEnum);
        return resultVO;
    }

    public  static ResultVO error (String msg) {
        ResultVO  resultVO = new ResultVO();
        resultVO.setCode(-1);
        resultVO.setMsg(msg);
        return  resultVO;
    }
}
