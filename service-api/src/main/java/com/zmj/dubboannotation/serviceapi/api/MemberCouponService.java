package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.MemberCoupon;

/**
 *
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/11/16 15:04
 * @Email: 536304123@QQ.COM
 */
public interface MemberCouponService {

    MemberCoupon findByShopIdAndMemberId(String memberId, Integer shopActivityId);

    MemberCoupon findByMallIdAndMemberId(String memberId, Integer shopActivityId);
}
