package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.MemberAddress;

/**
 * Created by: meijun
 * Date: 2018/11/17 9:53
 */
public interface MemberAddressService {

    MemberAddress findByIdAndMemberId(Integer id,String memberId);
}
