package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.MallActivity;
import com.zmj.dubboannotation.serviceapi.model.ShopActivity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/10/23 21:06
 */
public interface MallActivityService {

    MallActivity save(MallActivity mallActivity);

    List<MallActivity> findAll();

    List<MallActivity> findValidAllActity(Integer type, Date startTime);

    ShopActivity findMaxActivity(Integer type, BigDecimal mallTotalPrice);
}
