package com.zmj.dubboannotation.serviceapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SkuProduct extends BaseModel implements Serializable {

    private static final long serialVersionUID = 989633576298859974L;

    @Id
    @GeneratedValue
    private String id;

    private String skuProductId;

    private String skuCode;

    private String productId;

    private String skuName;

    private String attrValue;

    private BigDecimal originPrice;

    private BigDecimal sellPrice;

    private BigDecimal sellMobilePrice;

    private Byte isGrund = 0;

    private Byte isAudit = 0;

    private Integer productStock;

    private Integer stockWarn;

    private Integer dummySalesVolume;

    private Integer realSalesVolume;

}