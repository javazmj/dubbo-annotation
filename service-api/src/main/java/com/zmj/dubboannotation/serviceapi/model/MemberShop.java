package com.zmj.dubboannotation.serviceapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EntityListeners(AuditingEntityListener.class)
public class MemberShop extends BaseModel implements Serializable {

    private static final long serialVersionUID = -7460363147568998939L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String memberId;

    private String shopName;

    private String shopHead;

    private String shopPhone;

    private String shopAddress;

    private String shopQrCode;

    private Byte isAudit;

    private Byte isSelfSupport;

    private Date auditTime;

}