package com.zmj.dubboannotation.serviceapi.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 测试CRUD表
 * </p>
 *
 * @author zmj
 * @since 2018-11-12
 */
@Data
public class Test implements Serializable {

    private static final long serialVersionUID = -552561240106737703L;

    private Integer id;

    private String name;

    private Integer age;

    private LocalDateTime createTime;


}
