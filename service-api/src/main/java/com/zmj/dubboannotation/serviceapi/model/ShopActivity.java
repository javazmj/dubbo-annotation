package com.zmj.dubboannotation.serviceapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EntityListeners(AuditingEntityListener.class)
public class ShopActivity extends BaseModel implements Serializable {

    private static final long serialVersionUID = 2379889080019952857L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer shopId;

    private String name;

    private Byte type;

    private Integer nums;

    private String useCategoryId;

    private String notUseCategoryId;

    private BigDecimal operationPrice;

    private BigDecimal deratePrice;

    private Byte isSuperpose;

    private Date startTime;

    private Date endTime;


}