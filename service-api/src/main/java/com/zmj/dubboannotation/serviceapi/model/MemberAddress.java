package com.zmj.dubboannotation.serviceapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EntityListeners(AuditingEntityListener.class)
public class MemberAddress extends BaseModel implements Serializable {

    private static final long serialVersionUID = 7518673723064088400L;

    @Id
    @GeneratedValue
    private Integer id;

    private String memberId;

    private String receiverName;

    private String receiverMobile;

    private String receiverAddressCode;

    private String receiverAddress;

    private String receiverAddressInfo;

    private Byte checked;


}