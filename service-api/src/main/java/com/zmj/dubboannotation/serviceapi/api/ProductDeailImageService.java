package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.ProductDetailImage;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/10/28 13:16
 */
public interface ProductDeailImageService {

    List<ProductDetailImage> findAllByProductId(String productId);
}
