package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.dubboannotation.serviceapi.model.SkuProduct;

/**
 * Created by: meijun
 * Date: 2018/10/25 10:22
 */
public interface SKUProducService {

    int save(SkuProduct skuProduct);


}
