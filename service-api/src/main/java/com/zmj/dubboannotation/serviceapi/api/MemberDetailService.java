package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.commoncore.dto.LoginDTO;
import com.zmj.dubboannotation.serviceapi.model.MemberDetail;

/**
 * Created by: meijun
 * Date: 2018/09/14 22:52
 */
public interface MemberDetailService {

    MemberDetail findByMemberId(String memberId);

}
