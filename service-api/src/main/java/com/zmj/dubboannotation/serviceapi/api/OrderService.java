package com.zmj.dubboannotation.serviceapi.api;

import com.zmj.commoncore.dto.OrderDTO;
import com.zmj.dubboannotation.serviceapi.model.MallOrder;

import java.util.List;
import java.util.Map;

/**
 * Created by: meijun
 * Date: 2018/09/14 22:52
 */
public interface OrderService {

    List<MallOrder> findAll();

    List<MallOrder> findByQuery(MallOrder mallOrder);

    int createOrder(Map<Object, Object> cart, OrderDTO orderDTO);

    int createOrder(OrderDTO orderDTO) ;

    List<MallOrder> findWaitPayOrder(MallOrder order);
}
