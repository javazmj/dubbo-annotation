package com.zmj.dubboannotation.serviceapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EntityListeners(AuditingEntityListener.class)
public class MallOrder extends BaseModel implements Serializable {

    private static final long serialVersionUID = 2161453837679270930L;

    @Id
    @GeneratedValue
    private String id;

    private String orderSn;

    private String paySn;

    private String memberId;

    private String receiverName;

    private String receiverMobile;

    private String receiverAddress;

    private String receiverAddressInfo;

    private BigDecimal productPrice;

    private BigDecimal totalPrice;

    private BigDecimal payPrice;

    private BigDecimal postagePrice;

    private BigDecimal mallReducePrice;

    private BigDecimal shopReducePrice;

    private BigDecimal orderIntegral;

    private BigDecimal integralPrice;

    private Byte paymentType;

    private Byte payStatus;

    private Byte orderStatus;

    private Byte isInvoice;

    private Integer invoiceId;

    private String shopActivityId;

    private String mallActivityId;

    private Date paymentTime;

    @Transient
    private List<OrderItem> orderItemList;


}