package com.zmj.dubboannotation.serviceapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EntityListeners(AuditingEntityListener.class)
public class ShopAuditLog extends BaseModel implements Serializable {

    private static final long serialVersionUID = -5446389341177380045L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer shopId;

    private String shopPhone;

    private String shopAddress;

    private String businessLicense;

    private String industryLicense;

    private String otherLicense;

    private Byte isAudit;

    private String failDetail;

    private String auditerName;

    private Date auditTime;

}