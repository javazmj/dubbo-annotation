package com.zmj.dubboannotation.serviceapi.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author meijun
 */
@Entity
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EntityListeners(AuditingEntityListener.class)
public class MallMember extends BaseModel implements Serializable {

    private static final long serialVersionUID = 4415357591511119054L;

    @Id
    @GeneratedValue
    private String id;

    private String mobile;

    private String email;

    @JSONField(serialize = false)
    private String password;

    private String nickName;

    private String head;

    private BigDecimal memberIntegral;

    private BigDecimal maxIntegral;

    private Byte rank;

    @JSONField(serialize = false)
    private String salt;

    private String lastIp;

    private Date lastTime;

}