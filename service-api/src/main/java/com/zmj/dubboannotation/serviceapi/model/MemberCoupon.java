package com.zmj.dubboannotation.serviceapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EntityListeners(AuditingEntityListener.class)
public class MemberCoupon extends BaseModel implements Serializable {

    private static final long serialVersionUID = 4356415212377959451L;

    @Id
    @GeneratedValue
    private Integer id;

    private Integer shopActivityId;

    private Integer mallActivityId;

    private String activityName;

    private BigDecimal operationPrice;

    private BigDecimal deratePrice;

    private Byte isSuperpose;

    private Byte isUse;

    private Date startTime;

    private Date endTime;

}