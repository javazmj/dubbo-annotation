package com.zmj.dubboannotation.serviceapi.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by yide on 2018/7/9.
 */
@Data
public class CategoryVO implements TreeEntity<CategoryVO> {

    public Integer id;

    public String name;

    public Integer parentId;

    public String img;

    public List<CategoryVO> children;



}
