package com.zmj.dubboannotation.consumerweb.controller;

import com.zmj.dubboannotation.serviceapi.model.MallOrder;
import com.zmj.dubboannotation.serviceapi.utils.ResultVOUtil;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/09/15 8:44
 */
@RestController
@RequestMapping("/web")
@Slf4j
public class HelllController {

    @GetMapping("/order")
    @ResponseBody
    public ResultVO<List<MallOrder>> findAllOrder() {
        return null;
    }

    @GetMapping("/say/{say}")
    public ResultVO say(@PathVariable String say) {
        return ResultVOUtil.success(say);
    }

    @GetMapping("/hello")
    public Mono<String> hello() {
        return Mono.just("Welcome to reactive world ~");
    }

    @GetMapping("/test")
    public Mono<ServerResponse> helloCity(ServerRequest request) {
        return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN)
                .body(BodyInserters.fromObject("Hello, City!"));
    }
    @GetMapping("/test1")
    public Mono<ServerResponse> getDate(ServerRequest serverRequest) {
        return ServerResponse.ok().contentType(MediaType.TEXT_PLAIN).body(Mono.just("Today is " + new SimpleDateFormat("yyyy-MM-dd").format(new Date())), String.class);
    }
}
