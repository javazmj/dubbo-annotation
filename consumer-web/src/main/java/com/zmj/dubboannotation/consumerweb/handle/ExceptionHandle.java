package com.zmj.dubboannotation.consumerweb.handle;

import com.zmj.dubboannotation.serviceapi.enums.ResultEnum;
import com.zmj.dubboannotation.serviceapi.exception.MallAuthorException;
import com.zmj.dubboannotation.serviceapi.exception.OrderException;
import com.zmj.dubboannotation.serviceapi.exception.ResponseBankException;
import com.zmj.dubboannotation.serviceapi.utils.ResultVOUtil;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * 统一异常拦截器
 * Created by: meijun
 * Date: 2018/09/15 18:59
 */
//@RestControllerAdvice
@Slf4j
public class ExceptionHandle {

    /**
     * 商城统一拦截
     * @param e
     * @return
     */
    @ExceptionHandler(MallAuthorException.class)
    @ResponseBody
    public ResultVO handlerMallAuthorizeException(MallAuthorException e) {
        return ResultVOUtil.error(e.getMessage());
    }

    /**
     * 订单异常拦截
     * @param e
     * @return
     */
    @ExceptionHandler(value = OrderException.class)
    @ResponseBody
    public ResultVO handlerSellerException(OrderException e) {
        return ResultVOUtil.error(e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResultVO handlerMallAuthorizeExceptionServerError(Exception e) {
        if (e instanceof org.springframework.web.servlet.NoHandlerFoundException) {
            return ResultVOUtil.error(ResultEnum.RESTFUL_API_NOTFOUND);
        }
        if (e instanceof ChangeSetPersister.NotFoundException) {
            return ResultVOUtil.error(ResultEnum.RESTFUL_API_NOTFOUND);
        }
        return ResultVOUtil.error(ResultEnum.RESTFUL_API_ERROR);

    }

    @ExceptionHandler(value = ResponseBankException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handlerResponseBankException() {
    }
}
