package com.zmj.dubboannotation.consumerweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;

/**
 *
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/12/7 14:32
 * @Email: 536304123@QQ.COM
 */
@Configuration
public class TestRouter {

    @Bean
    public RouterFunction<ServerResponse> routerTest(TestHandler testHandler) {
        return RouterFunctions.route(RequestPredicates.GET("/hello")
                .and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),testHandler::helloCity);
    }

    @Autowired
    private TestHandler testHandler;

    @Bean
    public RouterFunction<ServerResponse> timerRouter() {
        return RouterFunctions.route(RequestPredicates.GET("/time"), req -> testHandler.getTime(req))
                .andRoute(RequestPredicates.GET("/date"), testHandler::getDate);  // 这种方式相对于上一行更加简洁
    }
}
