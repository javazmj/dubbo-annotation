package com.zmj.dubboannotation.consumermobile.redis;

/**
 * Created by: meijun
 * Date: 2018/7/29 19:45
 */
public class AccessKey extends BasePrefix {

    public AccessKey(int expireSeconds, String prefix) {
        super(expireSeconds, prefix);
    }

    public static AccessKey withExpire(int expireSeconds) {
        return new AccessKey(expireSeconds,"access");
    }
}
