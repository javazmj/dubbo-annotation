package com.zmj.dubboannotation.consumermobile.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zmj
 * @version 2018/7/3
 */
@ConfigurationProperties(prefix = "web.mobile.login")
@Data
@Component
public class LoginConfig {

    /**
     * 要拦截的url
     */
//    @Value("${web.mobile.login.interceptorurls}")
    private String  interceptorurls;

//    @Value("${web.mobile.login.notInterceptorurls}")
    private String  notInterceptorurls;
}
