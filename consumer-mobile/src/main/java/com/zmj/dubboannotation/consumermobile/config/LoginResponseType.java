package com.zmj.dubboannotation.consumermobile.config;

/**
 * @author zmj
 * @version 2018/1/31
 */
public enum LoginResponseType {

    /**
     * 跳转
     */
    REDIRECT,
    /**
     * 返回json
     */
    JSON


}
