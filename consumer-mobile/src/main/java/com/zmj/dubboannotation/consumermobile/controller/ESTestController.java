package com.zmj.dubboannotation.consumermobile.controller;

import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import org.assertj.core.util.Lists;
import org.elasticsearch.action.get.GetRequestBuilder;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.schema.Maps;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by: meijun
 * Date: 2018/11/29 21:20
 */
@RestController
@RequestMapping("/es/test")
public class ESTestController {

    @Autowired
    private TransportClient client;

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    @GetMapping(produces = "text/html;charset=UTF-8")
    @ResponseBody
    public ResponseEntity get(@RequestParam(name = "id") String id) {
        GetRequestBuilder requestBuilder = client.prepareGet("mal-test", "product", id);
        GetResponse response = requestBuilder.execute().actionGet();
        System.out.println(response.getSourceAsString());
        return new ResponseEntity(response.getSourceAsString(), HttpStatus.OK);
    }

    /**
     * query
     * @return
     */
    @GetMapping(value = "/query",produces = "text/html;charset=UTF-8")
    @ResponseBody
    public ResponseEntity query() {
//        IndexRequestBuilder builder = client.prepareIndex("mal-test", "product");
//        TermQueryBuilder queryBuilder = QueryBuilders.termQuery("stock", 6000);
     /*   SearchResponse searchResponse = client.prepareSearch("小米")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setScroll(new TimeValue(600))
                .setQuery(queryBuilder)
                .setSize(100)
                .execute().actionGet();*/

        SearchResponse searchResponse = client.prepareSearch("mal-test")
                .setTypes("product")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(QueryBuilders.termQuery("stock",20000))
                .setPostFilter(QueryBuilders.rangeQuery("sales").from(12).to(20000))
                .setFrom(0)
                .setSize(100).setExplain(true)
                .execute().actionGet();
        SearchHits hits = searchResponse.getHits();
        ArrayList<Object> list = Lists.newArrayList();
        for (SearchHit hit : hits.getHits()) {
            Map<String, Object> source = hit.getSource();
            list.add(source);
        }
        return new ResponseEntity(list.toString(), HttpStatus.OK);
    }
}
