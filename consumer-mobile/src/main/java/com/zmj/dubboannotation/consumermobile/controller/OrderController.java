package com.zmj.dubboannotation.consumermobile.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zmj.commoncore.constant.RedisConstant;
import com.zmj.commoncore.dto.OrderDTO;
import com.zmj.commoncore.utils.MemberIdUtil;
import com.zmj.dubboannotation.consumermobile.redis.RedisUtil;
import com.zmj.dubboannotation.serviceapi.api.CartService;
import com.zmj.dubboannotation.serviceapi.api.OrderService;
import com.zmj.dubboannotation.serviceapi.enums.OrderEnum;
import com.zmj.dubboannotation.serviceapi.enums.ResultEnum;
import com.zmj.dubboannotation.serviceapi.model.MallOrder;
import com.zmj.dubboannotation.serviceapi.utils.ResultVOUtil;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by: meijun
 * Date: 2018/11/10 17:15
 */
@RestController
@Api(value = "OrderController", description = "订单接口")
@RequestMapping("/order")
@Slf4j
public class OrderController extends BaseController {

    @Reference
    private OrderService orderService;

    @Reference
    private CartService cartService;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 购物车提交订单
     * @param request
     * @param orderDTO
     * @return
     */
    @ApiOperation(value = "购物车购买",notes = "购物车提交订单")
    @PostMapping("/cartCreate")
    public ResultVO create (HttpServletRequest request,OrderDTO orderDTO) {
        String memberId = MemberIdUtil.getCookieMemberId(request);
        orderDTO.setMemberId(memberId);
        Map<Object, Object> cart = redisUtil.getHashEntries(RedisConstant.MEMBER_CART + memberId);
        if(null != cart && cart.size() > 0 ) {
            //购物车选中商品
             ResultVOUtil.success(orderService.createOrder(cart,orderDTO));
            return ResultVOUtil.success(ResultEnum.SUCCESS);
        }
        return null;
    }


    /**
     * 点击商品 立即购买
     * @param orderDTO
     * @return
     */
    @ApiOperation(value = "立即购买",notes = "立即购买提交订单")
    @PostMapping("/onceCreate")
    public ResultVO save (HttpServletRequest request,OrderDTO orderDTO) {
        String memberId = MemberIdUtil.getCookieMemberId(request);
        orderDTO.setMemberId(memberId);

        orderService.createOrder(orderDTO);
        return ResultVOUtil.success(ResultEnum.SUCCESS);
    }

    /**
     * 查询所有已支付的订单 根据订单状态查询
     * 返回的是 orderChild 各个店铺的订单
     * @param request
     * @param orderStatus
     * @return
     */
    @ApiOperation(value = "订单查询",notes = "查询已支付的订单,返回OrderChild")
    @GetMapping
    public ResultVO list (HttpServletRequest request,Integer orderStatus) {
        return null;
    }


    /**
     * 查询未付款的订单
     * 返回的是 mallOrder 总订单
     *
     * @param request
     * @param order
     * @return
     */
    @ApiOperation(value = "订单查询",notes = "查询未支付的订单,返回MallOrder")
    @GetMapping("/waitPayOrder")
    public ResultVO waitOrder (HttpServletRequest request, MallOrder order) {
        String memberId = MemberIdUtil.getCookieMemberId(request);
        order.setMemberId(memberId);
        order.setPayStatus(OrderEnum.PAY_STATUS_WAIT.getCode().byteValue());
        order.setOrderStatus(OrderEnum.ORDER_STATUS_NOT_PAY.getCode().byteValue());
        List<MallOrder> orderList = orderService.findWaitPayOrder(order);
        return ResultVOUtil.success(orderList);
    }

}
