package com.zmj.dubboannotation.consumermobile.aspect;

import com.zmj.commoncore.utils.JsonUtil;
import com.zmj.dubboannotation.consumermobile.config.DomainConfig;
import com.zmj.dubboannotation.consumermobile.config.QiniuConfig;
import com.zmj.dubboannotation.consumermobile.utils.ImgPathUtil;
import com.zmj.dubboannotation.serviceapi.vo.MemberVO;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * 前后端分离所有接口日志记录
 *
 * @author zmj
 * @version 2018/7/03
 */
@Aspect
@Component
@Slf4j
@Order(1)
public class LogAspect {

    @Autowired
    private QiniuConfig qiniuConfig;

    @Autowired
    private DomainConfig domainConfig;
    /**
     * 织入所有controller层来记录日志
     */
    @Pointcut(value = "execution(public * com.zmj.dubboannotation.consumermobile.controller.*.*(..))")
    public void all() {
    }


    @Pointcut(value = "execution(public * com.zmj.dubboannotation.consumermobile.controller.LoginController.login(..))")
    public void login() {
    }

    /**
     * 声明环绕通知
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("all()")
    @Order(1)
    public Object doToken(ProceedingJoinPoint joinPoint) throws Throwable {

        MethodSignature methodSignature =(MethodSignature) joinPoint.getSignature();
        Object[] args = joinPoint.getArgs();
        log.info(String.format("[请求日志] %s.%s ，参数：%s",methodSignature.getDeclaringTypeName(),
                methodSignature.getName(),args == null? "":Arrays.toString(args)));

        //记录起始时间
        long begin = System.currentTimeMillis();

        Object result = "";

        /** 执行目标方法 */
        result= joinPoint.proceed();
        
        /** 记录操作时间 */
        long took = (System.currentTimeMillis() - begin)/1000;
        if (took >= 10) {
            log.error("Service 执行时间为: {}秒", took);
        } else if (took >= 5) {
            log.warn("Service 执行时间为: {}秒", took);
        } else  if (took >= 2) {
            log.info("Service执行时间为: {}秒", took);
        }

        return result;
    }


    @Around("login()")
    @Order(2)
    public Object afterReturning(ProceedingJoinPoint joinPoint) throws Throwable {

        /** 执行目标方法 */
        Object proceed = joinPoint.proceed();

        ResultVO resultVO = JsonUtil.toJavaBean(proceed, ResultVO.class);
        MemberVO memberVO = JsonUtil.toJavaBean(resultVO.getData(), MemberVO.class);
//        memberVO.setHead(qiniuConfig.getPath() + memberVO.getHead());
        try {
            memberVO.setHead(ImgPathUtil.replaceImgPath(domainConfig.getDefaultimg(),domainConfig.getImgurl(),memberVO.getHead()));
        }catch (Exception e) {
            e.printStackTrace();
            memberVO.setHead(domainConfig.getDefaultimg());
            log.error("[afterReturning] login后重新设置头像路径出错" );
        }
        resultVO.setData(memberVO);
        return  resultVO;
    }

}
