package com.zmj.dubboannotation.consumermobile.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by: meijun
 * Date: 2019/01/14 16:10
 */
public class ImgPathUtil {

    public static String replaceImgPath(String defaultimg,String imgurl, String imgPath,String zoomSize) {
        String path = replacePath(defaultimg, imgurl, imgPath);
        if (StringUtils.isNotBlank(zoomSize)) {
            return  path +  zoomSize ;
        }
        return  path;
    }

    public static String replaceImgPath(String defaultimg,String imgurl, String imgPath) {
        return replacePath(defaultimg, imgurl, imgPath);
    }

    private static String replacePath(String defaultimg,String imgurl, String imgPath) {
        if (StringUtils.isBlank(imgPath)) {
            return defaultimg;
        }
        //获取该字符串第2次出现的位置
        int indexOf = StringUtils.ordinalIndexOf(imgPath, "/",2);
        final String img = StringUtils.substring(imgPath, indexOf);

        return  imgurl + img;
    }


}
