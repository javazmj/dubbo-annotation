package com.zmj.dubboannotation.consumermobile.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zmj
 * @version 2018/7/17
 */
@Configuration
@ConfigurationProperties(prefix = "domain")
@Data
public class DomainConfig {

    private String imgurl;

    private String zmjurl;

    private String defaultimg;

    private String defaultimgsize;

}
