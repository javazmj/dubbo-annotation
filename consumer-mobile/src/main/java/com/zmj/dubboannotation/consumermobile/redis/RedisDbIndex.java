package com.zmj.dubboannotation.consumermobile.redis;

/**
 * Created by: meijun
 * Date: 2018/10/21 9:33
 */
public class RedisDbIndex {

    //用户信息
    public static final int MEMBER = 0;

    //购物车
    public static final int CART = 1;

    //商品列表
    public static final int PRODUCT_LIST = 2;

    //商品详情
    public static final int PRODUCT_DETAIL = 3;

    //订单列表
    public static final int ORDER_LIST = 4;

    //字典表
    public static final int DICT = 5;


}
