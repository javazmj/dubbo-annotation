package com.zmj.dubboannotation.consumermobile.config;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by: meijun
 * Date: 2018/11/25 18:50
 */
//@Configuration
//@ConfigurationProperties(prefix = "data.elasticsearch")
@Slf4j
public class ElasticSearchConfig {

    @Bean
    public TransportClient client() throws UnknownHostException {
        // 设置端口名字
        InetSocketTransportAddress node = new InetSocketTransportAddress(InetAddress.getByName("47.93.254.231"), 9200);
        // 设置名字
        Settings settings = Settings.builder().put("cluster.name", "zmjmall").build();

        TransportClient client = new PreBuiltTransportClient(settings);
        client.addTransportAddress(node);

        return client;
    }

}
