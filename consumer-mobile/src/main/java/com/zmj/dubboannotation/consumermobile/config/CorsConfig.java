package com.zmj.dubboannotation.consumermobile.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Created by: meijun
 * Date: 2019/01/06 21:10
 */
@Configuration
@Slf4j
public class CorsConfig {

    private CorsConfiguration buildCOnfig() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedHeader("*");
        configuration.addAllowedOrigin("*");
        configuration.addAllowedMethod("*");
        return configuration;
    }

    @Bean
    public CorsFilter corsFilter() {
        log.info("设置跨域过滤器=====");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**",buildCOnfig());
        return new CorsFilter(source);
    }
}
