package com.zmj.dubboannotation.consumermobile.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zmj.dubboannotation.serviceapi.api.ProductService;
import com.zmj.dubboannotation.serviceapi.api.RedisService;
import com.zmj.dubboannotation.serviceapi.model.MallProduct;
import com.zmj.dubboannotation.serviceapi.utils.ResultVOUtil;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 测试
 * Created by: meijun
 * Date: 2018/09/15 8:44
 */
@RestController
@Api(value = "HelloController", description = "测试swagger2")
@RequestMapping("/test")
@Slf4j
public class HelloController {

    @Reference
    private RedisService redisService;

    @Reference
    private ProductService productService;

    @ApiOperation(value = "查询所有商品", notes = "查询所有商品")
    @GetMapping("/list")
    public ResultVO list() {
        List<MallProduct> all = productService.findAll();
        return ResultVOUtil.success(all);
    }


    @ApiOperation(value = "测试redis集群set", notes = "测试redis集群set")
    @GetMapping("/redis/set")
    public ResultVO set(String key, String value) {
        redisService.set(key, value);
        return ResultVOUtil.success();
    }


    @ApiOperation(value = "测试redis集群get", notes = "测试redis集群get")
    @GetMapping("/redis/get")
    public ResultVO get(String key) {
        String s = redisService.get(key);
        return ResultVOUtil.success(s);
    }


}
