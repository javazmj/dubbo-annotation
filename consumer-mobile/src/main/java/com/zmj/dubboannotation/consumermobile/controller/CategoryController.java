package com.zmj.dubboannotation.consumermobile.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zmj.dubboannotation.serviceapi.api.CategoryService;
import com.zmj.dubboannotation.serviceapi.model.MallCategory;
import com.zmj.dubboannotation.serviceapi.utils.ResultVOUtil;
import com.zmj.dubboannotation.serviceapi.utils.TreeParser;
import com.zmj.dubboannotation.serviceapi.vo.CategoryVO;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 分类控制层
 * Created by: meijun
 * Date: 2018/10/23 21:05
 */
@RestController
@Api(value = "CategoryController", description = "商品分类接口")
@RequestMapping("/category")
@Slf4j
public class CategoryController {

    @Reference
    private CategoryService categoryService;

    /**
     * 获取递归分类列表
     * @param categoryId
     * @return
     */
    @ApiOperation(value = "根据分类ID获取分类列表",notes = "获取分类列表")
    @ApiImplicitParam(name = "categoryId", value = "分类ID", required = true, dataType = "Integer")
    @GetMapping("/list/{categoryId}")
    public ResultVO list(@PathVariable Integer categoryId) {
        List<MallCategory> list = categoryService.findAll();
        List<CategoryVO> resultList = new ArrayList<>();
        for(MallCategory category : list){
            CategoryVO tempCategoryVO = new CategoryVO();
            tempCategoryVO.setId(category.getId());
            tempCategoryVO.setName(category.getCategoryName());
            tempCategoryVO.setParentId(category.getParentId());
            tempCategoryVO.setImg(category.getImg());
            resultList.add(tempCategoryVO);
        }
        List<CategoryVO>  categoryVOList = TreeParser.getTreeList(categoryId,resultList);
        return ResultVOUtil.success(categoryVOList);
    }
}
