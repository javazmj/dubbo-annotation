package com.zmj.dubboannotation.consumermobile.async;

import com.alibaba.dubbo.config.annotation.Reference;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zmj.commoncore.constant.RedisConstant;
import com.zmj.commoncore.utils.IpUtil;
import com.zmj.dubboannotation.consumermobile.redis.UserKey;
import com.zmj.dubboannotation.serviceapi.api.MemberLoginLogService;
import com.zmj.dubboannotation.serviceapi.api.MemberService;
import com.zmj.dubboannotation.serviceapi.api.RedisService;
import com.zmj.dubboannotation.serviceapi.enums.MallEnum;
import com.zmj.dubboannotation.serviceapi.model.MemberLoginLog;
import com.zmj.dubboannotation.serviceapi.vo.MemberVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.concurrent.Future;

/**
 * 异步更新
 *
 * @author zmj
 * @version 2018/7/5
 */
@Component
@Slf4j
public class AsyncTask {

    @Reference
    private MemberService memberService;

    @Reference
    private MemberLoginLogService memberLoginLogService;

    @Reference
    private RedisService redisService;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 登录信息异步更新
     * @param request
     * @param memberVO
     * @return
     */
    @Async
    public Future<MemberVO> doLoin(HttpServletRequest request, MemberVO memberVO) {

        String ip = IpUtil.getIpAddr(request);

        //登录方式 11.web账号密码登录,12.web手机号验证码登录,21mobile账号密码登录,12.mobile手机号验证码登录
        Long loginType = MallEnum.LOGIN_TYPE_WEB_PASSWORD.getCode().longValue();
        log.info("[用户登录] 用户：{}，登录ip: {}",memberVO.getMobile(),ip);
        if(IpUtil.isMobileDevice(request.getHeader("user-agent"))) {
            loginType = MallEnum.LOGIN_TYPE_MOBILE_PASSWORD.getCode().longValue();
        }
        Date date = new Date();
        memberVO.setLastIp(ip);
        memberVO.setLastTime(date);
        memberService.updateByParam(memberVO);

        MemberLoginLog memberLoginLog = new MemberLoginLog();
        memberLoginLog.setLoginIp(ip);
        memberLoginLog.setLoginTime(date);
        memberLoginLog.setLoginType(loginType);
        memberLoginLog.setMemberId(memberVO.getId());
        memberLoginLogService.insert(memberLoginLog);

        //根据token生成redis缓存用户信息
        try {
            String redisUserInfo = objectMapper.writeValueAsString(memberVO);
            //更新 redis 用户信息
            redisService.setForTimeM( String.format("%s-%s",UserKey.getById.getPrefix(),memberVO.getId()),redisUserInfo, RedisConstant.TOKEN_EXPIRE);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            log.error("[用户登录] 登录javabean转json错误...");
        }

        return new AsyncResult<>(memberVO);
    }
}
