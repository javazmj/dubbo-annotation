package com.zmj.dubboannotation.consumermobile.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zmj.dubboannotation.serviceapi.api.ESProductService;
import com.zmj.dubboannotation.serviceapi.api.ProductService;
import com.zmj.dubboannotation.serviceapi.model.ESProduct;
import com.zmj.dubboannotation.serviceapi.model.MallProduct;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;
import org.elasticsearch.action.get.GetRequestBuilder;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/11/25 18:53
 */
@RestController
@RequestMapping("/es")
public class ESearchController {

    @Autowired
    private TransportClient client;

    @Reference
    private ProductService productService;

    @Reference
    private ESProductService esProductService;

    @GetMapping(produces = "text/html;charset=UTF-8")
    @ResponseBody
    public ResponseEntity get(@RequestParam(name = "id") String id) {
        GetRequestBuilder requestBuilder = client.prepareGet("mall-dev", "product", id);
        GetResponse response = requestBuilder.execute().actionGet();
        System.out.println(response.getSourceAsString());
        return new ResponseEntity(response.getSourceAsString(), HttpStatus.OK);
    }

    @PostMapping(produces = "text/html;charset=UTF-8")
    @ResponseBody
    public ResponseEntity updateAll() {
       // esProductService.saveEntity(all);
        return new ResponseEntity("cg", HttpStatus.OK);
    }


    @GetMapping(value = "/get",produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity getById(@RequestParam(name = "id") String id) {
        ESProduct esProduct = esProductService.queryESProductById(id);
        return new ResponseEntity(esProduct, HttpStatus.OK);
    }
}
