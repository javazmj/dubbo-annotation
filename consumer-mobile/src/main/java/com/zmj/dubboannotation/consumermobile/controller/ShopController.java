package com.zmj.dubboannotation.consumermobile.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.zmj.commoncore.utils.MemberIdUtil;
import com.zmj.dubboannotation.serviceapi.api.ProductService;
import com.zmj.dubboannotation.serviceapi.api.ShopActivityService;
import com.zmj.dubboannotation.serviceapi.api.ShopService;
import com.zmj.dubboannotation.serviceapi.model.MallProduct;
import com.zmj.dubboannotation.serviceapi.model.MemberShop;
import com.zmj.dubboannotation.serviceapi.model.ShopActivity;
import com.zmj.dubboannotation.serviceapi.utils.ResultVOUtil;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * 店铺控制层
 *
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/11/1 13:25
 * @Email: 536304123@QQ.COM
 */
@RestController
@RequestMapping("/shop")
public class ShopController  {

    @Reference
    private ShopService shopService;

    @Reference
    private ProductService productService;

    @Reference
    private ShopActivityService shopActivityService;

    /**
     * 根据用户id 获取用户自己的店铺信息
     * @return
     */
    @GetMapping
    public ResultVO get (HttpServletRequest request, HttpServletResponse response)  {
        String memberId = MemberIdUtil.getCookieMemberId(request);
        MemberShop shop = shopService.findByMemberId(memberId);
        return ResultVOUtil.success(shop);
    }

    /**
     * 获取店铺信息
     * @return
     */
    @GetMapping("/{shopId}")
    public ResultVO get (@PathVariable Integer shopId)  {
        MemberShop shop = shopService.findById(shopId);
        return ResultVOUtil.success(shop);
    }

    /**
     * 创建店铺
     * @return
     * @throws Exception
     */
    @PostMapping
    public ResultVO create () {
        return null;
    }

    /**
     * 查询某个店铺的所有商品
     * @param shopId
     * @return
     */
    @GetMapping("/list/{shopId}")
    public ResultVO get (@RequestParam(defaultValue = "1") Integer pageNum
                        ,@RequestParam(defaultValue = "20")Integer pageSize
                        ,@PathVariable Integer shopId) {
        MallProduct mallProduct = new MallProduct();
        mallProduct.setShopId(shopId);
        PageInfo<ProductVO> pageInfo = productService.findProductByParam(pageNum, pageSize, mallProduct);
        return ResultVOUtil.success(pageInfo);
    }

    /**
     * 根据分类获取店铺的可用活动
     * @param shopId
     * @param type
     * @return
     */
    @GetMapping("/activity")
    public ResultVO coupon (Integer shopId,Integer type) {
       List<ShopActivity> list = shopActivityService.findValidAllActity(shopId, type,new Date());
       return ResultVOUtil.success(list);
    }


}
