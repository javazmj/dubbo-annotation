package com.zmj.dubboannotation.consumermobile.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zmj.dubboannotation.serviceapi.api.MallActivityService;
import com.zmj.dubboannotation.serviceapi.enums.MallEnum;
import com.zmj.dubboannotation.serviceapi.model.MallActivity;
import com.zmj.dubboannotation.serviceapi.model.MallProduct;
import com.zmj.dubboannotation.serviceapi.utils.ResultVOUtil;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/11/03 20:35
 */
@RestController
@Api(value = "MallActivityController", description = "商城活动接口")
@RequestMapping("/mallActivity")
public class MallActivityController {

    @Reference
    private MallActivityService mallActivityService;

    @ApiOperation(value = "创建商城活动",notes = "创建商城活动")
    @PostMapping
    public ResultVO create(MallActivity mallActivity) {
        MallActivity save = mallActivityService.save(mallActivity);
        return ResultVOUtil.success(save);
    }


    @ApiOperation(value = "返回商城现有活动列表",notes = "返回商城现有活动列表")
    @GetMapping
    public ResultVO list() throws ParseException {
        Date date = DateUtils.parseDate("2018-11-07","yyyy-MM-dd");
        List<MallActivity> all = mallActivityService.findValidAllActity(MallEnum.ACTIVITY_REDUCE.getCode(),date);
        return ResultVOUtil.success(all);
    }


}
