package com.zmj.dubboannotation.consumermobile.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.zmj.commoncore.dto.ProductDTO;
import com.zmj.commoncore.utils.MemberIdUtil;
import com.zmj.dubboannotation.consumermobile.config.DomainConfig;
import com.zmj.dubboannotation.consumermobile.utils.ImgPathUtil;
import com.zmj.dubboannotation.serviceapi.api.MemberActionLogService;
import com.zmj.dubboannotation.serviceapi.api.ProductDeailImageService;
import com.zmj.dubboannotation.serviceapi.api.ProductImgService;
import com.zmj.dubboannotation.serviceapi.api.ProductService;
import com.zmj.dubboannotation.serviceapi.enums.ResultEnum;
import com.zmj.dubboannotation.serviceapi.model.MemberActionLog;
import com.zmj.dubboannotation.serviceapi.model.ProductDetailImage;
import com.zmj.dubboannotation.serviceapi.model.ProductImage;
import com.zmj.dubboannotation.serviceapi.utils.ResultVOUtil;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 商品控制层
 * Created by: meijun
 * Date: 2018/10/26 11:05
 */
@RestController
@RequestMapping("/product")
@Slf4j
public class ProductController {

    @Reference
    private ProductService productService;

    @Reference
    private ProductImgService productImgService;

    @Reference
    private ProductDeailImageService productDeailImageService;

    @Reference
    private MemberActionLogService memberActionLogService;

    @Autowired
    private DomainConfig domainConfig;

    /**
     * 查询所有sku商品
     * @return
     */
    @GetMapping
    public ResultVO list(@RequestParam(defaultValue = "1") Integer pageNum
                        ,@RequestParam(defaultValue = "20")Integer pageSize) {
        PageInfo<ProductVO> pageInfo = productService.findProductByParam(pageNum, pageSize,null);
        for (ProductVO productVO: pageInfo.getList()) {
            String attrValue = productVO.getAttrValue();
            JSONArray objects = JSONArray.parseArray(attrValue);

            productVO.setAttrValue( JSONObject.toJSON(objects).toString());
            //更改图片路径
            productVO.setMainImg(ImgPathUtil.replaceImgPath(domainConfig.getDefaultimg(),domainConfig.getImgurl(),
                    productVO.getMainImg(),domainConfig.getDefaultimgsize()));

        }
        return  ResultVOUtil.success(pageInfo);
    }

    /**
     * 根据skuId查询商品
     * @return
     */
    @GetMapping("/{skuProductId}")
    public ResultVO getProduct(@PathVariable(name = "skuProductId",required = true) String skuId) {
        ProductVO product = productService.findProductBySkuId(skuId);
        if(null == product) {
            return ResultVOUtil.error(ResultEnum.PRODUCT_EMPTY);
        }
        return  ResultVOUtil.success(product);
    }


    /**
     * 获取商品图片列表
     * @param productId
     * @return
     */
    @GetMapping("/img/{productId}")
    public ResultVO getProductImg(@PathVariable(name = "productId",required = true) String productId) {
        List<ProductImage> list = productImgService.findAllByProductId(productId);
        list.stream().filter(bean -> {
            bean.setUrl(ImgPathUtil.replaceImgPath(domainConfig.getDefaultimg(),domainConfig.getImgurl(),bean.getUrl(),domainConfig.getDefaultimgsize()));
            return true;
        }).collect(Collectors.toList());
        return  ResultVOUtil.success(list);
    }

    /**
     * 获取商品详情图片列表
     * @param productId
     * @return
     */
    @GetMapping("/detailImg/{productId}")
    public ResultVO getProductDetailImg(@PathVariable(name = "productId",required = true)String productId) {
        List<ProductDetailImage> list = productDeailImageService.findAllByProductId(productId);
        list.stream().filter(bean -> {
            bean.setUrl(ImgPathUtil.replaceImgPath(domainConfig.getDefaultimg(),domainConfig.getImgurl(),bean.getUrl(),domainConfig.getDefaultimgsize()));
            return true;
        }).collect(Collectors.toList());
        return  ResultVOUtil.success(list);
    }

    /**
     * 创建商品
     * @param productDTO
     * @return
     */
    @PostMapping("/create")
    public ResultVO create(ProductDTO productDTO) {
        int save = productService.save(productDTO);
        if(save >= 0) {
            return ResultVOUtil.success(save);
        }
        return ResultVOUtil.error(ResultEnum.PRPDUCT_CREATE_ERROR);
    }


    /**
     * 推荐商品
     * @return
     */
    @GetMapping("/recommend")
    public ResultVO recommend(HttpServletRequest request,@RequestParam(defaultValue = "1") Integer pageNum
            ,@RequestParam(defaultValue = "20")Integer pageSize) {
        String memberId = MemberIdUtil.getCookieMemberId(request);
        //如果是登录用户,则根据最近浏览的商品分类/价格推荐
        if(StringUtils.isNotBlank(memberId)) {
            List<MemberActionLog> actionLogs = memberActionLogService.findByMemberIdAndAction(memberId, 1);
            PageInfo<ProductVO> pageInfo = productService.findRecommend(pageNum, pageSize,actionLogs);
            return null;
        }
        //如果未登录,则推荐销量最好的

        return ResultVOUtil.error(ResultEnum.PRPDUCT_CREATE_ERROR);
    }

}
