package com.zmj.dubboannotation.consumermobile.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.zmj.commoncore.utils.MemberIdUtil;
import com.zmj.dubboannotation.serviceapi.api.ProductCollectionService;
import com.zmj.dubboannotation.serviceapi.api.ProductService;
import com.zmj.dubboannotation.serviceapi.enums.ResultEnum;
import com.zmj.dubboannotation.serviceapi.model.MallProduct;
import com.zmj.dubboannotation.serviceapi.model.MemberProductCollection;
import com.zmj.dubboannotation.serviceapi.utils.ResultVOUtil;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;
import com.zmj.dubboannotation.serviceapi.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 *  商品收藏控制层
 *
 * @Version: 1.0
 * @Author: 周美军
 * @Date: 2018/11/1 17:52
 * @Email: 536304123@QQ.COM
 */
@RestController
@Api(value = "prodcutCollection", description = "商品收藏接口")
@RequestMapping("/prodcutCollection")
public class ProductCollectionController {

    @Reference
    private ProductCollectionService productCollectionService;

    @Reference
    private ProductService productService;

    /**
     * 商品添加收藏
     * @param request
     * @param collection
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "商品添加收藏",notes = "商品添加收藏")
    @PostMapping
    public ResultVO create(HttpServletRequest request,@Valid MemberProductCollection collection){
        String memberId = MemberIdUtil.getCookieMemberId(request);
        collection.setMemberId(memberId);
        MemberProductCollection memberProductCollection = productCollectionService.selectByProductId(memberId, collection.getProductId());
        if(null != memberProductCollection) {
            return ResultVOUtil.error(ResultEnum.PRODUCT_COLLECTION_IS_EXIST.getMsg());
        }
        productCollectionService.save(collection);
        return ResultVOUtil.success(ResultEnum.COLLECTION_MSG.getMsg());
    }

    /**
     * 收藏商品列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "收藏商品列表",notes = "收藏商品列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", required = false, dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize", value = "每页条数", required = false, dataType = "Integer")
    })
    @GetMapping
    public ResultVO list(HttpServletRequest request,@RequestParam(defaultValue = "1") Integer pageNum
                        ,@RequestParam(defaultValue = "20")Integer pageSize){
        String memberId = MemberIdUtil.getCookieMemberId(request);
        List<MemberProductCollection> collectionList = productCollectionService.findAllById(memberId);
        PageInfo<ProductVO> page = productService.findProductByList(pageNum, pageSize, collectionList);
        return ResultVOUtil.success(page);
    }

    /**
     * 删除收藏产品
     * @param productId
     * @return
     */
    @ApiOperation(value = "删除收藏产品",notes = "删除收藏产品")
    @ApiImplicitParam(name = "skuId", value = "skuId", required = true, dataType = "Long")
    @DeleteMapping
    public ResultVO delete(HttpServletRequest request,@NotNull String productId) {
        String memberId = MemberIdUtil.getCookieMemberId(request);
        productCollectionService.deleteByProductId(memberId,productId);
        return ResultVOUtil.success(ResultEnum.DELETE_MSG.getMsg());
    }
}
