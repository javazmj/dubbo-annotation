package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MemberProductCollection;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/11/01 20:22
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberProductCollectionMapperTest {

    @Autowired
    private MemberProductCollectionMapper mapper;

    @Test
    public void insert() {
        MemberProductCollection productCollection = new MemberProductCollection();
        productCollection.setMemberId("1055276244765159425");
        productCollection.setProductId("1055735020899155970");
        int insert = mapper.insert(productCollection);

        Assert.assertEquals(1,insert);
    }

    @Test
    public void findAllById() {
    }
}