package com.zmj.dubboannotation.productservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MallCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/10/23 21:31
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MalCategoryRepositoryTest {

    @Autowired
    private MalCategoryRepository repository;

    @Test
    public void save() {

        MallCategory mallCategory = new MallCategory();
        mallCategory.setCategoryCode(11);
        mallCategory.setCategoryName("服装1111");
        mallCategory.setParentId(0);
        mallCategory.setImg("http://img14.360buyimg.com/cms/jfs/t27496/185/1097732947/19903/978e444c/5bc18d80N45e219f0.jpg");
        mallCategory.setSort(1);
        mallCategory.setCreateTime(new Date());
        mallCategory.setCreateBy("junit");

        MallCategory save = repository.save(mallCategory);
        Assert.assertNotNull(save.getId());

    }
}