package com.zmj.dubboannotation.productservice.mapper;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.zmj.dubboannotation.serviceapi.model.MallProduct;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/10/23 22:35
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MallProductMapperTest {

    @Autowired
    private MallProductMapper productMapper;

    @Autowired
    private ProductAttributeMapper attributeMapper;

    @Autowired
    private ProductAttrValueMapper attrValueMapper;

    @Autowired
    private SkuProductMapper skuProductMapper;

    @Test
    public void insert() {

        MallProduct product = new MallProduct();
        product.setId(IdWorker.getIdStr());
        product.setBrandId(1);
        product.setCategoryId(1);
        product.setCode(100);
        product.setDetail("这是第一个junit测试创建的商品");
        product.setGuarantee("只包装 不售后");
        product.setIsAudit(Integer.valueOf(0).byteValue());
        product.setIsGrund(Integer.valueOf(0).byteValue());
        product.setIsSetMeal(Integer.valueOf(0).byteValue());
        product.setProductName("测试junit商品");
        product.setShopId(1);
        product.setProduteType(Integer.valueOf(0).byteValue());

        int insert = productMapper.insert(product);


        Assert.assertEquals(insert,1);
    }


    @Test
    public void findProductByParam() {

        MallProduct product = new MallProduct();
        product.setBrandId(2);
        product.setCategoryId(8);
        product.setShopId(1);

        List<ProductVO> productByParam = productMapper.findProductByParam(product);

        Assert.assertEquals(4,productByParam.size());
    }

    @Test
    public void findProductBySkuId() {

        ProductVO productBySkuId = productMapper.findProductBySkuId("1055735020693635073");

        Assert.assertNotNull(productBySkuId);
    }


}