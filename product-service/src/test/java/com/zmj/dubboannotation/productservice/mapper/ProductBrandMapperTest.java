package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductBrand;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/10/25 17:03
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductBrandMapperTest {

    @Autowired
    private ProductBrandMapper productBrandMapper;

    @Test
    public void insert() {

        ProductBrand productBrand = new ProductBrand();
        productBrand.setBrandNameChinese("阿迪达斯");
        productBrand.setBrandNameEnglish("adidas");

        int insert = productBrandMapper.insert(productBrand);
        Assert.assertEquals(1,insert);
    }
}