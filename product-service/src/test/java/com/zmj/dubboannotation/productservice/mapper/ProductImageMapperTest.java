package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductImage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/10/28 13:12
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductImageMapperTest {

    @Autowired
    private ProductImageMapper productImageMapper;

    @Test
    public void findAllByProductId() {

        List<ProductImage> allByProductId = productImageMapper.findAllByProductId("1055735020089655298");
        Assert.assertNotEquals(0,allByProductId.size());

    }
}