package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductComment;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/10/28 13:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCommentMapperTest {


    @Autowired
    private ProductCommentMapper productCommentMapper;

    @Test
    public void insert() {
        ProductComment productComment = new ProductComment();
        productComment.setProductId("1055735020089655298");
        productComment.setMemberId("1055276244765159425");
        productComment.setComment("这个东西超级好用...");
        productComment.setCommentGrade(Integer.valueOf(5).byteValue());
        productComment.setBuyNurms("1055735020693635073-5-7");
        productComment.setIsAudit(Integer.valueOf(1).byteValue());
        productComment.setIsShow(Integer.valueOf(1).byteValue());

        int insert = productCommentMapper.insert(productComment);

        Assert.assertEquals(1,insert);

    }

    @Test
    public void findAllByProductId() {
        List<ProductComment> list = productCommentMapper.findAllByProductId("1055735020089655298");
        Assert.assertEquals(1,list.size());
    }

}