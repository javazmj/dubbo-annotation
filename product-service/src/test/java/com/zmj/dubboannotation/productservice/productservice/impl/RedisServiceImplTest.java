package com.zmj.dubboannotation.productservice.productservice.impl;

import com.zmj.dubboannotation.serviceapi.api.RedisService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author: 周美军
 * @Date: 2018/12/7 16:10
 * @Email: 536304123@QQ.COM
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisServiceImplTest {

    @Autowired
    private RedisService redisService;


    @Test
    public void set() {
        redisService.set("key3","key333");
    }

    @Test
    public void get() {
        String redis = redisService.get("redis");
        Assert.assertNotNull(redis);

    }
}