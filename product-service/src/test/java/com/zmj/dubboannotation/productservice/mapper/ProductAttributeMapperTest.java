package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductAttribute;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/10/25 17:07
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductAttributeMapperTest {

    @Autowired
    private ProductAttributeMapper productAttributeMapper;

    @Test
    public void insert() {

        ProductAttribute attribute = new ProductAttribute();
        attribute.setAttrName("颜色");
        attribute.setAttrCode(1000);
        attribute.setCategoryId(3);

        int insert = productAttributeMapper.insert(attribute);
        Assert.assertEquals(1,insert);
    }
}