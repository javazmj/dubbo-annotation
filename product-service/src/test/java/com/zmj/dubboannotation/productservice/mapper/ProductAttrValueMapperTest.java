package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductAttrValue;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by: meijun
 * Date: 2018/10/25 17:16
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductAttrValueMapperTest {

    @Autowired
    private ProductAttrValueMapper attrValueMapper;

    @Test
    public void insert() {

        ProductAttrValue attrValue = new ProductAttrValue();
        attrValue.setAttributeId(1);
        attrValue.setAttrCode(1000);
        attrValue.setAttrVale("黑色");
        attrValue.setSort(Integer.valueOf(1).byteValue());

        int insert = attrValueMapper.insert(attrValue);
        Assert.assertEquals(1,insert);
    }
}