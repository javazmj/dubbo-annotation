package com.zmj.dubboannotation.productservice.provider;

import com.zmj.commoncore.dto.ProductCommentDTO;
import org.apache.ibatis.jdbc.SQL;

/**
 * Created by: meijun
 * Date: 2018/10/28 20:30
 */
public class ProductCommentProvider {

    private final String TABLE = "product_comment";

    public String findByQuery(ProductCommentDTO productCommentDTO) {
        //优化
        return new SQL(){
            {
                SELECT("id, product_id, member_id, order_id, comment, " +
                        "comment_grade, buy_nurms, is_audit, is_show, create_time")
                .FROM(TABLE);
                if (null != productCommentDTO.getMemberId() ) {
                    WHERE("member_id = #{memberId}");
                }
                if (null != productCommentDTO.getOrderId() ) {
                    WHERE("order_id = #{orderId}");
                }
                if(null != productCommentDTO.getProductId()) {
                    WHERE("product_id = #{productId}");
                }
            }
        }.toString();
        /*SQL sql = new SQL().SELECT("id, product_id, member_id, order_id, comment, " +
                "comment_grade, buy_nurms, is_audit, is_show, create_time").FROM(TABLE);

        Long memberId = productCommentDTO.getMemberId();
        if(null != memberId) {
            sql.WHERE("member_id = #{memberId}");
        }

        Long orderId = productCommentDTO.getOrderId();
        if(null != orderId) {
            sql.WHERE("order_id = #{orderId}");
        }

        Long productId = productCommentDTO.getProductId();
        if(null != productId) {
            sql.WHERE("product_id = #{productId}");
        }
        return sql.toString();*/
    }
}
