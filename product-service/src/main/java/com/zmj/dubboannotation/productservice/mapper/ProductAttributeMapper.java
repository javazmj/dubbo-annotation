package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductAttribute;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface ProductAttributeMapper {
    @Delete({
        "delete from product_attribute",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into product_attribute (id, category_id, ",
        "shop_id, attr_name, ",
        "attr_code, description, ",
        "sort, isSKU, isSearch, ",
        "isShow, create_time, ",
        "update_time, create_by, ",
        "update_by, del_flag)",
        "values (#{id,jdbcType=INTEGER}, #{categoryId,jdbcType=INTEGER}, ",
        "#{shopId,jdbcType=INTEGER}, #{attrName,jdbcType=VARCHAR}, ",
        "#{attrCode,jdbcType=INTEGER}, #{description,jdbcType=VARCHAR}, ",
        "#{sort,jdbcType=INTEGER}, #{issku,jdbcType=INTEGER}, #{issearch,jdbcType=INTEGER}, ",
        "#{isshow,jdbcType=INTEGER}, #{createTime,jdbcType=TIMESTAMP}, ",
        "#{updateTime,jdbcType=TIMESTAMP}, #{createBy,jdbcType=VARCHAR}, ",
        "#{updateBy,jdbcType=VARCHAR}, #{delFlag,jdbcType=TINYINT})"
    })
    int insert(ProductAttribute record);

    @Select({
        "select",
        "id, category_id, shop_id, attr_name, attr_code, description, sort, isSKU, isSearch, ",
        "isShow, create_time, update_time, create_by, update_by, del_flag",
        "from product_attribute",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="category_id", property="categoryId", jdbcType=JdbcType.INTEGER),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="attr_name", property="attrName", jdbcType=JdbcType.VARCHAR),
        @Result(column="attr_code", property="attrCode", jdbcType=JdbcType.INTEGER),
        @Result(column="description", property="description", jdbcType=JdbcType.VARCHAR),
        @Result(column="sort", property="sort", jdbcType=JdbcType.INTEGER),
        @Result(column="isSKU", property="issku", jdbcType=JdbcType.INTEGER),
        @Result(column="isSearch", property="issearch", jdbcType=JdbcType.INTEGER),
        @Result(column="isShow", property="isshow", jdbcType=JdbcType.INTEGER),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    ProductAttribute selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, category_id, shop_id, attr_name, attr_code, description, sort, isSKU, isSearch, ",
        "isShow, create_time, update_time, create_by, update_by, del_flag",
        "from product_attribute"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="category_id", property="categoryId", jdbcType=JdbcType.INTEGER),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="attr_name", property="attrName", jdbcType=JdbcType.VARCHAR),
        @Result(column="attr_code", property="attrCode", jdbcType=JdbcType.INTEGER),
        @Result(column="description", property="description", jdbcType=JdbcType.VARCHAR),
        @Result(column="sort", property="sort", jdbcType=JdbcType.INTEGER),
        @Result(column="isSKU", property="issku", jdbcType=JdbcType.INTEGER),
        @Result(column="isSearch", property="issearch", jdbcType=JdbcType.INTEGER),
        @Result(column="isShow", property="isshow", jdbcType=JdbcType.INTEGER),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<ProductAttribute> selectAll();

    @Update({
        "update product_attribute",
        "set category_id = #{categoryId,jdbcType=INTEGER},",
          "shop_id = #{shopId,jdbcType=INTEGER},",
          "attr_name = #{attrName,jdbcType=VARCHAR},",
          "attr_code = #{attrCode,jdbcType=INTEGER},",
          "description = #{description,jdbcType=VARCHAR},",
          "sort = #{sort,jdbcType=INTEGER},",
          "isSKU = #{issku,jdbcType=INTEGER},",
          "isSearch = #{issearch,jdbcType=INTEGER},",
          "isShow = #{isshow,jdbcType=INTEGER},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "create_by = #{createBy,jdbcType=VARCHAR},",
          "update_by = #{updateBy,jdbcType=VARCHAR},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ProductAttribute record);


    List<ProductAttribute> findByCategoryId(Integer categoryId);
}