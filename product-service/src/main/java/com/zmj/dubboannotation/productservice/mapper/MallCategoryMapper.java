package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.MallCategory;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface MallCategoryMapper {
    @Delete({
        "delete from mall_category",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into mall_category (id, parent_id, ",
        "category_name, category_code, ",
        "img, sort, create_time, ",
        "update_time, create_by, ",
        "update_by, del_flag)",
        "values (#{id,jdbcType=INTEGER}, #{parentId,jdbcType=INTEGER}, ",
        "#{categoryName,jdbcType=VARCHAR}, #{categoryCode,jdbcType=INTEGER}, ",
        "#{img,jdbcType=VARCHAR}, #{sort,jdbcType=INTEGER}, #{createTime,jdbcType=TIMESTAMP}, ",
        "#{updateTime,jdbcType=TIMESTAMP}, #{createBy,jdbcType=VARCHAR}, ",
        "#{updateBy,jdbcType=VARCHAR}, #{delFlag,jdbcType=TINYINT})"
    })
    int insert(MallCategory record);

    @Select({
        "select",
        "id, parent_id, category_name, category_code, img, sort, create_time, update_time, ",
        "create_by, update_by, del_flag",
        "from mall_category",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="parent_id", property="parentId", jdbcType=JdbcType.INTEGER),
        @Result(column="category_name", property="categoryName", jdbcType=JdbcType.VARCHAR),
        @Result(column="category_code", property="categoryCode", jdbcType=JdbcType.INTEGER),
        @Result(column="img", property="img", jdbcType=JdbcType.VARCHAR),
        @Result(column="sort", property="sort", jdbcType=JdbcType.INTEGER),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    MallCategory selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, parent_id, category_name, category_code, img, sort, create_time, update_time, ",
        "create_by, update_by, del_flag",
        "from mall_category"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="parent_id", property="parentId", jdbcType=JdbcType.INTEGER),
        @Result(column="category_name", property="categoryName", jdbcType=JdbcType.VARCHAR),
        @Result(column="category_code", property="categoryCode", jdbcType=JdbcType.INTEGER),
        @Result(column="img", property="img", jdbcType=JdbcType.VARCHAR),
        @Result(column="sort", property="sort", jdbcType=JdbcType.INTEGER),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<MallCategory> selectAll();

    @Update({
        "update mall_category",
        "set parent_id = #{parentId,jdbcType=INTEGER},",
          "category_name = #{categoryName,jdbcType=VARCHAR},",
          "category_code = #{categoryCode,jdbcType=INTEGER},",
          "img = #{img,jdbcType=VARCHAR},",
          "sort = #{sort,jdbcType=INTEGER},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "create_by = #{createBy,jdbcType=VARCHAR},",
          "update_by = #{updateBy,jdbcType=VARCHAR},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MallCategory record);
}