package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.SkuProduct;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface SkuProductMapper {
    @Delete({
        "delete from sku_product",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
        "insert into sku_product (id, sku_product_id, ",
        "sku_code, product_id, ",
        "sku_name, attr_value, ",
        "origin_price, sell_price, ",
        "sell_mobile_price, is_grund, ",
        "is_audit, product_stock, ",
        "stock_warn, dummy_sales_volume, ",
        "real_sales_volume)",
        "values (#{id,jdbcType=VARCHAR}, #{skuProductId,jdbcType=VARCHAR}, ",
        "#{skuCode,jdbcType=VARCHAR}, #{productId,jdbcType=VARCHAR}, ",
        "#{skuName,jdbcType=VARCHAR}, #{attrValue,jdbcType=VARCHAR}, ",
        "#{originPrice,jdbcType=DECIMAL}, #{sellPrice,jdbcType=DECIMAL}, ",
        "#{sellMobilePrice,jdbcType=DECIMAL}, #{isGrund,jdbcType=TINYINT}, ",
        "#{isAudit,jdbcType=TINYINT}, #{productStock,jdbcType=INTEGER}, ",
        "#{stockWarn,jdbcType=INTEGER}, #{dummySalesVolume,jdbcType=INTEGER}, ",
        "#{realSalesVolume,jdbcType=INTEGER})"
    })
    int insert(SkuProduct record);

    @Select({
        "select",
        "id, sku_product_id, sku_code, product_id, sku_name, attr_value, origin_price, ",
        "sell_price, sell_mobile_price, is_grund, is_audit, product_stock, stock_warn, ",
        "dummy_sales_volume, real_sales_volume",
        "from sku_product",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="sku_product_id", property="skuProductId", jdbcType=JdbcType.VARCHAR),
        @Result(column="sku_code", property="skuCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="sku_name", property="skuName", jdbcType=JdbcType.VARCHAR),
        @Result(column="attr_value", property="attrValue", jdbcType=JdbcType.VARCHAR),
        @Result(column="origin_price", property="originPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="sell_price", property="sellPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="sell_mobile_price", property="sellMobilePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="is_grund", property="isGrund", jdbcType=JdbcType.TINYINT),
        @Result(column="is_audit", property="isAudit", jdbcType=JdbcType.TINYINT),
        @Result(column="product_stock", property="productStock", jdbcType=JdbcType.INTEGER),
        @Result(column="stock_warn", property="stockWarn", jdbcType=JdbcType.INTEGER),
        @Result(column="dummy_sales_volume", property="dummySalesVolume", jdbcType=JdbcType.INTEGER),
        @Result(column="real_sales_volume", property="realSalesVolume", jdbcType=JdbcType.INTEGER)
    })
    SkuProduct selectByPrimaryKey(String id);

    @Select({
        "select",
        "id, sku_product_id, sku_code, product_id, sku_name, attr_value, origin_price, ",
        "sell_price, sell_mobile_price, is_grund, is_audit, product_stock, stock_warn, ",
        "dummy_sales_volume, real_sales_volume",
        "from sku_product"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="sku_product_id", property="skuProductId", jdbcType=JdbcType.VARCHAR),
        @Result(column="sku_code", property="skuCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="sku_name", property="skuName", jdbcType=JdbcType.VARCHAR),
        @Result(column="attr_value", property="attrValue", jdbcType=JdbcType.VARCHAR),
        @Result(column="origin_price", property="originPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="sell_price", property="sellPrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="sell_mobile_price", property="sellMobilePrice", jdbcType=JdbcType.DECIMAL),
        @Result(column="is_grund", property="isGrund", jdbcType=JdbcType.TINYINT),
        @Result(column="is_audit", property="isAudit", jdbcType=JdbcType.TINYINT),
        @Result(column="product_stock", property="productStock", jdbcType=JdbcType.INTEGER),
        @Result(column="stock_warn", property="stockWarn", jdbcType=JdbcType.INTEGER),
        @Result(column="dummy_sales_volume", property="dummySalesVolume", jdbcType=JdbcType.INTEGER),
        @Result(column="real_sales_volume", property="realSalesVolume", jdbcType=JdbcType.INTEGER)
    })
    List<SkuProduct> selectAll();

    @Update({
        "update sku_product",
        "set sku_product_id = #{skuProductId,jdbcType=VARCHAR},",
          "sku_code = #{skuCode,jdbcType=VARCHAR},",
          "product_id = #{productId,jdbcType=VARCHAR},",
          "sku_name = #{skuName,jdbcType=VARCHAR},",
          "attr_value = #{attrValue,jdbcType=VARCHAR},",
          "origin_price = #{originPrice,jdbcType=DECIMAL},",
          "sell_price = #{sellPrice,jdbcType=DECIMAL},",
          "sell_mobile_price = #{sellMobilePrice,jdbcType=DECIMAL},",
          "is_grund = #{isGrund,jdbcType=TINYINT},",
          "is_audit = #{isAudit,jdbcType=TINYINT},",
          "product_stock = #{productStock,jdbcType=INTEGER},",
          "stock_warn = #{stockWarn,jdbcType=INTEGER},",
          "dummy_sales_volume = #{dummySalesVolume,jdbcType=INTEGER},",
          "real_sales_volume = #{realSalesVolume,jdbcType=INTEGER}",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(SkuProduct record);
}