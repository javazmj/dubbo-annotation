package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ESProduct;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * Created by: meijun
 * Date: 2018/11/26 20:15
 */
@Component
public interface ESProducRepository extends ElasticsearchRepository<ESProduct,String> {
}
