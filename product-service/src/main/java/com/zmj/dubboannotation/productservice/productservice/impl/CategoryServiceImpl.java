package com.zmj.dubboannotation.productservice.productservice.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.productservice.repository.MalCategoryRepository;
import com.zmj.dubboannotation.serviceapi.api.CategoryService;
import com.zmj.dubboannotation.serviceapi.model.MallCategory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/09/15 12:20
 */
@Service(interfaceClass = CategoryService.class)
@Component
@Slf4j
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private MalCategoryRepository categoryRepository;


    @Override
    @Transactional

    public MallCategory save(MallCategory category) {
        return categoryRepository.save(category);
    }

    @Override
    public List<MallCategory> findAll() {
        return categoryRepository.findAll();
    }
}
