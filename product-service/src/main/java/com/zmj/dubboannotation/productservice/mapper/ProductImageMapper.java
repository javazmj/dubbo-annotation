package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductImage;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

public interface ProductImageMapper {
    @Insert({
        "insert into product_image (id, product_id, ",
        "url, sort, is_main, ",
        "is_show, create_time)",
        "values (#{id,jdbcType=INTEGER}, #{productId,jdbcType=VARCHAR}, ",
        "#{url,jdbcType=VARCHAR}, #{sort,jdbcType=TINYINT}, #{isMain,jdbcType=TINYINT}, ",
        "#{isShow,jdbcType=TINYINT}, #{createTime,jdbcType=TIMESTAMP})"
    })
    int insert(ProductImage record);

    @Select({
        "select",
        "id, product_id, url, sort, is_main, is_show, create_time",
        "from product_image"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="url", property="url", jdbcType=JdbcType.VARCHAR),
        @Result(column="sort", property="sort", jdbcType=JdbcType.TINYINT),
        @Result(column="is_main", property="isMain", jdbcType=JdbcType.TINYINT),
        @Result(column="is_show", property="isShow", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<ProductImage> selectAll();

    @Select({
            "select",
            "id, product_id, url, sort, is_main, is_show, create_time",
            "from product_image WHERE product_id = #{productId} order by sort"
    })
    @Results({
            @Result(column="id", property="id", jdbcType=JdbcType.INTEGER),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
            @Result(column="url", property="url", jdbcType=JdbcType.VARCHAR),
            @Result(column="sort", property="sort", jdbcType=JdbcType.TINYINT),
            @Result(column="is_main", property="isMain", jdbcType=JdbcType.TINYINT),
            @Result(column="is_show", property="isShow", jdbcType=JdbcType.TINYINT),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<ProductImage> findAllByProductId(@Param("productId") String productId);
}