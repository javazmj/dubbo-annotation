package com.zmj.dubboannotation.productservice.productservice.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zmj.commoncore.constant.RedisConstant;
import com.zmj.commoncore.dto.CartDTO;
import com.zmj.dubboannotation.productservice.mapper.MallProductMapper;
import com.zmj.dubboannotation.productservice.redis.RedisUtil;
import com.zmj.dubboannotation.serviceapi.api.CartService;
import com.zmj.dubboannotation.serviceapi.api.ShopService;
import com.zmj.dubboannotation.serviceapi.enums.ProductEnum;
import com.zmj.dubboannotation.serviceapi.model.MemberShop;
import com.zmj.dubboannotation.serviceapi.vo.CartProductVO;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by: meijun
 * Date: 2018/09/15 12:20
 */
@Service(interfaceClass = CartService.class)
@Component
@Slf4j
public class CartServiceImpl implements CartService {

    @Autowired
    private MallProductMapper productMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Reference
    private ShopService shopService;

    @Override
    public JSONObject cartList(Map<Object, Object> cart, boolean checked) {
        JSONObject result = new JSONObject();
        //获取购物车列表
        Set<Object> keySet = cart.keySet();
        Iterator<Object> iterator = keySet.iterator();

        BigDecimal totalPrice = BigDecimal.ZERO;

        JSONArray cartArray = new JSONArray();
        while (iterator.hasNext()) {
            JSONObject shopJSON = new JSONObject();

            Object key = iterator.next();
            Object o = cart.get(key);
            JSONObject jsonObject = JSONObject.parseObject(o.toString());

            Set<String> set = jsonObject.keySet();
            Iterator<String> iterator1 = set.iterator();

            JSONArray array = new JSONArray();
            while (iterator1.hasNext()) {
                String productKey = iterator1.next();
                JSONObject object = jsonObject.getJSONObject(productKey);
                CartDTO cartDTO = JSONObject.toJavaObject(object, CartDTO.class);

                if(!checked) {
                    if(cartDTO.getChecked().intValue() == 1) {
                        ProductVO productVO = productMapper.findProductBySkuId(cartDTO.getProductId());
                        CartProductVO cartProductVO = new CartProductVO();

                        BeanUtils.copyProperties(cartDTO,cartProductVO);
                        BeanUtils.copyProperties(productVO,cartProductVO);

                        totalPrice = totalPrice.add(cartProductVO.getSellMobilePrice().multiply(BigDecimal.valueOf(cartProductVO.getQuantity())));
                        array.add(cartProductVO);
                    }
                }else {

                    ProductVO productVO = productMapper.findProductBySkuId(cartDTO.getProductId());
                    CartProductVO cartProductVO = new CartProductVO();

                    BeanUtils.copyProperties(cartDTO,cartProductVO);
                    BeanUtils.copyProperties(productVO,cartProductVO);

                    totalPrice = totalPrice.add(cartProductVO.getSellMobilePrice().multiply(BigDecimal.valueOf(cartProductVO.getQuantity())));
                    array.add(cartProductVO);
                }
            }
            MemberShop shop = shopService.findById(Integer.parseInt(key.toString()));
            shopJSON.put("shopName",shop.getShopName());
            shopJSON.put("shopId",shop.getId());
            shopJSON.put("productList",array);

            cartArray.add(shopJSON);
        }
        result.put("cartList",cartArray);
        result.put("totalPrice",totalPrice);
        return result;
    }

    @Override
    public int deleteChecked(Map<Object, Object> cart,String memberId) {
        if (null != cart && cart.size() > 0) {
            //获取购物车列表
            Set<Object> keySet = cart.keySet();
            Iterator<Object> iterator = keySet.iterator();
            boolean cartEmpty = true;
            while (iterator.hasNext()) {

                Object key = iterator.next();
                Object o = cart.get(key);
                JSONObject jsonObject = JSONObject.parseObject(o.toString());

                Set<String> set = jsonObject.keySet();
                Iterator<String> iterator1 = set.iterator();
                JSONObject productObj = new JSONObject();
                while (iterator1.hasNext()) {
                    String productKey = iterator1.next();
                    JSONObject object = jsonObject.getJSONObject(productKey);
                    CartDTO cartDTO = JSONObject.toJavaObject(object, CartDTO.class);
                    if(cartDTO.getChecked().intValue() == ProductEnum.CHECKED_NO.getCode().intValue()) {
                        productObj.put(productKey,cartDTO);
                    }
                }

                if(null == productObj || productObj.size() == 0) {
                    redisUtil.delete(RedisConstant.MEMBER_CART + memberId,String.valueOf(key));
                }else {
                    redisUtil.put(RedisConstant.MEMBER_CART + memberId,String.valueOf(key),productObj.toJSONString());
                    cartEmpty = false;
                }
            }

            //如果最后购物车为空,则清空缓存
            if(cartEmpty) {
                redisUtil.delete(RedisConstant.MEMBER_CART + memberId);
            }
        }
        return 0;
    }

}
