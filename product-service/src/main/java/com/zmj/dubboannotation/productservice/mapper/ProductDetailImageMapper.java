package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductDetailImage;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

public interface ProductDetailImageMapper {
    @Insert({
        "insert into product_detail_image (id, product_id, ",
        "url, sort, is_show, ",
        "create_time)",
        "values (#{id,jdbcType=VARCHAR}, #{productId,jdbcType=VARCHAR}, ",
        "#{url,jdbcType=VARCHAR}, #{sort,jdbcType=TINYINT}, #{isShow,jdbcType=TINYINT}, ",
        "#{createTime,jdbcType=TIMESTAMP})"
    })
    int insert(ProductDetailImage record);

    @Select({
        "select",
        "id, product_id, url, sort, is_show, create_time",
        "from product_detail_image"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="url", property="url", jdbcType=JdbcType.VARCHAR),
        @Result(column="sort", property="sort", jdbcType=JdbcType.TINYINT),
        @Result(column="is_show", property="isShow", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<ProductDetailImage> selectAll();

    @Select({
            "select",
            "id, product_id, url, sort, is_show, create_time",
            "from product_detail_image where product_id = #{productId} order by sort"
    })
    @Results({
            @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
            @Result(column="url", property="url", jdbcType=JdbcType.VARCHAR),
            @Result(column="sort", property="sort", jdbcType=JdbcType.TINYINT),
            @Result(column="is_show", property="isShow", jdbcType=JdbcType.TINYINT),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<ProductDetailImage> findAllByProductId(@Param("productId") String productId);
}