package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.productservice.provider.ProductProvider;
import com.zmj.dubboannotation.serviceapi.model.ESProduct;
import com.zmj.dubboannotation.serviceapi.model.MallProduct;
import com.zmj.dubboannotation.serviceapi.model.MemberProductCollection;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface MallProductMapper{

    @Insert({
        "insert into mall_product (id, product_name, ",
        "title, code, shop_id, ",
        "category_id, brand_id, ",
        "is_set_meal, product_giveaway_id, ",
        "detail, product_stock, ",
        "guarantee, produte_type, ",
        "is_grund, is_audit, ",
        "create_time, update_time, ",
        "del_flag)",
        "values (#{id,jdbcType=VARCHAR}, #{productName,jdbcType=VARCHAR}, ",
        "#{title,jdbcType=VARCHAR}, #{code,jdbcType=INTEGER}, #{shopId,jdbcType=INTEGER}, ",
        "#{categoryId,jdbcType=INTEGER}, #{brandId,jdbcType=INTEGER}, ",
        "#{isSetMeal,jdbcType=TINYINT}, #{productGiveawayId,jdbcType=INTEGER}, ",
        "#{detail,jdbcType=VARCHAR}, #{productStock,jdbcType=INTEGER}, ",
        "#{guarantee,jdbcType=VARCHAR}, #{produteType,jdbcType=TINYINT}, ",
        "#{isGrund,jdbcType=TINYINT}, #{isAudit,jdbcType=TINYINT}, ",
        "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP}, ",
        "#{delFlag,jdbcType=TINYINT})"
    })
    int insert(MallProduct record);

    @Select({
        "select",
        "id, product_name, title, code, shop_id, category_id, brand_id, is_set_meal, ",
        "product_giveaway_id, detail, product_stock, guarantee, produte_type, is_grund, ",
        "is_audit, create_time, update_time, del_flag",
        "from mall_product"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
        @Result(column="title", property="title", jdbcType=JdbcType.VARCHAR),
        @Result(column="code", property="code", jdbcType=JdbcType.INTEGER),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="category_id", property="categoryId", jdbcType=JdbcType.INTEGER),
        @Result(column="brand_id", property="brandId", jdbcType=JdbcType.INTEGER),
        @Result(column="is_set_meal", property="isSetMeal", jdbcType=JdbcType.TINYINT),
        @Result(column="product_giveaway_id", property="productGiveawayId", jdbcType=JdbcType.INTEGER),
        @Result(column="detail", property="detail", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_stock", property="productStock", jdbcType=JdbcType.INTEGER),
        @Result(column="guarantee", property="guarantee", jdbcType=JdbcType.VARCHAR),
        @Result(column="produte_type", property="produteType", jdbcType=JdbcType.TINYINT),
        @Result(column="is_grund", property="isGrund", jdbcType=JdbcType.TINYINT),
        @Result(column="is_audit", property="isAudit", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<MallProduct> selectAll();

    @Update({
        "update mall_product",
        "set product_name = #{productName,jdbcType=VARCHAR},",
          "title = #{title,jdbcType=VARCHAR},",
          "code = #{code,jdbcType=INTEGER},",
          "shop_id = #{shopId,jdbcType=INTEGER},",
          "category_id = #{categoryId,jdbcType=INTEGER},",
          "brand_id = #{brandId,jdbcType=INTEGER},",
          "is_set_meal = #{isSetMeal,jdbcType=TINYINT},",
          "product_giveaway_id = #{productGiveawayId,jdbcType=INTEGER},",
          "detail = #{detail,jdbcType=VARCHAR},",
          "product_stock = #{productStock,jdbcType=INTEGER},",
          "guarantee = #{guarantee,jdbcType=VARCHAR},",
          "produte_type = #{produteType,jdbcType=TINYINT},",
          "is_grund = #{isGrund,jdbcType=TINYINT},",
          "is_audit = #{isAudit,jdbcType=TINYINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(MallProduct record);

    @Select({
            "SELECT mp.id AS id," +
                    " mp.brand_id AS brandId, " +
                    " mp.category_id AS categoryId, " +
                    " mp.shop_id AS shopId, " +
                    " mp.detail AS detail, " +
                    " mp.title AS title, " +
                    " mp.guarantee AS guarantee, " +
                    " mp.produte_type AS produteType, " +
                    " mp.product_giveaway_id AS productGiveawayId, " +
                    " sp.attr_value AS attrValue, " +
                    " sp.sku_product_id AS skuProductId, " +
                    " sp.id AS skuId, " +
                    " sp.sku_name AS skuName, " +
                    " sp.sell_price AS sellPrice, " +
                    " sp.sell_mobile_price AS sellMobilePrice, " +
                    " sp.product_stock AS productStock, " +
                    " sp.dummy_sales_volume AS dummySalesVolume, " +
                    " ( SELECT p.url FROM product_image p WHERE p.product_id = mp.id AND p.is_main = 1 ) as mainImg  " +
                    " FROM " +
                    " sku_product sp" +
                    " LEFT JOIN mall_product mp ON mp.id = sp.product_id" +
                    " WHERE" +
                    " mp.del_flag = 0" +
                    " AND mp.is_audit = 1" +
                    " AND mp.is_grund = 1" +
                    " AND sp.is_grund = 1" +
                    " AND sp.is_audit = 1"
    })
    @ResultType(ProductVO.class)
    List<ProductVO> findMobileProductAll();

    @Select({
            "SELECT mp.id AS id," +
                    " mp.brand_id AS brandId, " +
                    " mp.category_id AS categoryId, " +
                    " mp.shop_id AS shopId, " +
                    " mp.detail AS detail, " +
                    " mp.title AS title, " +
                    " mp.guarantee AS guarantee, " +
                    " mp.produte_type AS produteType, " +
                    " mp.is_grund AS isGrund, " +
                    " mp.is_audit AS isAudit, " +
                    " mp.product_giveaway_id AS productGiveawayId, " +
                    " sp.attr_value AS attrValue, " +
                    " sp.sku_product_id AS skuProductId, " +
                    " sp.id AS skuId, " +
                    " sp.sku_name AS skuName, " +
                    " sp.sell_price AS sellPrice, " +
                    " sp.sell_mobile_price AS sellMobilePrice, " +
                    " sp.product_stock AS productStock, " +
                    " sp.dummy_sales_volume AS dummySalesVolume, " +
                    " ( SELECT p.url FROM product_image p WHERE p.product_id = mp.id AND p.is_main = 1 ) as mainImg  " +
                    " FROM " +
                    " sku_product sp" +
                    " LEFT JOIN mall_product mp ON mp.id = sp.product_id" +
                    " WHERE" +
                    " mp.del_flag = 0" +
                    " AND mp.is_audit = 1" +
                    " AND mp.is_grund = 1" +
                    " AND sp.is_grund = 1" +
                    " AND sp.id = #{skuId,jdbcType=VARCHAR}"
    })
    @ResultType(ProductVO.class)
    ProductVO findProductBySkuId(@Param("skuId") String skuId);


    @SelectProvider(type = ProductProvider.class,method = "findProductByParam")
    List<ProductVO> findProductByParam(MallProduct product);


    @SelectProvider(type = ProductProvider.class,method = "findProductByList")
    List<ProductVO> findProductByList(@Param("list")List<MemberProductCollection> list);


    @SelectProvider(type = ProductProvider.class,method = "findAllESProduct")
    List<ProductVO> findAllESProduct();
}