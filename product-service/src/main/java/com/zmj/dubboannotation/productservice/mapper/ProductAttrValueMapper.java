package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductAttrValue;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface ProductAttrValueMapper {
    @Delete({
        "delete from product_attr_value",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into product_attr_value (id, attribute_id, ",
        "shop_id, attr_vale, ",
        "attr_code, sort, ",
        "description, create_time, ",
        "update_time, create_by, ",
        "update_by, del_flag)",
        "values (#{id,jdbcType=INTEGER}, #{attributeId,jdbcType=INTEGER}, ",
        "#{shopId,jdbcType=INTEGER}, #{attrVale,jdbcType=VARCHAR}, ",
        "#{attrCode,jdbcType=INTEGER}, #{sort,jdbcType=TINYINT}, ",
        "#{description,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP}, ",
        "#{updateTime,jdbcType=TIMESTAMP}, #{createBy,jdbcType=VARCHAR}, ",
        "#{updateBy,jdbcType=VARCHAR}, #{delFlag,jdbcType=TINYINT})"
    })
    int insert(ProductAttrValue record);

    @Select({
        "select",
        "id, attribute_id, shop_id, attr_vale, attr_code, sort, description, create_time, ",
        "update_time, create_by, update_by, del_flag",
        "from product_attr_value",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="attribute_id", property="attributeId", jdbcType=JdbcType.INTEGER),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="attr_vale", property="attrVale", jdbcType=JdbcType.VARCHAR),
        @Result(column="attr_code", property="attrCode", jdbcType=JdbcType.INTEGER),
        @Result(column="sort", property="sort", jdbcType=JdbcType.TINYINT),
        @Result(column="description", property="description", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    ProductAttrValue selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, attribute_id, shop_id, attr_vale, attr_code, sort, description, create_time, ",
        "update_time, create_by, update_by, del_flag",
        "from product_attr_value"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="attribute_id", property="attributeId", jdbcType=JdbcType.INTEGER),
        @Result(column="shop_id", property="shopId", jdbcType=JdbcType.INTEGER),
        @Result(column="attr_vale", property="attrVale", jdbcType=JdbcType.VARCHAR),
        @Result(column="attr_code", property="attrCode", jdbcType=JdbcType.INTEGER),
        @Result(column="sort", property="sort", jdbcType=JdbcType.TINYINT),
        @Result(column="description", property="description", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="del_flag", property="delFlag", jdbcType=JdbcType.TINYINT)
    })
    List<ProductAttrValue> selectAll();

    @Update({
        "update product_attr_value",
        "set attribute_id = #{attributeId,jdbcType=INTEGER},",
          "shop_id = #{shopId,jdbcType=INTEGER},",
          "attr_vale = #{attrVale,jdbcType=VARCHAR},",
          "attr_code = #{attrCode,jdbcType=INTEGER},",
          "sort = #{sort,jdbcType=TINYINT},",
          "description = #{description,jdbcType=VARCHAR},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "create_by = #{createBy,jdbcType=VARCHAR},",
          "update_by = #{updateBy,jdbcType=VARCHAR},",
          "del_flag = #{delFlag,jdbcType=TINYINT}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ProductAttrValue record);
}