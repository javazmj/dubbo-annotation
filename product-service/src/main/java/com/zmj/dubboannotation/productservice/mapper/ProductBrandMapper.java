package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductBrand;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface ProductBrandMapper {
    @Delete({
        "delete from product_brand",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into product_brand (id, brand_name_english, ",
        "brand_name_chinese, logo, ",
        "set_up_date, brand_story, ",
        "brand_story_copy, create_time, ",
        "update_time, create_by, ",
        "update_by)",
        "values (#{id,jdbcType=INTEGER}, #{brandNameEnglish,jdbcType=VARCHAR}, ",
        "#{brandNameChinese,jdbcType=VARCHAR}, #{logo,jdbcType=VARCHAR}, ",
        "#{setUpDate,jdbcType=DATE}, #{brandStory,jdbcType=VARCHAR}, ",
        "#{brandStoryCopy,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP}, ",
        "#{updateTime,jdbcType=TIMESTAMP}, #{createBy,jdbcType=VARCHAR}, ",
        "#{updateBy,jdbcType=VARCHAR})"
    })
    int insert(ProductBrand record);

    @Select({
        "select",
        "id, brand_name_english, brand_name_chinese, logo, set_up_date, brand_story, ",
        "brand_story_copy, create_time, update_time, create_by, update_by",
        "from product_brand",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="brand_name_english", property="brandNameEnglish", jdbcType=JdbcType.VARCHAR),
        @Result(column="brand_name_chinese", property="brandNameChinese", jdbcType=JdbcType.VARCHAR),
        @Result(column="logo", property="logo", jdbcType=JdbcType.VARCHAR),
        @Result(column="set_up_date", property="setUpDate", jdbcType=JdbcType.DATE),
        @Result(column="brand_story", property="brandStory", jdbcType=JdbcType.VARCHAR),
        @Result(column="brand_story_copy", property="brandStoryCopy", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.VARCHAR)
    })
    ProductBrand selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, brand_name_english, brand_name_chinese, logo, set_up_date, brand_story, ",
        "brand_story_copy, create_time, update_time, create_by, update_by",
        "from product_brand"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="brand_name_english", property="brandNameEnglish", jdbcType=JdbcType.VARCHAR),
        @Result(column="brand_name_chinese", property="brandNameChinese", jdbcType=JdbcType.VARCHAR),
        @Result(column="logo", property="logo", jdbcType=JdbcType.VARCHAR),
        @Result(column="set_up_date", property="setUpDate", jdbcType=JdbcType.DATE),
        @Result(column="brand_story", property="brandStory", jdbcType=JdbcType.VARCHAR),
        @Result(column="brand_story_copy", property="brandStoryCopy", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="update_time", property="updateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="create_by", property="createBy", jdbcType=JdbcType.VARCHAR),
        @Result(column="update_by", property="updateBy", jdbcType=JdbcType.VARCHAR)
    })
    List<ProductBrand> selectAll();

    @Update({
        "update product_brand",
        "set brand_name_english = #{brandNameEnglish,jdbcType=VARCHAR},",
          "brand_name_chinese = #{brandNameChinese,jdbcType=VARCHAR},",
          "logo = #{logo,jdbcType=VARCHAR},",
          "set_up_date = #{setUpDate,jdbcType=DATE},",
          "brand_story = #{brandStory,jdbcType=VARCHAR},",
          "brand_story_copy = #{brandStoryCopy,jdbcType=VARCHAR},",
          "create_time = #{createTime,jdbcType=TIMESTAMP},",
          "update_time = #{updateTime,jdbcType=TIMESTAMP},",
          "create_by = #{createBy,jdbcType=VARCHAR},",
          "update_by = #{updateBy,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ProductBrand record);
}