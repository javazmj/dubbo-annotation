package com.zmj.dubboannotation.productservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MallCategory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by: meijun
 * Date: 2018/09/15 12:17
 */
public interface MalCategoryRepository extends JpaRepository<MallCategory,Integer> {

    @Override
    <S extends MallCategory> S save(S s);

}
