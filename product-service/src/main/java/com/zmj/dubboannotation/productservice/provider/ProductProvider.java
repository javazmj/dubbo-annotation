package com.zmj.dubboannotation.productservice.provider;

import com.google.common.base.Joiner;
import com.zmj.dubboannotation.serviceapi.model.MallProduct;
import com.zmj.dubboannotation.serviceapi.model.MemberProductCollection;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by: meijun
 * Date: 2018/10/28 20:30
 */
public class ProductProvider {

    public String findProductByParam(MallProduct product) {
        //优化
        return new SQL(){
            {
                SELECT("mp.id AS id," +
                                " mp.brand_id AS brandId, " +
                                " mp.category_id AS categoryId, " +
                                " mp.shop_id AS shopId, " +
                                " mp.detail AS detail, " +
                                " mp.title AS title, " +
                                " mp.guarantee AS guarantee, " +
                                " mp.produte_type AS produteType, " +
                                " mp.product_giveaway_id AS productGiveawayId, " +
                                " sp.attr_value AS attrValue, " +
                                " sp.sku_product_id AS skuProductId, " +
                                " sp.id AS skuId, " +
                                " sp.sku_name AS skuName, " +
                                " sp.sell_price AS sellPrice, " +
                                " sp.sell_mobile_price AS sellMobilePrice, " +
                                " sp.product_stock AS productStock, " +
                                " sp.dummy_sales_volume AS dummySalesVolume, " +
                                " ( SELECT p.url FROM product_image p WHERE p.product_id = mp.id AND p.is_main = 1 ) as mainImg  ")
                .FROM("sku_product sp")
                .LEFT_OUTER_JOIN("mall_product mp ON mp.id = sp.product_id");


                if(null != product) {
                    if (null != product.getDelFlag() ) {
                        WHERE("mp.del_flag =  #{delFlag}");
                    }else {
                        WHERE("mp.del_flag = 0");
                    }

                    if (null != product.getIsAudit() ) {
                        WHERE("mp.is_audit = #{isAudit}");
                        WHERE("sp.is_audit = #{isAudit}");
                    }else {
                        WHERE("mp.is_audit = 1");
                        WHERE("sp.is_audit = 1");
                    }

                    if (null != product.getIsGrund() ) {
                        WHERE("mp.is_grund =  #{isGrund}");
                        WHERE("sp.is_grund =  #{isGrund}");
                    }else {
                        WHERE("mp.is_grund = 1");
                        WHERE("sp.is_grund = 1");
                    }

                    if (null != product.getSkuProductId() ) {
                        WHERE("sp.sku_product_id =  #{skuProductId}");
                    }
                    if (null != product.getSkuId() ) {
                        WHERE("sp.id =  #{skuId}");
                    }
                    if (null != product.getShopId() ) {
                        WHERE("mp.shop_id =  #{shopId}");
                    }
                    if (null != product.getCategoryId() ) {
                        WHERE("mp.category_id =  #{categoryId}");
                    }
                    if (null != product.getBrandId() ) {
                        WHERE("mp.brand_id =  #{brandId}");
                    }
                }else {

                    WHERE("mp.del_flag = 0");
                    WHERE("mp.is_audit = 1");
                    WHERE("sp.is_audit = 1");
                    WHERE("mp.is_grund = 1");
                    WHERE("sp.is_grund = 1");

                }
            }
        }.toString();
    }


    public String findProductByList(Map map) {
        List<MemberProductCollection> memberActionLogs = (List<MemberProductCollection>) map.get("list");
        List<String> collect = memberActionLogs.stream()
                .map(memberActionLog -> memberActionLog.getProductId())
                .collect(Collectors.toList());
        String idList = Joiner.on(",").join(collect);
        //优化
        String sku_product_sp = new SQL() {
            {
                SELECT("mp.id AS id," +
                        " mp.brand_id AS brandId, " +
                        " mp.category_id AS categoryId, " +
                        " mp.shop_id AS shopId, " +
                        " mp.detail AS detail, " +
                        " mp.title AS title, " +
                        " mp.guarantee AS guarantee, " +
                        " mp.produte_type AS produteType, " +
                        " mp.product_giveaway_id AS productGiveawayId, " +
                        " sp.attr_value AS attrValue, " +
                        " sp.sku_product_id AS skuProductId, " +
                        " sp.id AS skuId, " +
                        " sp.sku_name AS skuName, " +
                        " sp.sell_price AS sellPrice, " +
                        " sp.sell_mobile_price AS sellMobilePrice, " +
                        " sp.product_stock AS productStock, " +
                        " sp.dummy_sales_volume AS dummySalesVolume, " +
                        " ( SELECT p.url FROM product_image p WHERE p.product_id = mp.id AND p.is_main = 1 ) as mainImg  ")
                        .FROM("sku_product sp")
                        .LEFT_OUTER_JOIN("mall_product mp ON mp.id = sp.product_id")
                        .WHERE("mp.del_flag = 0")
                        .WHERE("mp.is_audit = 1")
                        .WHERE("sp.is_audit = 1")
                        .WHERE("mp.is_grund = 1")
                        .WHERE("sp.is_grund = 1")
                        .WHERE("sp.id in ( " + idList + ")");
            }
        }.toString();

        return sku_product_sp;
    }


    public String findAllESProduct() {
        String findAllESProduct = new SQL() {
            {
                SELECT("SELECT " +
                        "mp.brand_id AS brandId, " +
                        "mp.category_id AS categoryId, " +
                        "mp.shop_id AS shopId, " +
                        "mp.detail AS detail, " +
                        "mp.title AS title, " +
                        "mp.guarantee AS guarantee," +
                        "mp.produte_type AS produteType," +
                        "mp.product_giveaway_id AS productGiveawayId," +
                        "sp.attr_value AS attrValue," +
                        "sp.sku_product_id AS skuProductId," +
                        "sp.id AS skuId," +
                        "sp.sku_name AS skuName," +
                        "sp.sell_price AS sellPrice, " +
                        "sp.sell_mobile_price AS sellMobilePrice, " +
                        "sp.product_stock AS productStock, " +
                        "sp.dummy_sales_volume AS dummySalesVolume," +
                        "mc.category_name as categoryName, " +
                        "pb.id as brandId, " +
                        "pb.brand_name_english as brandNameEnglish, " +
                        "pb.brand_name_chinese as brandNameChinese, " +
                        "ms.shop_name as shopName, " +
                        "(select GROUP_CONCAT(attr_value_name) from product_norms WHERE product_id = mp.id ) as attrValueName, " +
                        "(" +
                        "SELECT " +
                        "p.url " +
                        "FROM " +
                        "product_image p " +
                        "WHERE " +
                        "p.product_id = mp.id " +
                        "AND p.is_main = 1" +
                        ") AS mainImg " +
                        "FROM " +
                        "sku_product sp " +
                        "LEFT JOIN mall_product mp ON mp.id = sp.product_id " +
                        "LEFT JOIN mall_category mc on mc.id = mp.category_id " +
                        "LEFT JOIN product_brand pb on pb.id = mp.brand_id " +
                        "LEFT JOIN member_shop ms on ms.id = mp.shop_id ")
                        .WHERE("mp.del_flag = 0 ")
                        .WHERE("mp.is_audit = 1 ")
                        .WHERE("sp.is_audit = 1 ")
                        .WHERE("mp.is_grund = 1 ")
                        .WHERE("sp.is_grund = 1 ");
            }
        }.toString();

        return findAllESProduct;
    }
}
