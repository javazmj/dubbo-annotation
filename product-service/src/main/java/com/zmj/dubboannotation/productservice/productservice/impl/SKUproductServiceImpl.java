package com.zmj.dubboannotation.productservice.productservice.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.productservice.mapper.SkuProductMapper;
import com.zmj.dubboannotation.productservice.repository.MalCategoryRepository;
import com.zmj.dubboannotation.serviceapi.api.CategoryService;
import com.zmj.dubboannotation.serviceapi.api.SKUProducService;
import com.zmj.dubboannotation.serviceapi.model.MallCategory;
import com.zmj.dubboannotation.serviceapi.model.SkuProduct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by: meijun
 * Date: 2018/09/15 12:20
 */
@Service(interfaceClass = SKUProducService.class)
@Component
@Slf4j
public class SKUproductServiceImpl implements SKUProducService {

    @Autowired
    private SkuProductMapper skuProductMapper;


    @Override
    @Transactional
    public int save(SkuProduct skuProduct) {
        return skuProductMapper.insert(skuProduct);
    }
}
