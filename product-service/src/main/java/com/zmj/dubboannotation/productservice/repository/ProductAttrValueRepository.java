package com.zmj.dubboannotation.productservice.repository;

import com.zmj.dubboannotation.serviceapi.model.ProductAttrValue;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/10/26 13:55
 */
public interface ProductAttrValueRepository extends JpaRepository<ProductAttrValue,Integer> {

    List<ProductAttrValue> findByAttributeIdIsIn(@Param("list") List<Integer> list);
}
