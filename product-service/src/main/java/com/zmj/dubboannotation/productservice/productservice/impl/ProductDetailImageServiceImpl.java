package com.zmj.dubboannotation.productservice.productservice.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.productservice.mapper.ProductDetailImageMapper;
import com.zmj.dubboannotation.productservice.mapper.ProductImageMapper;
import com.zmj.dubboannotation.serviceapi.api.ProductDeailImageService;
import com.zmj.dubboannotation.serviceapi.api.ProductImgService;
import com.zmj.dubboannotation.serviceapi.model.ProductDetailImage;
import com.zmj.dubboannotation.serviceapi.model.ProductImage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/09/15 12:20
 */
@Service(interfaceClass = ProductDeailImageService.class)
@Component
@Slf4j
public class ProductDetailImageServiceImpl implements ProductDeailImageService {

    @Autowired
    private ProductDetailImageMapper productDetailImageMapper;


    @Override
    public List<ProductDetailImage> findAllByProductId(String productId) {
        return productDetailImageMapper.findAllByProductId(productId);
    }
}
