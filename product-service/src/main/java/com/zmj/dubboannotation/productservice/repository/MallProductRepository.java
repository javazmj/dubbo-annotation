package com.zmj.dubboannotation.productservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MallProduct;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by: meijun
 * Date: 2018/09/15 12:17
 */
public interface MallProductRepository extends JpaRepository<MallProduct,String> {
}
