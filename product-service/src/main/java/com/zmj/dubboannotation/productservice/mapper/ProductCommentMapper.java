package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.commoncore.dto.ProductCommentDTO;
import com.zmj.dubboannotation.productservice.provider.ProductCommentProvider;
import com.zmj.dubboannotation.serviceapi.model.ProductComment;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

public interface ProductCommentMapper {
    @Delete({
        "delete from product_comment",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into product_comment (id, product_id, ",
        "member_id, order_id, ",
        "comment, comment_grade, ",
        "buy_nurms, is_audit, ",
        "is_show, create_time)",
        "values (#{id,jdbcType=INTEGER}, #{productId,jdbcType=VARCHAR}, ",
        "#{memberId,jdbcType=VARCHAR}, #{orderId,jdbcType=VARCHAR}, ",
        "#{comment,jdbcType=VARCHAR}, #{commentGrade,jdbcType=TINYINT}, ",
        "#{buyNurms,jdbcType=VARCHAR}, #{isAudit,jdbcType=TINYINT}, ",
        "#{isShow,jdbcType=TINYINT}, #{createTime,jdbcType=TIMESTAMP})"
    })
    int insert(ProductComment record);

    @Select({
        "select",
        "id, product_id, member_id, order_id, comment, comment_grade, buy_nurms, is_audit, ",
        "is_show, create_time",
        "from product_comment",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="order_id", property="orderId", jdbcType=JdbcType.VARCHAR),
        @Result(column="comment", property="comment", jdbcType=JdbcType.VARCHAR),
        @Result(column="comment_grade", property="commentGrade", jdbcType=JdbcType.TINYINT),
        @Result(column="buy_nurms", property="buyNurms", jdbcType=JdbcType.VARCHAR),
        @Result(column="is_audit", property="isAudit", jdbcType=JdbcType.TINYINT),
        @Result(column="is_show", property="isShow", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    ProductComment selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, product_id, member_id, order_id, comment, comment_grade, buy_nurms, is_audit, ",
        "is_show, create_time",
        "from product_comment"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="order_id", property="orderId", jdbcType=JdbcType.VARCHAR),
        @Result(column="comment", property="comment", jdbcType=JdbcType.VARCHAR),
        @Result(column="comment_grade", property="commentGrade", jdbcType=JdbcType.TINYINT),
        @Result(column="buy_nurms", property="buyNurms", jdbcType=JdbcType.VARCHAR),
        @Result(column="is_audit", property="isAudit", jdbcType=JdbcType.TINYINT),
        @Result(column="is_show", property="isShow", jdbcType=JdbcType.TINYINT),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<ProductComment> selectAll();

    @Update({
        "update product_comment",
        "set product_id = #{productId,jdbcType=VARCHAR},",
          "member_id = #{memberId,jdbcType=VARCHAR},",
          "order_id = #{orderId,jdbcType=VARCHAR},",
          "comment = #{comment,jdbcType=VARCHAR},",
          "comment_grade = #{commentGrade,jdbcType=TINYINT},",
          "buy_nurms = #{buyNurms,jdbcType=VARCHAR},",
          "is_audit = #{isAudit,jdbcType=TINYINT},",
          "is_show = #{isShow,jdbcType=TINYINT},",
          "create_time = #{createTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ProductComment record);


    @Select({
            "select",
            "id, product_id, member_id, order_id, comment, comment_grade, buy_nurms, is_audit, ",
            "is_show, create_time",
            "from product_comment",
            "where product_id = #{productId} order by create_time desc"
    })
    @Results({
            @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
            @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
            @Result(column="order_id", property="orderId", jdbcType=JdbcType.VARCHAR),
            @Result(column="comment", property="comment", jdbcType=JdbcType.VARCHAR),
            @Result(column="comment_grade", property="commentGrade", jdbcType=JdbcType.TINYINT),
            @Result(column="buy_nurms", property="buyNurms", jdbcType=JdbcType.VARCHAR),
            @Result(column="is_audit", property="isAudit", jdbcType=JdbcType.TINYINT),
            @Result(column="is_show", property="isShow", jdbcType=JdbcType.TINYINT),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<ProductComment> findAllByProductId(String productId);

    @SelectProvider(type = ProductCommentProvider.class, method = "findByQuery")
    List<ProductComment> findByQuery(ProductCommentDTO productCommentDTO);
}