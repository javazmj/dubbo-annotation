package com.zmj.dubboannotation.productservice;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@EnableDubboConfiguration
@MapperScan(basePackages = "com.zmj.dubboannotation.productservice.mapper")
@EntityScan(basePackages = "com.zmj.dubboannotation.serviceapi.model")
@EnableJpaRepositories(basePackages = "com.zmj.dubboannotation.productservice.repository")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableJpaAuditing
@EnableAspectJAutoProxy
@EnableCaching
@EnableTransactionManagement
@EnableElasticsearchRepositories(basePackages = "com.zmj.dubboannotation.productservice.mapper")
//@EnableJpaAuditing(auditorAwareRef = "auditorAware")
//@ImportResource(locations = {"classpath:tcc-transaction.xml","classpath:tcc-repository.xml"})
public class ProductServiceApplication {

    public static void main(String[] args) {
        System.setProperty("es.set.netty.runtime.available.processors", "false");
        SpringApplication.run(ProductServiceApplication.class, args);
    }
}
