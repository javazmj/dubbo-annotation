package com.zmj.dubboannotation.productservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MallCategory;
import com.zmj.dubboannotation.serviceapi.model.ProductAttribute;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/09/15 12:17
 */
public interface ProductAttributeRepository extends JpaRepository<ProductAttribute,Integer> {

    List<ProductAttribute> findByCategoryId(@Param("categoryId") Integer categoryId);

}
