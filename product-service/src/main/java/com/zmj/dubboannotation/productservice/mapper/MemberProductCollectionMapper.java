package com.zmj.dubboannotation.productservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmj.dubboannotation.serviceapi.model.MemberProductCollection;
import java.util.List;

import com.zmj.dubboannotation.serviceapi.vo.ProductVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface MemberProductCollectionMapper extends BaseMapper<MemberProductCollection> {



    @Delete({
        "delete from member_product_collection",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into member_product_collection (id, member_id, ",
        "product_id, create_time)",
        "values (#{id,jdbcType=INTEGER}, #{memberId,jdbcType=VARCHAR}, ",
        "#{productId,jdbcType=VARCHAR}, NOW())"
    })
    int insert(MemberProductCollection record);

    @Select({
        "select",
        "id, member_id, product_id, create_time",
        "from member_product_collection",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    MemberProductCollection selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, member_id, product_id, create_time",
        "from member_product_collection"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
        @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<MemberProductCollection> selectAll();

    @Update({
        "update member_product_collection",
        "set member_id = #{memberId,jdbcType=VARCHAR},",
          "product_id = #{productId,jdbcType=VARCHAR},",
          "create_time = #{createTime,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(MemberProductCollection record);

    @Select({
            "select",
            "id, member_id, product_id, create_time",
            "from member_product_collection where member_id = #{memberId}"
    })
    @Results({
            @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
            @Result(column="member_id", property="memberId", jdbcType=JdbcType.VARCHAR),
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_time", property="createTime", jdbcType=JdbcType.TIMESTAMP)
    })
    List<MemberProductCollection> findAllById(String memberId);
}