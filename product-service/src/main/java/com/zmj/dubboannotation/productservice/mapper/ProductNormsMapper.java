package com.zmj.dubboannotation.productservice.mapper;

import com.zmj.dubboannotation.serviceapi.model.ProductNorms;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.type.JdbcType;

public interface ProductNormsMapper {
    @Delete({
        "delete from product_norms",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    @Insert({
        "insert into product_norms (id, product_id, ",
        "attribute_id, attr_value_id, ",
        "attribute_name, attr_value_name)",
        "values (#{id,jdbcType=INTEGER}, #{productId,jdbcType=INTEGER}, ",
        "#{attributeId,jdbcType=INTEGER}, #{attrValueId,jdbcType=INTEGER}, ",
        "#{attributeName,jdbcType=VARCHAR}, #{attrValueName,jdbcType=VARCHAR})"
    })
    int insert(ProductNorms record);

    @Select({
        "select",
        "id, product_id, attribute_id, attr_value_id, attribute_name, attr_value_name",
        "from product_norms",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.INTEGER),
        @Result(column="attribute_id", property="attributeId", jdbcType=JdbcType.INTEGER),
        @Result(column="attr_value_id", property="attrValueId", jdbcType=JdbcType.INTEGER),
        @Result(column="attribute_name", property="attributeName", jdbcType=JdbcType.VARCHAR),
        @Result(column="attr_value_name", property="attrValueName", jdbcType=JdbcType.VARCHAR)
    })
    ProductNorms selectByPrimaryKey(Integer id);

    @Select({
        "select",
        "id, product_id, attribute_id, attr_value_id, attribute_name, attr_value_name",
        "from product_norms"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="product_id", property="productId", jdbcType=JdbcType.INTEGER),
        @Result(column="attribute_id", property="attributeId", jdbcType=JdbcType.INTEGER),
        @Result(column="attr_value_id", property="attrValueId", jdbcType=JdbcType.INTEGER),
        @Result(column="attribute_name", property="attributeName", jdbcType=JdbcType.VARCHAR),
        @Result(column="attr_value_name", property="attrValueName", jdbcType=JdbcType.VARCHAR)
    })
    List<ProductNorms> selectAll();

    @Update({
        "update product_norms",
        "set product_id = #{productId,jdbcType=INTEGER},",
          "attribute_id = #{attributeId,jdbcType=INTEGER},",
          "attr_value_id = #{attrValueId,jdbcType=INTEGER},",
          "attribute_name = #{attributeName,jdbcType=VARCHAR},",
          "attr_value_name = #{attrValueName,jdbcType=VARCHAR}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(ProductNorms record);
}