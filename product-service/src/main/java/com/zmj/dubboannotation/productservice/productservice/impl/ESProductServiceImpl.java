package com.zmj.dubboannotation.productservice.productservice.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.productservice.mapper.ESProducRepository;
import com.zmj.dubboannotation.serviceapi.api.ESProductService;
import com.zmj.dubboannotation.serviceapi.model.ESProduct;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.lucene.search.function.FieldValueFactorFunction;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Created by: meijun
 * Date: 2018/11/26 20:15
 */
@Service(interfaceClass = ESProductService.class)
@Component
@Slf4j
public class ESProductServiceImpl implements ESProductService {

    @Autowired
    private ESProducRepository esProducRepository;

    @Override
    public boolean insert(ESProduct ESProduct) {
        boolean falg=false;
        try{
            esProducRepository.save(ESProduct);
            falg=true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return falg;
    }

    @Override
    public boolean delete(String id) {
        esProducRepository.deleteById(id);
        return false;
    }

    @Override
    public List<ESProduct> search(String searchContent) {
        QueryStringQueryBuilder builder = new QueryStringQueryBuilder(searchContent);
        System.out.println("查询的语句:"+builder);
        Iterable<ESProduct> searchResult = esProducRepository.search(builder);
        Iterator<ESProduct> iterator = searchResult.iterator();
        List<ESProduct> list=new ArrayList<ESProduct>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }

    @Override
    public List<ESProduct> searchUser(Integer pageNumber, Integer pageSize, String searchContent) {
        // 分页参数
        Pageable pageable = new PageRequest(pageNumber, pageSize);
        QueryStringQueryBuilder builder = new QueryStringQueryBuilder(searchContent);
        SearchQuery searchQuery = new NativeSearchQueryBuilder().withPageable(pageable).withQuery(builder).build();
        System.out.println("查询的语句:" + searchQuery.getQuery().toString());
        Page<ESProduct> searchPageResults = esProducRepository.search(searchQuery);
        return searchPageResults.getContent();
    }

    @Override
    public List<ESProduct> searchUserByWeight(String searchContent) {
        return null;
    }

    @Override
    public ESProduct queryESProductById(String id) {
        Optional<ESProduct> optional = esProducRepository.findById(id);
        return optional.get();
    }

    @Override
    public void saveEntity(ESProduct entity) {
        ESProduct save = esProducRepository.save(entity);
    }

    @Override
    public void saveEntity(List<ESProduct> entityList) {
        esProducRepository.saveAll(entityList);
    }

    @Override
    public List<ESProduct> searchEntity(String searchContent) {
        // 根据权重进行查询
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.must(QueryBuilders.matchQuery("skuName", "小米8"));

        ScoreFunctionBuilder<?> scoreFunctionBuilder = ScoreFunctionBuilders.fieldValueFactorFunction("sales").modifier(FieldValueFactorFunction.Modifier.LN1P).factor(0.1f);

        FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery(queryBuilder);


//                .add(QueryBuilders.boolQuery().should(QueryBuilders.matchQuery("name", searchContent)),
//                        ScoreFunctionBuilders.weightFactorFunction(10))
//                .add(QueryBuilders.boolQuery().should(QueryBuilders.matchQuery("description", searchContent)),
//                        ScoreFunctionBuilders.weightFactorFunction(100)).setMinScore(2);


        System.out.println("查询的语句:" + functionScoreQueryBuilder.toString());
        Iterable<ESProduct> searchResult = esProducRepository.search(functionScoreQueryBuilder);
        Iterator<ESProduct> iterator = searchResult.iterator();
        List<ESProduct> list=new ArrayList<ESProduct>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }

    @Override
    public ESProduct getById(String id) {
        return null;
    }
}
