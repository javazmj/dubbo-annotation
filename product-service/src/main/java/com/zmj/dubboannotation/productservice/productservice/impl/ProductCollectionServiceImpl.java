package com.zmj.dubboannotation.productservice.productservice.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmj.dubboannotation.productservice.mapper.MemberProductCollectionMapper;
import com.zmj.dubboannotation.productservice.repository.MemberProductCollectionRepository;
import com.zmj.dubboannotation.serviceapi.api.ProductCollectionService;
import com.zmj.dubboannotation.serviceapi.model.MemberProductCollection;
import com.zmj.dubboannotation.serviceapi.vo.ProductVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/11/01 20:18
 */
@Service(interfaceClass = ProductCollectionService.class)
@Component
@Slf4j
public class ProductCollectionServiceImpl implements ProductCollectionService {

    @Autowired
    private MemberProductCollectionMapper memberProductCollectionMapper;

    @Autowired
    private MemberProductCollectionRepository collectionRepository;

    @Override
    public List<MemberProductCollection> findAllById(String memberId) {
        return memberProductCollectionMapper.findAllById(memberId);
    }

    @Override
    public int save(MemberProductCollection memberProductCollection) {
        return memberProductCollectionMapper.insert(memberProductCollection);
    }

    @Override
    public int delete(int id) {
        return memberProductCollectionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public MemberProductCollection selectByProductId(String memberId, String productId) {
        return collectionRepository.getByMemberIdAndProductId(memberId,productId);
    }

    @Override
    @Transactional
    public int deleteByProductId(String memberId, String productId) {
        return collectionRepository.deleteByMemberIdAndProductId(memberId,productId);
    }
}
