package com.zmj.dubboannotation.productservice.repository;

import com.zmj.dubboannotation.serviceapi.model.MemberProductCollection;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

/**
 * Created by: meijun
 * Date: 2018/11/25 12:43
 */
public interface MemberProductCollectionRepository extends JpaRepository<MemberProductCollection,Integer> {

    MemberProductCollection getByMemberIdAndProductId(@Param("memberId") String memberId,@Param("productId")  String productId);

    @Modifying()
    int deleteByMemberIdAndProductId(@Param("memberId") String memberId,@Param("productId")  String productId);
}
