# dubbo-annotation

< 一个 HREF = “ https://996.icu ” > < IMG  SRC = “ https://img.shields.io/badge/link-996.icu-red.svg ”  ALT = “ 996.icu ” > < / a >
#### 项目介绍

一个研究电商系统的简单项目,麻雀虽小五脏俱全.目前提供了手机端商城的API,
后续会增加后台管理系统 商户后台管理系统等.

基于springboot2.0.4,用dubbo做服务化治理,用zookeeper做服务调度,
redis作为缓存,rabbitmq作为消息队列.

整个框架全部采用注解式开发,有mybatis3和jpa的高级查询示例
集成mycat读写分离负载均衡(只需要将jdbc连接改为mycat的端口账号密码) , 具体教程请参考google
项目地址:  https://gitee.com/javazmj/dubbo-annotation

#### 项目介绍

m.zmjmall.com 手机端入口
x.zmjmall.com  开放API
http://x.zmjmall.com/mobile/swagger-ui.html#/   接口文档

el.zmjmall.com  elasticsearch可视化界面
dubbo.zmjmall.com dubbo服务化治理后台入口 
zmjmall.com 主站入口

#### 软件架构
软件架构说明

jdk 1.8
mysql 5.7
zookeeper 3.4.11
dubbo-admin-2.5.4

暂时将项目拆分为 

base-service(基础类服务)  
product-service(产品相关服务)  
order-service(订单相关服务)
consumer-mobile(手机端)   
consumer-web(PC端)

分三台服务器部署,服务器为  a(2核4G).   b(2核4G).   c(2G2核)

b负责域名解析,使用Nginx分发请求到a b ,部署product-service 及 Elascticsearch
a部署 base-service order-service dubbo-admin
c部署 cms(暂未做后台管理系统)

a b c 数据库互为主备 并做redis和zk集群

版本功能介绍在version 文件


#### 安装教程

1. git clone git@gitee.com:javazmj/dubbo-annotation.git
2. 将跟目录 /sql/mall-dev.sql 导入数据库
3. 修改项目所有服务的 application.yml  里 spring.profiles.actives = dev
4. 修改项目所有服务的 application-dev.yml 的数据库账号密码


#### 使用说明


1. 启动zookeeper
2. 启动redis
3. 启动tomcat  dubbo-admin.war(可不启用,为了查看服务等)
4. 启动服务 base-service   product-service  order-service (按顺序启动,因为有依赖关系)
5. 启动 consumer-mobile
6. 访问 http://localhost:8103/mobile/test/list 测试接口,返回数据则成功

#### 参与贡献

@author:    zmj  
@Emali:     536304123@QQ.com

